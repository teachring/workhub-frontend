/** @format */

//https://github.com/babel/babel/tree/master/packages/babel-preset-stage-2
//Create our own preset using https://babeljs.io/docs/en/presets#preset-paths
module.exports = function() {
  return {
    plugins: [
      // Explicit
      '@babel/plugin-transform-runtime',
      // Stage 2
      // ['@babel/plugin-proposal-decorators', { legacy: true }],
      // '@babel/plugin-proposal-function-sent',
      // '@babel/plugin-proposal-export-namespace-from',
      // '@babel/plugin-proposal-numeric-separator',
      // '@babel/plugin-proposal-throw-expressions',

      // Stage 3
      '@babel/plugin-syntax-dynamic-import',
      // '@babel/plugin-syntax-import-meta',
      // ['@babel/plugin-proposal-class-properties', { loose: false }],
      '@babel/plugin-proposal-json-strings',

    
      ['import', { libraryName: 'lodash', libraryDirectory: '', camel2DashComponentName: false }, 'lodash'],
    ],
    presets: [
      '@babel/preset-typescript',
      [
        '@babel/preset-env',
        {
          targets: { browsers: ['>0.25%', 'not ie 11', 'not op_mini all'] },
        },
      ],
      '@babel/preset-react',
    ],
  };
};
