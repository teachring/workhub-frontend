const path = require('path');

const webpack = require('webpack');
const CompressionPlugin = require('compression-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const WebpackRequireFrom = require('webpack-require-from');

const config = env => {
  return {
    mode: 'development',
    bail: true, 
    cache: true,
    profile: true,
    node: {
      fs: 'empty',
    },
    entry: {
      main: './src/index.tsx',
    },
    output: {
      path: '/cloud/web',
      filename: '[name].bundle.js', //Needs .js at the end of the name for uglify/minify optimizations to happen
      //computed value of config.get('app.assets_cdn'). Using config.get doesn't work because
      //1. Understaing ES6 requires using "babel-node webpack" insated of "webpack"
      //2. env vars aren't passed as process.env in webpack
      publicPath: '', //specified at runtime through __webpack_public_path__ `https://cdn-deploy.vrgmetri.com/teaxrweb/${env.BUILD_VERSION}/web/`,
    },
    module: {
      rules: [
        { test: /\.txt$/, use: 'raw-loader'},
        { test: /\.svg$/, use: ['@svgr/webpack']},
        { test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: 'url-loader?limit=10000&mimetype=application/fontwoff' },
        { test: /\.css$/, use: [{loader: 'style-loader'}, {loader: 'css-loader'}] },
        { test: /\.scss$/, use: [{loader: 'style-loader'}, {loader: 'css-loader'}, {loader: 'sass-loader'}] },
        {
          test: /\.[jt]sx?$/,
          exclude: /node_modules/, //teaxrcommon shouldn't get excluded from babel-loader
          use: [{
            loader: 'babel-loader',
            options: {
              babelrc: false,
              cacheDirectory: true,
              presets: ['./fab/babel/babel-preset-react'],
            },
          }]
        },
      ],
    },
    devtool: 'inline-source-map',
    devServer: {
      publicPath: `http://localhost:${env.PORT_WEBPACK}/web/`,
      public: `localhost:${env.PORT_WEBPACK}`,
      port: env.PORT_WEBPACK,
      host: '0.0.0.0',
      hot: true,
      inline: true,
      headers: { 'Access-Control-Allow-Origin': '*' },
      // compress: true,
      disableHostCheck: true
    },
    resolve: {
      modules: ['node_modules', './src'],
      extensions: ['.js', '.jsx', '.ts', '.tsx'],
    },
    optimization: {
      splitChunks: {
        cacheGroups: {
          commons: {
            name: "common",
            chunks: "all",
            minChunks: 3
          }
        }
      },
    },
    plugins: [
      new webpack.HotModuleReplacementPlugin(),
      new WebpackRequireFrom({ variableName: '__webpack_public_path__' }),
    ],
  };
};

module.exports = config;