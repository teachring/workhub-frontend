This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).


## Our Dockers

> NOTE: Needs docker and docker-compose installed.

front-end dev docker command
```
docker run \
 -v $PWD:/src \
 -p 3000:3000 \
 -e PORT_WEBPACK=3000 \
 -e AWS_ACCESS_KEY_ID=AKIAVZVMX6X44QA5VIU2 \
 -e AWS_SECRET_ACCESS_KEY=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 \
 -w /src \
 -it node:lts-alpine3.11 sh
```

Mock server command
```
 docker run \
 -v $PWD:/src \
 -p 7000:7000 \
 -w /src \
 -it node:lts-alpine3.11 sh
```

Lightsail Docker Command

```
docker run \
-e PMA_HOST=ls-6d20b485c411ea46052a051551cd140874f21266.cmuwwbrx5uwx.ap-south-1.rds.amazonaws.com \
-e PMA_PORT=3306 \
-e PMA_USER=dbmasteruser \
-e PMA_PASSWORD="2_kboo(WsAx(~&>T8L7v[%=Z6V.8ptg6" \
-p 8080:80 phpmyadmin/phpmyadmin
```


DO DB creds
```
username = doadmin
password = fjf4y0b7jn9dlqxp
host = db-mysql-blr1-16767-do-user-6449078-0.db.ondigitalocean.com
port = 25060
database = defaultdb
sslmode = REQUIRED
```