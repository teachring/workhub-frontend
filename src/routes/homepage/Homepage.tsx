import React, { useEffect, useRef } from "react";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import { Typography, Box, Button } from "@material-ui/core";
import Typed from 'react-typed';

import AboutSVG from "../../assets/images/landing-about.svg";
import RegistrationSVG from "../../assets/images/registration.svg";
import Footer from "../../components/shared/footer/Footer";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        overline: {
            fontSize: "1.1rem",
            color: "#6b63ff",
            position: "relative",
            // "&>span": {
            //     "&:before": {
            //         width: "1rem",
            //         height: 2,
            //         content: "''",
            //         position: "absolute",
            //         backgroundColor: "#6b63ff",
            //         top: "50%",
            //         left: 0,
            //         transform: "translate(-160%, -25%)"
            //     },
            //     "&:after": {
            //         width: "1rem",
            //         height: 2,
            //         content: "''",
            //         position: "absolute",
            //         backgroundColor: "#6b63ff",
            //         top: "50%",
            //         right: 0,
            //         transform: "translate(160%, -25%)"
            //     }
            // }
        },
        heading: {
            fontSize: "1.8rem",
            maxWidth: 600,
            alignSelf: "center",
            margin: "auto"
        },
        row: {
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-evenly",
            flexWrap: "wrap"
        },
        rowText: {
            textAlign: "left",
            padding: "0 4rem",
            maxWidth: 550,
            // minWidth: 400,
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            "& .MuiTypography-body1": {
                fontSize: "1.1rem",
                marginTop: 5,
                marginBottom: 10
            }
        },
        rowImage: {
            "&>img": {
                maxWidth: 550,
                width: "100%",
            }
        },
        rowHeading: {
            fontSize: "1.3rem",
            color: "#6b63ff",
        },
        headerText: {
            textAlign: "inherit",
            "& .MuiTypography-root": {
                fontSize: "2rem",
            }
        },
        headerSubtext: {
            fontSize: "1.5rem",
            marginTop: "1rem",
            textAlign: "inherit"
        },
        headerBtns: {
            height: 50,
            textAlign: "inherit",
            "& button": {
                width: 190,
                padding: "10px 0"
            }
        },
        headerBtn1: {
            backgroundColor: "#6259ff",
            color: "#fff",
            "&:hover": {
                backgroundColor: "#5b51ff",
                boxShadow: "0 6px 13px -2px rgba(107, 99, 255, 0.3)"
            }
        },
        headerBtn2: {
            color: "#6259ff",
            // border: "1px solid",
            borderColor: "currentColor",
            "&:hover": {
                backgroundColor: "rgba(106, 99, 255, .1)"
            }
        },
        headerContent: {
            textAlign: "left"
        },
        aboutContainer: {
            margin: "0px auto 5rem",
            display: "flex",
            position: "relative",
            maxWidth: 700,
            textAlign: "left",
            alignItems: "flex-end",
            borderBottom: "1px solid #2F2E41",
        },
        [theme.breakpoints.down("sm")]: {
            headerContent: {
                textAlign: "center"
            },
            headerBtns: {
                height: 100
            },
            heading: {
                fontSize: "1.5rem",
                padding: "0 2rem",
            },
            rowImage: {
                padding: "2rem",
                "&>svg": {
                    maxWidth: "100%"
                }
            },
            aboutContainer: {
                borderBottom: "none",
                "&>div": {
                    padding: "0 4rem"
                },
                "&>img": {
                    display: "none",
                }
            }
        }
    })
);


const Homepage: React.FC = () => {

    const classes = useStyles();

    return (
        <>
            <Box overflow="hidden">

                <Box mt={10} mb={16}>
                    <Box className={classes.row} alignItems="center">
                        <Box className={classes.headerContent} mb={10}>
                            <Box className={classes.headerText}>
                                <Typography style={{ marginBottom: "1rem" }}>Host and manage your</Typography>

                                <Typography>
                                    <Typed
                                        strings={[
                                            "TUITIONS",
                                            "WORKSHOPS",
                                            "TRAININGS"
                                        ]}
                                        typeSpeed={80}
                                        backSpeed={50}
                                        startDelay={1000}
                                        backDelay={2000}
                                        loop={true}
                                        cursorChar="|"
                                    />
                                </Typography>
                            </Box>

                            <Typography className={classes.headerSubtext}>Simply. Effectively.</Typography>

                            <Box className={classes.headerBtns} pt={4} textAlign="left">
                                <Button className={classes.headerBtn1}>Create a Class</Button>
                                <Box mt={2} />
                                <Button className={classes.headerBtn2}>Explore classes</Button>
                            </Box>
                        </Box>

                        <Box className={classes.rowImage}>
                            <img src="https://s3.ap-south-1.amazonaws.com/deploy.teachring.com/assets/images/landing-header.svg" />
                        </Box>
                    </Box>
                </Box>

                <Box className={classes.aboutContainer}>
                    <img src="https://s3.ap-south-1.amazonaws.com/deploy.teachring.com/assets/images/landing-about.svg" alt="about" style={{ marginRight: "4rem", maxHeight: 600 }} />
                    <Box marginBottom="auto">
                        <Typography style={{ fontSize: "1.25rem", marginBottom: "2rem", letterSpacing: "0.006rem" }}><Typography component="span" style={{ fontWeight: "bold", fontSize: "inherit", letterSpacing: "inherit", fontFamily: "Montserrat Alternates", marginRight: 4 }}>Teachring</Typography> is a simple yet powerful platform to help educators easily manage their class and facilitate streamlined teaching-learning experiences.</Typography>
                        <Typography style={{ fontSize: "1.25rem", marginBottom: "0rem", letterSpacing: "0.006rem" }}>Built with the vision to empower educators and learners with the right set of tools so that they can focus on what matters the most.</Typography>
                    </Box>
                </Box>

                <Box my={3}>
                    <Typography className={classes.overline} variant="overline"><span>Seamless Registration</span></Typography>
                    <Typography className={classes.heading}>Set up a completely customized registration flow for your class in just minutes</Typography>

                    <Box className={classes.row} mt={5}>
                        <Box style={{ transform: "scale(.8)" }} className={classes.rowImage}>
                            <img src="https://s3.ap-south-1.amazonaws.com/deploy.teachring.com/assets/images/registration.svg" />
                        </Box>

                        <Box className={classes.rowText}>
                            <Typography className={classes.rowHeading} variant="subtitle1">Accept payments both online and in cash</Typography>
                            <Typography>Collect payments online right into your bank account. Want to accept onsite cash payments, send payment reminders, don’t worry. Teachring helps you manage everything.</Typography>
                            <Box mt={2} />
                            <Typography className={classes.rowHeading} variant="subtitle1">Custom Registration forms</Typography>
                            <Typography>Create your own form to collect all the student information that matters to you. We make sure that you are always in control of your data.</Typography>
                        </Box>
                    </Box>
                </Box>

                <Box my={10}>
                    <Typography className={classes.overline} variant="overline"><span>Communicate</span></Typography>
                    <Typography className={classes.heading}>Clear doubts, broadcast information to your class anytime, anywhere</Typography>

                    <Box className={classes.row} style={{ flexWrap: "wrap-reverse" }} >
                        <Box className={classes.rowText}>
                            <Typography className={classes.rowHeading} variant="subtitle1">Instant SMS announcements</Typography>
                            <Typography>Whether it is class rescheduling or unexpected cancellations, notify your students instantly with just a single button click.</Typography>
                            <Box mt={2} />
                            <Typography className={classes.rowHeading} variant="subtitle1">Receive Questions, feedback of your learners</Typography>
                            <Typography>Online platform for educators and learners to communicate effectively even outside the classroom. In a one-to-one online channel, even the most shy students are comfortable speaking out.</Typography>
                        </Box>

                        <Box className={classes.rowImage}>
                            <img src="https://s3.ap-south-1.amazonaws.com/deploy.teachring.com/assets/images/communicate.svg" />
                        </Box>
                    </Box>
                </Box>

                <Box my={10}>
                    <Typography className={classes.overline} variant="overline"><span>Learning Resources</span></Typography>
                    <Typography className={classes.heading}>Publish and distribute your learning resources, lesson plans with ease</Typography>

                    <Box className={classes.row}>
                        <Box className={classes.rowImage}>
                            <img src="https://s3.ap-south-1.amazonaws.com/deploy.teachring.com/assets/images/learning2.svg" />
                        </Box>

                        <Box className={classes.rowText}>
                            <Typography className={classes.rowHeading} variant="subtitle1">Create and store your learning resources</Typography>
                            <Typography>Easily create high-quality digital learning content for your class. Publish lecture materials, notes, links etc. and store them organized in a central place.</Typography>
                            <Box mt={2} />
                            <Typography className={classes.rowHeading} variant="subtitle1">Give instant access to your students</Typography>
                            <Typography>Distribute all your learning resources to the class. Students can access them anytime whenever they need it.</Typography>
                        </Box>
                    </Box>
                </Box>

                <Box mt={10}>
                    <Typography className={classes.overline} variant="overline"><span>Online Presence</span></Typography>
                    <Typography className={classes.heading}>Build a strong professional online presence, attract more students</Typography>

                    <Box className={classes.row} style={{ flexWrap: "wrap-reverse" }} >

                        <Box className={classes.rowText}>
                            <Typography className={classes.rowHeading} variant="subtitle1">Your own beautiful web-page</Typography>
                            <Typography>Convey all the information about your class in your own online space. Completely free yet customizable with no coding skills required.</Typography>
                            <Box mt={2} />
                            <Typography className={classes.rowHeading} variant="subtitle1">Manage Pre-registration Enquiries</Typography>
                            <Typography>Receive enquiries from students before they register for your class. You can respond to them at your own time without being interrupted all day.</Typography>
                        </Box>


                        <Box style={{ transform: "scale(.7)" }} className={classes.rowImage}>
                            <img src="https://s3.ap-south-1.amazonaws.com/deploy.teachring.com/assets/images/online-presence.svg" />
                        </Box>

                    </Box>
                </Box>
            </Box>

            <Footer />
        </>
    )
}

export default Homepage;