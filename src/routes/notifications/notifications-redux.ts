import { RootState } from "../../utils/reduxReducers";
import { markNotificationsAsRead } from "../../utils/reduxActions/notifications";
import { connect } from "react-redux";
import NotificationsComp from "./notifications";


const mapStateToProps = (state:RootState) => {


    return {
        notificationList: state.NotificationData.notificationList,
        getUserDetails: (userId:number) => {
            return state.UsersDB[userId];
        },
        getClassInfo: (classId:number) => {
            return state.ClassInfoList[classId];
        }
    }
}

const mapDispatchToProps = (dispatch:Function) => {
    return {
        onNotificationsRead: (notificationIdList:number[]) => {
            dispatch(markNotificationsAsRead(notificationIdList));
        }
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(NotificationsComp);