import React from "react";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import { Paper, Typography, Box, useTheme, CardContent, Card, Avatar, Tooltip, Button } from "@material-ui/core";
import {
    CheckCircleTwoTone,
    QuestionAnswerTwoTone,
    ContactSupportTwoTone,
    NotificationImportantTwoTone,
    GroupAddTwoTone,
    PaymentTwoTone,
    Face,
    AttachMoneyOutlined,
    InfoOutlined,
    HelpOutlineOutlined,
    ClassOutlined,
    DoneAllOutlined,
    PersonAddOutlined,
    LocalLibraryOutlined,
    PanToolOutlined,
    LiveHelpOutlined
} from "@material-ui/icons";
import { deepOrange, green, blue, purple, pink, lightBlue, red, indigo } from "@material-ui/core/colors";
import { SavedDraftInfo } from "../../utils/core-types/educator-types";
import { ClassInfo } from "../../utils/core-types/class-types";
import { readDateTime, timeSince } from "../../utils/helpers";
import { NotificationTypes } from "../../utils/core-types/user-types";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        paper: {
            maxWidth: 500,
            margin: "3rem auto",
            display: "flex",
            padding: "2rem",
            position: "relative",
            "&:before": {
                content: "''",
                position: "absolute",
                left: 0,
                top: "2rem",
                background: purple[500],
                width: 3,
                height: 24,
            }
        },
        content: {
            textAlign: "left",
            paddingLeft: 24,
            fontSize: "1.2rem",
            flex: 1
        },
        details: {
            display: "flex",
            justifyContent: "space-between",
            marginBottom: "1rem",
            alignItems: "center",
        },
        user: {
            fontSize: "0.8rem",
            fontWeight: "lighter"
        },
        time: {
            fontWeight: "lighter",
            fontSize: "0.8rem"
        },
        icon: {
            "&[data-payment]": { color: red[500] },
            "&[data-announcement]": { color: purple[500] },
            "&[data-query]": { color: deepOrange[500] },
            "&[data-question]": { color: pink[500] },
            "&[data-answer]": { color: green['A700'] },
            "&[data-moderator]": { color: indigo['A400'] },
            "&[data-register]": { color: lightBlue['A400'] },
            "&:after": {
                top: "2rem",
                left: 0,
                height: 24,
                content: "''",
                position: "absolute",
                borderLeft: "3px solid",
            }
        },
        notifBtn: {
            marginTop: "2rem"
        },
        [theme.breakpoints.down("xs")]: {
            paper: {
                margin: "3rem 1rem"
            }
        }

    })
);

interface IProp {
    className: string,
    notificationType: NotificationTypes,
    body: string,
    date: Date,
    actionBtnText?: string,
    actionHandler?: () => void
}

//show button when notification type is one of the following:   [NotificationTypes.REG_ENQUIRY], [NotificationTypes.CLASSROOOM_QUES_ASKED], [NotificationTypes.CLASSROOM_QUES_CLOSED], [NotificationTypes.MODERATOR_ACCESS] 

const NotificationCard: React.FC<IProp> = ({ className, notificationType, body, date, actionBtnText, actionHandler }) => {

    const classes = useStyles();

    const tooltip_placement = "bottom-end";

    return (
        <Paper className={classes.paper}>

            {
                (() => {
                    switch (notificationType) {
                        case NotificationTypes.PAYMENT_REMINDER:
                            return <Box className={classes.icon} data-payment>
                                <Tooltip title="Payment reminder" placement={tooltip_placement}><AttachMoneyOutlined /></Tooltip>
                            </Box>

                        case NotificationTypes.ANNOUNCEMENT:
                            return <Box className={classes.icon} data-announcement>
                                <Tooltip title="Announcement" placement={tooltip_placement}><InfoOutlined /></Tooltip>
                            </Box>

                        case NotificationTypes.REG_ENQUIRY:
                            return <Box className={classes.icon} data-query>
                                <Tooltip title="Registration query" placement={tooltip_placement}><LiveHelpOutlined /></Tooltip>
                            </Box>

                        case NotificationTypes.CLASSROOOM_QUES_ASKED:
                            return <Box className={classes.icon} data-question>
                                <Tooltip title="Classroom question asked" placement={tooltip_placement}><PanToolOutlined /></Tooltip>
                            </Box>

                        case NotificationTypes.CLASSROOM_QUES_CLOSED:
                            return <Box className={classes.icon} data-answer>
                                <Tooltip title="Classroom question answered" placement={tooltip_placement}><DoneAllOutlined /></Tooltip>
                            </Box>

                        case NotificationTypes.MODERATOR_ACCESS:
                            return <Box className={classes.icon} data-moderator>
                                <Tooltip title="Moderator added" placement={tooltip_placement}><PersonAddOutlined /></Tooltip>
                            </Box>

                        case NotificationTypes.SUCCESSFUL_REGISTER:
                            return <Box className={classes.icon} data-register>
                                <Tooltip title="Registration successful" placement={tooltip_placement}><LocalLibraryOutlined /></Tooltip>
                            </Box>

                        default:
                            break;
                    }
                })()
            }

            <Box className={classes.content}>
                <Box className={classes.details}>
                    <Typography className={classes.user}>{className}</Typography>
                    <Typography className={classes.time}>{timeSince(date)}</Typography>
                </Box>
                {body}

                {
                    actionBtnText && <div><Button className={classes.notifBtn} variant="outlined" color="primary" onClick={actionHandler}>{actionBtnText}</Button></div>
                }
            </Box>
        </Paper>
    );
}

export default NotificationCard;