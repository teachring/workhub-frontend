import React, { useEffect } from 'react'
import { Notification, User, NotificationTypes } from '../../utils/core-types/user-types';
import { ClassInfo } from '../../utils/core-types/class-types';
import NotificationCard from './notification-card';
import { Typography } from '@material-ui/core';
import useStyleClasses from './notifications-styles';
import { AnnouncementObj, EducatorRegQuery } from '../../utils/core-types/educator-types';
import { QuesAnsObj } from '../../utils/core-types/shared';
import { useHistory } from 'react-router-dom';

type IProp = {
    notificationList: Notification[],
    onNotificationsRead: (idList: number[]) => void,
    getUserDetails: (userId: number) => User,
    getClassInfo: (classId: number) => ClassInfo
}

const NotificationsComp: React.FC<IProp> = ({ notificationList, onNotificationsRead, getClassInfo, getUserDetails }) => {

    const classes = useStyleClasses();
    const routeHistory = useHistory();

    useEffect(() => {
        const unreadNotificationIdList: number[] = [];
        notificationList.forEach((notiObj) => {
            if (!notiObj.isRead) unreadNotificationIdList.push(notiObj.notificationId);
        })
        if (unreadNotificationIdList.length > 0) {
            onNotificationsRead(unreadNotificationIdList);
        }

    }, [notificationList])

    const getActionBtnData = (obj: Notification): { actionBtnText?: string, actionHandler?: () => void } => {

        switch (obj.notificationType) {
            case NotificationTypes.ANNOUNCEMENT:
            case NotificationTypes.PAYMENT_REMINDER:
            case NotificationTypes.SUCCESSFUL_REGISTER:
                return { actionBtnText: undefined, actionHandler: undefined }
            case NotificationTypes.CLASSROOOM_QUES_ASKED:
                return { actionBtnText: 'See Details', actionHandler: () => { routeHistory.push('/educator') } }
            case NotificationTypes.CLASSROOM_QUES_CLOSED:
                return { actionBtnText: 'See Details', actionHandler: () => { routeHistory.push('/') } }
            case NotificationTypes.REG_ENQUIRY:
                return { actionBtnText: 'See Details', actionHandler: () => { routeHistory.push('/educator') } }
            case NotificationTypes.MODERATOR_ACCESS:
                return { actionBtnText: 'Go to Class', actionHandler: () => { routeHistory.push('/educator') } }
        }

    }

    return (
        <div className={classes.cardWrapper}>
            {
                notificationList.length > 0 ?
                    notificationList.map(obj => {
                        return <NotificationCard
                            key={obj.notificationId}
                            className={getClassInfo(obj.classId).title} notificationType={obj.notificationType}
                            date={obj.createdDateTime}
                            body={generateNotificationBody(obj, getUserDetails)}
                            {...getActionBtnData(obj)}
                        />
                    })
                    :
                    "No Notifications now"
            }
        </div>

    )
}

const generateNotificationBody = (obj: Notification, getUserDetails: (userId: number) => User): string => {
    switch (obj.notificationType) {
        case NotificationTypes.ANNOUNCEMENT: {
            return (obj.payload as AnnouncementObj).message;
        }
        case NotificationTypes.PAYMENT_REMINDER:
        case NotificationTypes.SUCCESSFUL_REGISTER: {
            return obj.payload;
        }
        case NotificationTypes.CLASSROOOM_QUES_ASKED: {
            return getUserDetails((obj.payload as QuesAnsObj).askedBy) + ' has asked a question';
        }
        case NotificationTypes.CLASSROOM_QUES_CLOSED: {
            if ((obj.payload as QuesAnsObj).answerBody) {
                return 'Educator has answered one of the questions asked.'
            }
            return 'Educator has closed one of the questions asked';
        }
        case NotificationTypes.REG_ENQUIRY: {
            return getUserDetails((obj.payload as EducatorRegQuery).askedBy).userName + ' has sent an enquiry.'
        }
        case NotificationTypes.MODERATOR_ACCESS: {
            return 'You are added as a collaborator.'
        }
    }
}



export default NotificationsComp;