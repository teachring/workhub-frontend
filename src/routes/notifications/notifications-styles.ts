

import {makeStyles,Theme, useTheme,createStyles} from "@material-ui/core/styles";

const useClasses = makeStyles((theme: Theme) =>
  createStyles({
    cardWrapper: {
        width: "100%",
    }
  })
);

export default useClasses;