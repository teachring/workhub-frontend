import React, { useEffect } from 'react';
import { Switch, useRouteMatch, Route } from 'react-router-dom';
import { LearnerRegComp } from '../../components/learner-register';
import { LearnerClassroomComp } from '../../components/learner-classroom';
import { extractIdFromClassUrl } from '../../utils/helpers';
import { ClassDetailsComp } from '../../components/class-details';

export const LearnerClassRouterComp:React.FC = () => {
    
    //because these components works by d
    var  {path,params} = useRouteMatch();

    const classId = extractIdFromClassUrl(params.class_name);
    //Now if the route matches

    return (

        <Switch>
             <Route path={`${path}/register`}>
              <LearnerRegComp classId={classId} />
            </Route>
            <Route path={`${path}/classroom`}>
              <LearnerClassroomComp classId={classId} />
            </Route>
            <Route exact path={path}>
                <ClassDetailsComp classId={classId}/>
            </Route>
            
        </Switch>

    )




}