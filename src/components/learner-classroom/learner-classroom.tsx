import React from 'react';
import { Route, Switch, useRouteMatch, useHistory } from 'react-router-dom'
import {
    Book as BookIcon,
    QuestionAnswer as QuestionAnswerIcon
} from '@material-ui/icons';

//import { ROUTES } from "../../utils/core-types/learner-types";
import { DrawerData } from '../../utils/core-types/shared';
import Dashboard from "../shared/dashboard/Dashboard";
import { extractIdFromClassUrl } from '../../utils/helpers';
import { LearningResAccessComp } from './components/learning-res-access';
import { LearnerQuesAnsComp } from './components/learner-ques-ans';

export enum ROUTES {
    EMPTY = "",
    BASE = "/class/:class_name/classroom",
    RESOURCES = '/learning-resources'
};

const LearnerDashboardData: DrawerData = {
    base: ROUTES.BASE,
    lists: [
        {
            items: [
                {
                    url: ROUTES.EMPTY,
                    text: "Your Questions",
                    icon: <QuestionAnswerIcon />
                },
                {
                    url: ROUTES.RESOURCES,
                    text: "Learning Resources",
                    icon: <BookIcon />
                }
            ]
        }
    ]
}

const LearnerClassroom: React.FC<{classId:number}> = ({classId}) => {

    //Here we need to
    const {params,url} = useRouteMatch();
    const history = useHistory();
    //console.log("UDII",history.location.pathname);
    var currentTabUrl = history.location.pathname.split('/').slice(4,).join();
    if(currentTabUrl!='') currentTabUrl = '/'+currentTabUrl;

    const handleTabChange = (newTabUrl) => {
        //console.log("URL ",url);
        //console.log("Hello World",newTabUrl);
        history.push(url+newTabUrl);
    }
    

    return (
        <Dashboard drawerData={LearnerDashboardData} currentTabUrl = {currentTabUrl} onTabChange = {handleTabChange} classId= {classId}>
            <Switch>
                <Route exact path={ROUTES.BASE}>
                    <LearnerQuesAnsComp classId={classId} />
                </Route>
                <Route path={ROUTES.BASE + ROUTES.RESOURCES}>
                    <LearningResAccessComp classId={classId} />
                </Route>
            </Switch>
        </Dashboard>
    )
}

export default LearnerClassroom;
