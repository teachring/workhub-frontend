import { learnerPostQuestion, learnerFetchQuestions } from '../../../../utils/apis/ques-ans';
import { fetchLearnerAccessToken } from '../../../../utils/apis/learners';
import { RootState } from '../../../../utils/reduxReducers';
import { QuesAnsObj } from '../../../../utils/core-types/shared';

export type LearnerRecvClassTokenAction = {
    type:'LEARNER_RECEIVE_CLASS_TOKEN',
    classId:number,
    token:string
}

export type LearnerRecvQusAnsAction = {
    type: 'LEARNER_RECEIVE_QUES_ANS',
    classId:number,
    quesAnsList: QuesAnsObj[] 
}

export type LearnerNewQuesAction = {
    type: 'LEARNER_ASK_NEW_QUES',
    classId:number,
    quesObj: QuesAnsObj
}



export const fetchQAIfNeeded = (classId:number) => {
    return async function(dispatch:(arg0:LearnerRecvQusAnsAction) => any,getState:()=>RootState) {

        if(!getState().LearnerState.ClassroomQuesAns[classId]){
            let quesAnsList = await learnerFetchQuestions(classId);
            dispatch({type:'LEARNER_RECEIVE_QUES_ANS',classId:classId,quesAnsList:quesAnsList})
        }
        return ;

    }

}

export const postNewQuestion = (classId:number,quesBody:string) => {
    return async function(dispatch:(arg0:LearnerRecvClassTokenAction | LearnerNewQuesAction ) => any, getState:()=>RootState) : Promise<boolean> {

        // var classToken = getState().LearnerState.ClassroomTokens[classId];
        // if(!classToken){
        //     classToken = await fetchLearnerAccessToken(classId)
        //     dispatch({type:'LEARNER_RECEIVE_CLASS_TOKEN',classId:classId,token:classToken})
        // }

        return learnerPostQuestion(classId,quesBody)
        .then((quesObj) => {
            dispatch({type: 'LEARNER_ASK_NEW_QUES',classId:classId,quesObj:quesObj})
            return true;
        },
        err=> {
            return false
        })   
    }

}
