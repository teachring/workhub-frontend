import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import { purple } from '@material-ui/core/colors';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            width: "100%",
            maxWidth: 700,
            margin: "auto",
            padding: "2rem"
        },
        paper: {
            margin: "2rem auto",
            padding: "2rem",
            display: "flex"
        },
        body: {
            paddingLeft: "1rem",
            textAlign: "left",
            flex: 1
        },
        details: {
            fontWeight: "lighter",
            "& p": {
                fontSize: "0.9rem",
            }
        },
        question: {
            textAlign: "left",
            fontSize: "1.4rem",
            marginBottom: "1rem",
        },
        answerBody: {
            fontSize: "1.1rem",
            fontWeight: "lighter",
            marginBottom: "1rem"
        },
        questionBox: {
            display: "flex",
            flexWrap: "wrap",
            alignItems: "center",
            background: "#fff",
            padding: "2rem",
            position: "relative",
            overflow: "hidden",
            "&:before": {
                content: "''",
                position: "absolute",
                top: 0,
                left: 0,
                background: "#9c28b0",
                width: 4,
                height: "100%",
            }
        },
        questionInput: {
            flex: 1,
            minWidth: 200,
            marginBottom: "1rem",
            marginRight: "2rem"
        },
        askQuestionBtn: {
            minWidth: 100
        },
    }),
);

export default useStyles;