import { RootState } from "../../../../utils/reduxReducers"
import {fetchQAIfNeeded,postNewQuestion} from './actions';
import LearnerQuesAns from './learner-ques-ans';
import { connect } from "react-redux";

const mapStateToProps = (state:RootState,ownProps:{classId:number}) => {
    return {
        // classInfo: state.ClassInfoList[ownProps.classId],
        quesAnsList: state.LearnerState.ClassroomQuesAns[ownProps.classId]
    }
}

const mapDispatchToProps = (dispatch:Function,ownProps:{classId:number}) => {
    return {
        fetchQAIfNeeded: () :void => {
            dispatch(fetchQAIfNeeded(ownProps.classId));
        },
        postNewQues:(quesBody:string) : Promise<boolean> => {
            return dispatch(postNewQuestion(ownProps.classId,quesBody));
        }
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(LearnerQuesAns);