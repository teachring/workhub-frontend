import React, { useEffect } from 'react';
import { QuesAnsObj } from '../../../../utils/core-types/shared';
import Heading from '../../../shared/heading/heading';
import { Paper, Box, Typography, TextField, Button, CircularProgress } from '@material-ui/core';
import { HourglassEmptyOutlined, DoneAllOutlined, ErrorOutlineOutlined } from '@material-ui/icons';
import { timeSince } from '../../../../utils/helpers';

import useStyles from "./learner-qa-styles";
import { green, orange, blue } from '@material-ui/core/colors';
import WaitingDialog from '../../../dialog-screens/waiting-dialog';
import EmptyState from '../../../shared/empty/empty';
import EmptyImg from "../../../../assets/images/no-educator-qa.svg";

type IProp = {
    quesAnsList?: QuesAnsObj[],
    fetchQAIfNeeded: () => void,
    postNewQues: (quesBody: string) => Promise<boolean>
}

const sortQ = (quesAnsList: QuesAnsObj[] | undefined): QuesAnsObj[] | undefined => {
    return quesAnsList?.sort((a: QuesAnsObj, b: QuesAnsObj) => {
        return a.askedDateTime > b.askedDateTime
            ? -1
            : (a.askedDateTime < b.askedDateTime ? 1 : 0)
    });
}

const LearnerQuesAns: React.FC<IProp> = ({ quesAnsList, fetchQAIfNeeded, postNewQues }) => {

    const classes = useStyles();

    const sortedQA = sortQ(quesAnsList);

    const [newQuestion, setNewQuestion] = React.useState("");
    const handleNewQuestion = (event: React.ChangeEvent<HTMLInputElement>) => {
        setNewQuestion(event.target.value);
    }

    // waiting dialog
    const [submitQuesDialogView, setSubmitQuesDialogView] = React.useState(false);
    const [networkWaiting, setNetworkWaiting] = React.useState(false);
    const handleSubmitQuesDialogClose = (confirm: boolean) => {
        if (confirm) {
            setNetworkWaiting(true);
            postNewQues(newQuestion)
                .then((isSuccess) => {
                    setNetworkWaiting(false);
                    setSubmitQuesDialogView(false);
                    // if (isSuccess) {
                    //     publishAlerts(AlertMessageType.SUCCESS_MESSAGE, "Draft Saved Successfully");
                    //     setNewQuestion("");
                    // } else {
                    //     publishAlerts(AlertMessageType.ERROR_MESSAGE, "Could not save draft , Please try again");
                    // }
                })

        } else {
            setSubmitQuesDialogView(false);
        }
    }

    const onSubmitQuesReq = () => {
        setSubmitQuesDialogView(true);
    }

    useEffect(() => {
        fetchQAIfNeeded();
    }, [])

    const card = (qa: QuesAnsObj) => {
        let askedOn = timeSince(qa.askedDateTime);
        if (askedOn === "few moments ago")
            askedOn = "asked " + askedOn;
        else askedOn = "asked on " + askedOn;

        return (
            <Paper key={qa.questionId} className={classes.paper} elevation={2}>
                <Box className={classes.body}>

                    <Typography className={classes.question}>{qa.questionBody}</Typography>

                    <Box display="flex" alignItems="flex-start">
                        {
                            qa.status === "PENDING" && (
                                <Box color={blue[500]} mt="3px">
                                    <HourglassEmptyOutlined color="inherit" fontSize="small" />
                                </Box>
                            )
                        }
                        {
                            qa.status === "CLOSED" && qa.answerBody && (
                                <Box color={green[500]} mt="3px">
                                    <DoneAllOutlined color="inherit" fontSize="small" />
                                </Box>
                            )
                        }
                        {
                            qa.status === "CLOSED" && !qa.answerBody && (
                                <Box color={orange[500]} mt="3px">
                                    <ErrorOutlineOutlined color="inherit" fontSize="small" />
                                </Box>
                            )
                        }

                        <Box ml={1}>
                            {
                                <Typography className={classes.answerBody}>
                                    {
                                        qa.answerBody
                                            ? qa.answerBody
                                            : qa.status === "CLOSED"
                                                ? "Closed without answer"
                                                : "Not answered yet"
                                    }
                                </Typography>
                            }

                            <Box className={classes.details}>
                                <Typography>{askedOn}</Typography>
                            </Box>
                        </Box>
                    </Box>

                </Box>
            </Paper>
        );
    }

    return (
        <Box className={classes.root}>
            <Heading>Ask Question</Heading>

            <Box my={3}>
                <Paper elevation={2} className={classes.questionBox}>
                    <TextField
                        className={classes.questionInput}
                        placeholder="Enter question"
                        value={newQuestion}
                        onChange={handleNewQuestion}
                        disabled={networkWaiting}
                        multiline
                        autoFocus
                    />
                    <Button
                        className={classes.askQuestionBtn}
                        disabled={networkWaiting || newQuestion.trim().length === 0}
                        onClick={onSubmitQuesReq}
                        variant="contained"
                        color="primary">ask</Button>
                </Paper>
            </Box>

            <WaitingDialog open={submitQuesDialogView} waiting={networkWaiting} onDialogClose={handleSubmitQuesDialogClose} dialogTitle='Confirm submission' okBtnText='OK' cancelBtnText='Cancel'>
                Your question will be submitted to the educator.
            </WaitingDialog>

            <Heading>Your Questions</Heading>

            <Box my={3}>
                {
                    sortedQA
                        ? sortedQA.length > 0
                            ? sortedQA.map(card)
                            : <EmptyState img="https://s3.ap-south-1.amazonaws.com/deploy.teachring.com/assets/images/no-educator-qa.svg" text="No quetions yet" />
                        : <CircularProgress color="primary" />
                }
            </Box>
        </Box>
    );
}

//Now when there is something, we need to modify things 

export default LearnerQuesAns