import { RootState } from "../../../../utils/reduxReducers";
import { fetchLearnerAccessToken } from "../../../../utils/apis/learners";
import { learnerFetchAllDocs } from '../../../../utils/apis/learning-resources';
import { fetchUserDetailsIfNeeded } from "../../../../utils/reduxActions/users-db";
import { LearnerRecvClassTokenAction } from "../learner-ques-ans/actions";
import { RecvLearningResAction } from "../../../educator-classroom/components/learning-res-mgt/actions";


export const fetchLearningResIfNeeded = (classId:number) => {
    
    return async function(dispatch:(arg0:LearnerRecvClassTokenAction | RecvLearningResAction | any) => any,getState:() => RootState) {
        if(!getState().ClassLearningRes[classId]) {

            // var classToken = getState().LearnerState.ClassroomTokens[classId];
            // if(!classToken){
            //     classToken = await fetchLearnerAccessToken(classId)
            //     dispatch({type:'LEARNER_RECEIVE_CLASS_TOKEN',classId:classId,token:classToken})
            // }

            let resList = await learnerFetchAllDocs(classId);
            let userIds = resList.map(resObj=> {return resObj.createdBy})
            await dispatch(fetchUserDetailsIfNeeded(userIds));
            dispatch({type:'RECEIVE_LEARNING_RESOURCE',classId:classId,resList:resList})
        }
        return;
    }
}