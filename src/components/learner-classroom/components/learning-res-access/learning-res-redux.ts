import { RootState } from "../../../../utils/reduxReducers";
import { connect } from "react-redux";
import LearningResAccess from "./learning-res-access";
import { fetchLearningResIfNeeded } from './actions';

const mapStateToProps = (state:RootState,ownProps:{classId:number}) => {
    return {
        resList: state.ClassLearningRes[ownProps.classId]
    }
}

const mapDispatchToProps = (dispatch:Function,ownProps:{classId:number}) => {
    return {
        fetchLearningResIfNeeded:() => {
            dispatch(fetchLearningResIfNeeded(ownProps.classId));
        }
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(LearningResAccess);