import React, { useEffect } from 'react';
import { LearningRes } from "../../../../utils/core-types/shared"
import ResourceCard from '../../../shared/cards/ResourceCard';
import Heading from '../../../shared/heading/heading';
import { Box, CircularProgress } from '@material-ui/core';
import EmptyState from '../../../shared/empty/empty';
import EmptyImg from "../../../../assets/images/no-learning-res.svg";

type IProp = {
    resList?: LearningRes[],
    fetchLearningResIfNeeded: () => void
}

const LearningResAccess: React.FC<IProp> = ({ resList, fetchLearningResIfNeeded }) => {

    useEffect(() => {
        fetchLearningResIfNeeded();
    }, [])

    return <>
        <Heading ml="2rem">Learning Resources</Heading>
        <Box display="flex" flexWrap="wrap" alignItems="flex-start">
            {
                resList
                    ? resList.length > 0
                        ? resList?.map(resource => <ResourceCard resource={resource} />)
                        : <EmptyState img="https://s3.ap-south-1.amazonaws.com/deploy.teachring.com/assets/images/no-learning-res.svg" text="No materials yet" />
                    : <CircularProgress color="primary" />
            }
        </Box>
    </>;

}


export default LearningResAccess;