import React, { useEffect } from "react";
import { ClassInfo } from "../../utils/core-types/class-types";
import ClassCardBig from "../shared/cards/ClassCardBig";
import Heading from "../shared/heading/heading";
import ClassCardSmall from "../shared/cards/ClassCardSmall";
import { Box, SvgIcon, useTheme } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import { buildClassUrl } from "../../utils/helpers";

type IProp = {
    activeClasses: ClassInfo[],
    pastClasses: ClassInfo[],
    upcomingClasses: ClassInfo[]
}

const LearnerHome: React.FC<IProp> = ({ activeClasses, pastClasses, upcomingClasses }) => {

    const history = useHistory();
    const theme = useTheme();

    const navigateToClassDetails = (classInfoObj: ClassInfo) => {

        history.push('/class/' + buildClassUrl(classInfoObj.organizerName, classInfoObj.title, classInfoObj.classId));

    }

    const navigateToLearnerClassroom = (classInfoObj: ClassInfo) => {
        history.push('/class/' + buildClassUrl(classInfoObj.organizerName, classInfoObj.title, classInfoObj.classId) + '/classroom');
    }

    return (
        <>
            {
                activeClasses.length + pastClasses.length > 0
                    ? (
                        <>
                            <Heading ml="3rem">Your Classes</Heading>
                            <Box display="flex" flexWrap="wrap">
                                {
                                    activeClasses.map((classObj: ClassInfo) => (
                                        <ClassCardSmall key={classObj.classId} classObj={classObj} type="ACTIVE" onActionFired={() => {
                                            if (classObj.startDateTime > new Date()) {
                                                //not started yet..
                                                navigateToClassDetails(classObj);
                                            } else {
                                                navigateToLearnerClassroom(classObj);
                                            }
                                        }} />
                                    ))
                                }
                                {
                                    pastClasses.map((classObj: ClassInfo) => (
                                        <ClassCardSmall key={classObj.classId} classObj={classObj} type="PAST" onActionFired={
                                            () => { navigateToLearnerClassroom(classObj) }} />
                                    ))
                                }
                            </Box>
                        </>
                    ) : ""

            }

            <Heading ml="3rem">Explore all</Heading>
            <Box display="flex" flexWrap="wrap" justifyContent="flex-start">
                {
                    upcomingClasses.map((classObj: ClassInfo) => (
                        <ClassCardBig key={classObj.classId} classObj={classObj} type="ACTIVE" onActionFired={() => { navigateToClassDetails(classObj) }} />
                    ))
                }
            </Box>
        </>

    )
}

export default LearnerHome;