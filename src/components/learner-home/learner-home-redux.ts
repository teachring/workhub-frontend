import { RootState } from "../../utils/reduxReducers";
import { ClassInfo } from "../../utils/core-types/class-types";
import LearnerHome from "./learner-home";
import { connect } from "react-redux";


const mapStateToProps = (state:RootState) => {

    const registeredClasses:ClassInfo[] = Object.keys(state.LearnerState.RegisteredClassesState).map((classId) => state.ClassInfoList[classId]);

    const activeClasses:ClassInfo[] = [];
    const pastClasses:ClassInfo[] = [];

    registeredClasses.forEach((classObj) => {
        if(classObj.endDateTime > new Date()){
            activeClasses.push(classObj);
        } else {
            pastClasses.push(classObj);
        }
    })

    const upcomingClasses:ClassInfo[] = []

    Object.keys(state.ClassInfoList).forEach((classId) => {
        const classObj:ClassInfo  = state.ClassInfoList[classId];
        if((classObj.startDateTime > new Date()) && !registeredClasses.some((regClassObj) => {
            return regClassObj.classId == classObj.classId;
        })){
            upcomingClasses.push(classObj);
        }
    })


    return {
        activeClasses: activeClasses,
        pastClasses: pastClasses,
        upcomingClasses: upcomingClasses
    }
    
}

export default connect(mapStateToProps)(LearnerHome);