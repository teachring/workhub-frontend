import React from "react";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import { Box } from "@material-ui/core";
// import NotFoundImg from "../../../assets/images/404.svg";

const useStyles = makeStyles(() =>
    createStyles({
        img: {
            width: "100%",
            maxWidth: 404,
            margin: "2rem",
        },
    })
);


const NotFound: React.FC = () => {

    const classes = useStyles();

    return (
        <img className={classes.img} src="https://s3.ap-south-1.amazonaws.com/deploy.teachring.com/assets/images/404.svg" alt="404 image" />
    );
}

export default NotFound;