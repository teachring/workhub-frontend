import React from "react";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import { Paper, Typography, Chip, Box, Button, useTheme } from "@material-ui/core";
import { EventOutlined, LocationOnOutlined, ArrowForwardOutlined, MoneyOff } from "@material-ui/icons";
import { ClassInfo } from "../../../utils/core-types/class-types";
import { readDate, readTime } from "../../../utils/helpers";
import RupeeLight from "../../../assets/icons/RupeeLight";

interface CardStyleProps {
    bg_color: string;
    bg_gradient: string;
}

const useStyles = makeStyles<Theme, CardStyleProps>((theme: Theme) =>
    createStyles({
        root: {
            borderRadius: 8,
            padding: "2rem",
            width: "33%",
            boxSizing: "border-box",
            minWidth: 450,
            [theme.breakpoints.down("md")]: {
                width: "50%"
            },
            [theme.breakpoints.down("xs")]: {
                padding: 0,
                width: "100%",
                minWidth: "auto",
                marginTop: "2rem",
                marginBottom: "1rem"
            }
        },
        cardHead: {
            background: props => props.bg_gradient,
            color: "#fff",
            textAlign: "left",
            padding: "2rem",
            "& .MuiTypography-root": {
                color: "#fff"
            }
        },
        title: {
            fontSize: "2rem"
        },
        subtitleTop: {},
        subtitleBottom: {
            marginTop: 10
        },
        cardBody: {
            display: "flex",
            flexWrap: "wrap",
            padding: "2rem",
        },
        paper: {
            overflow: "hidden",
            boxShadow: theme.shadows[2],
            "&:hover": {
                boxShadow: theme.shadows[3]
            },
            [theme.breakpoints.down("xs")]: {
                borderRadius: 0
            }
        },
        chip: {
            borderColor: "#fff",
            marginTop: 20,
            background: "#fff",
            color: props => props.bg_color,
            padding: "1rem",
            fontSize: "0.9rem",
            // fontWeight: "lighter",
            "& .MuiChip-icon": {
                fontSize: "1rem",
                color: "inherit"
            }
        },
        cardActionBtn: {
            marginLeft: "auto",
            padding: "10px 15px",
            borderRadius: 0,
            backgroundColor: "#f3f3f3",
            "& svg": {
                marginLeft: 10
            }
        },
        dateTimeBox: {
            width: "100%",
            display: "flex",
            flexWrap: "wrap",
            alignItems: "center",
            marginBottom: 20
        },
        dateTime: {
            textAlign: "left",
            // marginRight: 70,
            position: "relative"
        },
        dash: {
            margin: "0 25px",
            width: 9,
            height: 2,
        },
        icon: {
            marginRight: 20
        },
        [theme.breakpoints.down("xs")]: {
            dash: {
                display: "none"
            },
            dateTime: {
                "&:last-child": {
                    flex: "100%",
                    marginLeft: 45,
                    marginTop: 13,
                }
            }
        }
    })
);

interface IProp {
    classObj: ClassInfo,
    type: "ACTIVE" | "PAST" | "UPCOMING",
    onActionFired: () => void
}

const ClassCardBig: React.FC<IProp> = ({ classObj, type, onActionFired }) => {

    const bg_gradient = classObj.bgColor;
    const bg_color = bg_gradient.substring(bg_gradient.indexOf("#"), bg_gradient.indexOf("#") + 7);

    const classes = useStyles({ bg_gradient, bg_color });

    return (
        <Box className={classes.root} onClick={console.log}>
            <Paper elevation={0} className={classes.paper}>
                <Box className={classes.cardHead}>
                    <Typography className={classes.subtitleTop} variant="subtitle1">{classObj.organizerName}</Typography>
                    <Typography className={classes.title}>{classObj.title}</Typography>
                    <Typography className={classes.subtitleBottom} variant="subtitle1">
                        {
                            type === "ACTIVE" ? "" : ""
                        }
                    </Typography>



                    {
                        classObj.isPaid
                            ? <Chip className={classes.chip} icon={<RupeeLight />} label={classObj.paymentDetails?.feeAmt} variant="outlined" />
                            : <Chip className={classes.chip} label={"FREE"} variant="outlined" />
                    }

                </Box>

                <Box className={classes.cardBody}>
                    <Box className={classes.dateTimeBox}>
                        <EventOutlined className={classes.icon} />
                        <Box className={classes.dateTime}>
                            <Typography>{readDate(classObj.startDateTime)}</Typography>
                            <Typography>{readTime(classObj.startDateTime)}</Typography>
                        </Box>
                        <span className={classes.dash} />
                        <Box className={classes.dateTime}>
                            <Typography>{readDate(classObj.endDateTime)}</Typography>
                            <Typography>{readTime(classObj.endDateTime)}</Typography>
                        </Box>
                    </Box>

                    <Box className={classes.dateTimeBox}>
                        <LocationOnOutlined className={classes.icon} /> <Typography>{classObj.venue}</Typography>
                    </Box>
                    <Button className={classes.cardActionBtn} onClick={(e) => onActionFired()}>Know more <ArrowForwardOutlined /></Button>
                </Box>
            </Paper>
        </Box>
    )
}

export default ClassCardBig;