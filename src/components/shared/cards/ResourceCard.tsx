import React from 'react';
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import { Paper, Box, Typography, IconButton } from '@material-ui/core';
import { Description, Face, ExpandLess, ExpandMore } from '@material-ui/icons';
import { purple } from '@material-ui/core/colors';
import clsx from "clsx";

import { LearningRes } from "../../../utils/core-types/shared";
import { User } from '../../../utils/core-types/user-types';
import { timeSince } from '../../../utils/helpers';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        paper: {
            maxWidth: "20rem",
            padding: "1.3rem",
            margin: "2rem",
            textAlign: "left",
            position: "relative",
            overflow: "hidden",
            paddingLeft: "2rem",
            width: "100%",
            boxShadow: theme.shadows[2],
            "&:after": {
                content: "''",
                position: "absolute",
                top: 0,
                left: 0,
                backgroundColor: purple[500],
                height: "100%",
                width: 5
            },

            "&[data-expand='true']": {
                maxWidth: "100%",
                "& .res-thumbnail": {
                    overflow: "initial",
                    fontSize: "inherit",
                    paddingBottom: "1rem",
                    height: "auto"
                }
            }
        },
        title: {
            textAlign: "left",
            paddingLeft: 10,
            fontSize: "1.5rem",
            fontWeight: "bold",
            fontFamily: "Roboto Slab"
        },
        subtitle: {
            paddingLeft: 10
        },
        thumbnail: {
            height: 150,
            overflow: "hidden",
            fontSize: "0.6rem",
            color: "inherit",
            padding: "0 10px 10px 10px",
            marginTop: "1rem",
            paddingTop: "1rem"
        },
        expandBtn: {
            // border: "1px solid " + theme.palette.primary.main
        },
    })
);

type IProp = {
    resource: LearningRes,
    getUserDetails?: (userId: number) => User,
}

const ResourceCard: React.FC<IProp> = ({ resource, getUserDetails }) => {

    const classes = useStyles();

    const [expand, setExpand] = React.useState({});

    const handleExpand = (resId) => {
        console.log(resId)
        setExpand(prevState => {
            return {
                ...prevState,
                [resId]: !prevState[resId]
            }
        })
    }


    return (
        <Paper
            key={resource.resourceId}
            className={classes.paper} data-expand={expand[resource.resourceId]}
        >
            <Box display="flex" alignItems="flex-start" justifyContent="space-between">
                <Box>
                    <Box display="flex">
                        <Description color="primary" fontSize="small" style={{ position: "relative", top: 7 }} /> <Typography className={classes.title}>{resource.title}</Typography>
                    </Box>
                    {getUserDetails && (
                        <Box display="flex" my={1}>
                            <Face color="primary" fontSize="small" /> <Typography className={classes.subtitle}>{getUserDetails(resource.createdBy)?.userName}</Typography>
                        </Box>
                    )}
                    <Box paddingLeft="20px">
                        <Typography className={classes.subtitle}>{timeSince(resource.creationDateTime)}</Typography>
                    </Box>

                </Box>
                <IconButton className={classes.expandBtn} onClick={() => handleExpand(resource.resourceId)}>
                    {expand[resource.resourceId] ? <ExpandLess color="primary" /> : <ExpandMore color="primary" />}
                </IconButton>
            </Box>
            <Paper className={clsx(classes.thumbnail, "res-thumbnail")} elevation={1} dangerouslySetInnerHTML={{ __html: resource.body }} />

        </Paper>
    );
}

export default ResourceCard;