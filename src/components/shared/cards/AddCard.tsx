import React from "react";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import { Paper, Typography, Box, useTheme } from "@material-ui/core";
import {
    CreateOutlined,
    AddOutlined,
    RadioButtonCheckedOutlined,
    TimelapseOutlined,
    CheckCircleOutlineOutlined
} from "@material-ui/icons";
import { deepOrange, green, blue, purple } from "@material-ui/core/colors";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            width: "33%",
            padding: "2rem 0",
            boxSizing: "border-box",
            borderRadius: 5,
            overflow: "hidden",
            cursor: "pointer",
            [theme.breakpoints.down("sm")]: {
                width: "50%"
            },
            [theme.breakpoints.down("xs")]: {
                width: "100%"
            }
        },
        title: {
            fontSize: "1.3rem",
            paddingTop: 10,
            paddingBottom: 20,
        },
        cardBody: {
            padding: "2rem",
            display: "flex",
            flexDirection: "column",
            flexWrap: "wrap",
            justifyContent: "center",
            alignItems: "center",
            boxSizing: "border-box",
            height: "100%",
            boxShadow: theme.shadows[2],
            margin: "0 2rem",
            "&:hover": {
                boxShadow: theme.shadows[3]
            },
        },
        addIcon: {
            fontSize: "2rem"
        }
    })
);

interface IProp {
    text: string,
    onActionFired: () => void
}

const AddCard: React.FC<IProp> = ({ text, onActionFired }) => {

    const classes = useStyles();

    return (
        <Box className={classes.root} onClick={onActionFired}>
            <Paper className={classes.cardBody}>
                <AddOutlined className={classes.addIcon} />
                <Typography className={classes.title}>{text}</Typography>
            </Paper>
        </Box>
    );
}

export default AddCard;