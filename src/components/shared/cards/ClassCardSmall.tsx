import React from "react";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import { Paper, Typography, Box } from "@material-ui/core";
import {
    CreateOutlined,
    RadioButtonCheckedOutlined,
    TimelapseOutlined,
    CheckCircleOutlineOutlined
} from "@material-ui/icons";
import { deepOrange, green, blue, purple } from "@material-ui/core/colors";
import { SavedDraftInfo } from "../../../utils/core-types/educator-types";
import { ClassInfo } from "../../../utils/core-types/class-types";
import { readDateTime } from "../../../utils/helpers";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            width: "33%",
            padding: "2rem",
            boxSizing: "border-box",
            borderRadius: 5,
            overflow: "hidden",
            [theme.breakpoints.down("sm")]: {
                width: "50%"
            },
            [theme.breakpoints.down("xs")]: {
                width: "100%"
            }
        },
        title: {
            fontSize: "1.3rem",
            paddingTop: 10,
            paddingBottom: 20,
        },
        cardBody: {
            textAlign: "left",
            padding: "2rem",
            display: "flex",
            cursor: "pointer",
            flexDirection: "column",
            flexWrap: "wrap",
            justifyContent: "space-between",
            boxSizing: "border-box",
            height: "100%",
            boxShadow: theme.shadows[2],
            "&:hover": {
                boxShadow: theme.shadows[3]
            },
        },
        iconText: {
            display: "flex",
            marginTop: "1rem",
            "& svg": {
                fontSize: "1.34rem",
                marginRight: 8,
            }
        },
        draft: { color: deepOrange[500] },
        published: { color: green[500] },
        active: { color: blue[500] },
        past: { color: purple[500] }
    })
);

interface IProp {
    classObj: SavedDraftInfo | ClassInfo,
    type: "DRAFT" | "PUBLISHED" | "ACTIVE" | "PAST",
    onActionFired?: () => void
}

const ClassCardSmall: React.FC<IProp> = ({ classObj, type, onActionFired }) => {

    const classes = useStyles();

    return (
        <Box className={classes.root}>
            <Paper className={classes.cardBody} onClick={(e) => onActionFired && onActionFired()}>
                <div>
                    <Typography variant="subtitle1">{classObj.organizerName}</Typography>
                    <Typography className={classes.title}>{classObj.title}</Typography>
                </div>

                <Box className={classes.iconText} display="flex">
                    {
                        type === "DRAFT"
                            ? <><CreateOutlined className={classes.draft} /> <Typography>Continue editing</Typography></>
                            : ""
                    }
                    {
                        type === "ACTIVE" && new Date() < classObj.regStartDateTime!
                            ? <>
                                <RadioButtonCheckedOutlined className={classes.published} />
                                <Typography>Registration starts on {readDateTime(classObj.regStartDateTime!)}</Typography>
                            </>
                            : ""
                    }
                    {
                        type === "ACTIVE" && new Date() >= classObj.regStartDateTime!
                            ? <>
                                <TimelapseOutlined className={classes.active} />
                                <Typography>
                                    {
                                        new Date() < classObj.startDateTime!
                                            ? "Class starts on " + readDateTime(classObj.startDateTime!)
                                            : "Class ends on " + readDateTime(classObj.endDateTime!)
                                    }
                                </Typography>
                            </>
                            : ""
                    }
                    {
                        type === "PAST"
                            ? <><CheckCircleOutlineOutlined className={classes.past} /> <Typography>Ended on Jan 20, 9 PM</Typography></>
                            : ""
                    }
                </Box>
            </Paper>
        </Box>
    )
}

export default ClassCardSmall;