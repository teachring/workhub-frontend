import React, { useState, useEffect } from "react";
import { AppBar, Toolbar, Hidden, Typography, IconButton, Badge, Button, Box } from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import NotificationsIcon from "@material-ui/icons/Notifications";
import useStyles from './top-app-bar-styles';
import { MenuComp } from './components/top-app-bar-menu';
import { SideNavDrawerComp } from "./components/side-nav-drawer";
import { useHistory, useLocation } from "react-router-dom";
import theme from "../../../assets/theme/muiTheme";
import { MoreVert } from "@material-ui/icons";

export enum AvlNavPaths {
    NOTIFICATIONS = "/notifications",
    FOR_EDUCATORS = "/educator",
    HOME = "/"
}

type IProp = {
    isSignedIn: boolean,
    username?: string,
    notificationCount?: number,
    showLoginSignupWin: (isLoginTab?: boolean, onSuccessAction?: () => void) => void,
    onUserLogout: (onSuccessAction: () => void) => void
}

const TopAppBar: React.FC<IProp> = ({ isSignedIn, username, notificationCount, showLoginSignupWin, onUserLogout }) => {

    // router hook
    const history = useHistory();

    let background = theme.palette.background.default;
    const currentPath = history.location.pathname;

    if (currentPath === "/" || currentPath === "/browse" || currentPath.startsWith("/educator/create-class"))
        background = theme.palette.background.paper;

    const classes = useStyles({ background });

    console.log({ history })

    const onNavRequest = (navPath: AvlNavPaths) => {
        history.push(navPath);
    }

    const handleNavReq = (navRequest: AvlNavPaths) => {
        if (!isSignedIn && navRequest == AvlNavPaths.FOR_EDUCATORS) {
            showLoginSignupWin(undefined, () => { history.push('/educator') });
        } else {
            onNavRequest(navRequest);
        }
    }

    const handleLogoutRequest = () => {
        console.log("Logout request received!");
        onUserLogout(() => { history.push('/') });
    }

    return (
        <div className={classes.wrapperDiv}>
            <AppBar className={classes.appBar}>
                <Toolbar className={classes.cssToolbarMixin}>
                    <Box className={classes.title}>
                        <Typography variant="h6" className={classes.brand}>Teachring</Typography>
                    </Box>
                    {
                        isSignedIn
                            ? <>
                                <React.Fragment >
                                    <Box mr={3}>
                                        <IconButton
                                            className={classes.menuButton}
                                            edge="end"
                                            aria-label="user-notifications"
                                        >
                                            <Badge badgeContent={notificationCount} color="secondary" variant="dot">
                                                <NotificationsIcon color="primary" fontSize="small" />
                                            </Badge>
                                        </IconButton>
                                    </Box>
                                    <MenuComp username={username} handleLogoutRequest={handleLogoutRequest} />
                                </React.Fragment>
                            </>
                            : <>
                                <Hidden smUp>
                                    <MenuComp showLoginSignupWin={showLoginSignupWin} handleNavReq={handleNavReq} />
                                </Hidden>
                                <Hidden xsDown>
                                    <Box mr={2}>
                                        <Button
                                            className={classes.textButton}
                                            variant="text"
                                            classes={{ text: classes.text }}
                                            onClick={() => handleNavReq(AvlNavPaths.FOR_EDUCATORS)}
                                        >
                                            For Educators
                                        </Button>
                                    </Box>
                                    <Button
                                        classes={{ text: classes.text }}
                                        className={classes.containedButton}
                                        variant="contained"
                                        onClick={() => showLoginSignupWin()}
                                    >
                                        Signup / Login
                                    </Button>
                                </Hidden>
                            </>
                    }
                </Toolbar>
            </AppBar>
        </div>
    );

}

export default TopAppBar;
