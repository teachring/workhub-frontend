import React, { useState } from 'react';
import { Box, IconButton, Typography, Menu, MenuItem, ListItemIcon } from "@material-ui/core";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles"
import NotificationsNoneIcon from '@material-ui/icons/NotificationsNone';
import PowerSettingsNewIcon from '@material-ui/icons/PowerSettingsNew';
import { MoreVert, Face, School } from '@material-ui/icons';
import { useHistory } from 'react-router-dom';
import { AvlNavPaths } from '../../top-app-bar';

const useClasses = makeStyles((theme: Theme) =>
    createStyles({
        childItem: {
            paddingRight: 4
        },
        menuPaper: {
            borderRadius: 6,
            boxShadow: "rgb(230, 230, 230) 0px 3px 14px -1px",
        },
        menuList: {
            padding: 0
        },
        menuItem: {
            padding: "1rem 2rem",
            borderBottom: "1px solid #eee",
            color: "#484848",
            "& ..MuiListItemIcon-root": {

            }
        },
        icon: {
            minWidth: "auto",
            marginRight: "1rem"
        }
    })
);

enum UserMenuBarOptions {
    LOGOUT,
    NOTIFICATIONS,
    FOR_EDUCATOR,
    LOGIN
}


type IProp = {
    username?: string,
    handleLogoutRequest?: () => void,
    showLoginSignupWin?: (isLoginTab?: boolean, onSuccessAction?: () => void) => void,
    handleNavReq?: (navPath: AvlNavPaths) => void
}


const MenuComp: React.FC<IProp> = ({ username, handleLogoutRequest, showLoginSignupWin, handleNavReq }) => {
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

    const history = useHistory();

    const handleClick = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null)
    }

    const menuItemSelected = (itemSelected: UserMenuBarOptions) => {
        handleClose();
        switch (itemSelected) {
            case UserMenuBarOptions.LOGOUT: {
                handleLogoutRequest && handleLogoutRequest();
                break;
            }
            case UserMenuBarOptions.NOTIFICATIONS: {
                history.push('/notifications');
                break;
            }
            case UserMenuBarOptions.FOR_EDUCATOR: {
                handleNavReq && handleNavReq(AvlNavPaths.FOR_EDUCATORS)
                break;
            }
            case UserMenuBarOptions.LOGIN: {
                showLoginSignupWin && showLoginSignupWin()
                break;
            }
        }

    }


    const classes = useClasses();

    return (
        <React.Fragment>
            <Box display="flex" alignItems="center" alignSelf="stretch" onClick={(e) => { handleClick(e) }}>
                <Typography variant="subtitle1" className={classes.childItem}>{username}</Typography>
                <IconButton edge="end">
                    <MoreVert color={username ? "primary" : "action"} />
                </IconButton>
            </Box>
            <Menu
                elevation={2}
                getContentAnchorEl={null}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'center'
                }}
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'center'
                }}
                id="account-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
                classes={{ paper: classes.menuPaper, list: classes.menuList }}
            >
                {
                    username
                        ? <div>
                            <MenuItem className={classes.menuItem} onClick={() => menuItemSelected(UserMenuBarOptions.NOTIFICATIONS)}>
                                <ListItemIcon className={classes.icon}><NotificationsNoneIcon color="primary" fontSize="small" /></ListItemIcon>
                                <Typography>Notifications</Typography>
                            </MenuItem>
                            <MenuItem className={classes.menuItem} onClick={() => menuItemSelected(UserMenuBarOptions.LOGOUT)}>
                                <ListItemIcon className={classes.icon}><PowerSettingsNewIcon color="primary" fontSize="small" /></ListItemIcon>
                                <Typography>Logout</Typography>
                            </MenuItem>
                        </div>
                        : <div>
                            <MenuItem className={classes.menuItem} onClick={() => { menuItemSelected(UserMenuBarOptions.FOR_EDUCATOR) }}>
                                <ListItemIcon className={classes.icon}><School color="action" fontSize="small" /></ListItemIcon>
                                <Typography>For Educator</Typography>
                            </MenuItem>
                            <MenuItem className={classes.menuItem} onClick={() => { menuItemSelected(UserMenuBarOptions.LOGIN) }}>
                                <ListItemIcon className={classes.icon}><Face color="action" fontSize="small" /></ListItemIcon>
                                <Typography>Login</Typography>
                            </MenuItem>
                        </div>
                }

            </Menu>
        </React.Fragment>

    );
}

export default MenuComp;