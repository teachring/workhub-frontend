import { connect } from "react-redux";
import SignedInMenu from "./top-app-bar-menu"
import { RootState } from "../../../../../utils/reduxReducers";
import { onAppLogout } from "../../../../App/actions";


const mapStateToProps = (state: RootState, ownProps: { username?: string }) => {
    return {
        username: ownProps.username
    }
}

const mapDispatchToProps = (dispatch: Function) => {

    return {
        onUserLogoutReq: (onSuccessAction: () => void) => {
            dispatch(onAppLogout(onSuccessAction));
        }
    }

}

export default connect(mapStateToProps, mapDispatchToProps)(SignedInMenu);