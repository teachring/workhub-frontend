import React, { useState } from 'react';
import { IconButton, Drawer, List, ListItem, Box, Typography, Button, useTheme } from "@material-ui/core";
import AccountCircle from "@material-ui/icons/AccountCircle"
import useClasses from "./side-nav-drawer-styles";
import MenuIcon from "@material-ui/icons/Menu";
import { AvlNavPaths } from "../../top-app-bar";


type IProp = {
    isSignedIn: boolean,
    onNavRequest: (navPath: AvlNavPaths) => void,
    userName?: string,
    onLogoutRequest: () => void,
    onLoginSignupReq: (isLoginTab: boolean) => void,
}


const SideNavDrawer: React.FC<IProp> = ({ isSignedIn, onNavRequest, onLogoutRequest, userName, onLoginSignupReq }) => {

    const classes = useClasses();
    const theme = useTheme();

    const [drawerOpen, setDrawerOpen] = useState(false);
    // const [signInModalState, setSignInModalState] = useState({ isLoginTab: false, open: false });

    const toggleDrawer = (open: boolean) => {
        setDrawerOpen(open);
    }

    const handleNavReq = (navRequest: AvlNavPaths) => {
        toggleDrawer(false);
        if (!isSignedIn && navRequest == AvlNavPaths.FOR_EDUCATORS) {
            onLoginSignupReq(false);
        } else {
            onNavRequest(navRequest);
        }
    }


    const getSignedInList = () => {
        return (
            <List disablePadding={true}>
                <ListItem key={"signin-title"} divider={true} classes={{ root: classes.signInTitle }}>
                    <Box py={2.5} display="flex" alignItems="center">
                        <AccountCircle fontSize="large" color="primary" />
                        <Box display="flex" flexDirection="column">
                            {/* TODO: Hardcoded name??? */}
                            <Typography variant="subtitle1" noWrap>Welcome,</Typography>
                            <Typography variant="subtitle1" noWrap>Udit Agarwal</Typography>
                        </Box>
                    </Box>
                </ListItem>
                <ListItem button onClick={(e) => handleNavReq(AvlNavPaths.HOME)}>Home</ListItem>
                <ListItem button onClick={(e) => handleNavReq(AvlNavPaths.NOTIFICATIONS)}>Notification</ListItem>
                <ListItem button onClick={(e) => handleNavReq(AvlNavPaths.FOR_EDUCATORS)}>For Educators</ListItem>
                <ListItem button onClick={(e) => {
                    toggleDrawer(false);
                    onLogoutRequest()
                }}>
                    Logout
                </ListItem>
            </List>
        );
    }

    function getNotSignedInList() {
        return (
            <React.Fragment>
                <List>
                    <ListItem button
                        onClick={(e) => handleNavReq(AvlNavPaths.HOME)}>
                        Home
                    </ListItem>
                    <ListItem button divider
                        onClick={(e) => handleNavReq(AvlNavPaths.FOR_EDUCATORS)}>
                        For Educators
                    </ListItem>
                </List>
                <Box flexGrow="1" display="flex" flexDirection="column" alignItems="stretch" mt={4} px={2}>
                    <Button className={classes.loginSignupBtn} variant="contained" color="primary" size="large"
                        onClick={(e) => {
                            toggleDrawer(false);
                            onLoginSignupReq(false)
                        }}>
                        Join for Free
                    </Button>
                    <Button className={classes.loginSignupBtn} variant="outlined" color="primary" size="large"
                        onClick={(e) => {
                            toggleDrawer(false);
                            onLoginSignupReq(true)
                        }}>
                        Login
                    </Button>
                </Box>
            </React.Fragment>
        );
    }

    return (
        <React.Fragment>
            <IconButton
                edge="start"
                className={classes.menuButton}
                // color={theme.palette.text.primary}
                aria-label="menu"
                onClick={(e) => toggleDrawer(true)}
            >
                <MenuIcon />
            </IconButton>
            <Drawer open={drawerOpen} onClose={(e) => toggleDrawer(false)}>
                <div className={classes.drawerBody}>
                    {isSignedIn ? getSignedInList() : getNotSignedInList()}
                </div>
            </Drawer>

        </React.Fragment>

    );
}

export default SideNavDrawer;