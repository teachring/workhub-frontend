import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
const useClasses = makeStyles((theme: Theme) =>
  createStyles({

    menuButton: {
      marginRight: theme.spacing(1),
      marginLeft: theme.spacing(1)
    },

    drawerBody : {
      width: 220,
      display:"flex",
      flexDirection: "column",
      height:"100%"
    },
    signInTitle : {
      backgroundColor: theme.palette.grey[200]
    },
    navBarListItem : {
      paddingTop : theme.spacing(2),
      paddingBottom : theme.spacing(2),
      paddingLeft: theme.spacing(2)
    },
    loginSignupBtn : {
      marginTop: theme.spacing(2),
      padding: "auto",
      textTransform: "capitalize"
    },
  })
);

export default useClasses;