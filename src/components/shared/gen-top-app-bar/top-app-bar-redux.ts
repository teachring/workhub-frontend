import { connect } from "react-redux";
import TopAppBar from "./top-app-bar";
import { RootState } from "../../../utils/reduxReducers";
import { LoginSignupWinShowAction } from "../../login-signup/actions";
import {onAppLogout} from '../../App/actions';
    
const mapStateToProps = (state: RootState) => {
    return {
        isSignedIn: state.AppState.isLoggedIn,
        username: state.AppState.userName,
        notificationCount: state.NotificationData.notificationList.filter(item => !item.isRead).length
    }
}

const mapDispatchToProps = (dispatch:Function) => {
    return {
        showLoginSignupWin: (isLoginTab:boolean = false, onSuccessAction?:() => void) => {
            const actionObj:LoginSignupWinShowAction = {type:'LOGIN_SIGNUP_WINDOW_SHOW',isLoginTab: isLoginTab,onSuccessAction:onSuccessAction}
            dispatch(actionObj);
        },
        onUserLogout: (onSuccessAction: () => void) => {
            dispatch(onAppLogout(onSuccessAction));
        }
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(TopAppBar);