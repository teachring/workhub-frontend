import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";

interface TopAppBarStyleProps {
    background: string
}

const useClasses = makeStyles<Theme, TopAppBarStyleProps>((theme: Theme) =>
    createStyles({
        wrapperDiv: {
            minHeight: 56,
            [`${theme.breakpoints.up('xs')} and (orientation: landscape)`]: {
                minHeight: 48
            },
            [theme.breakpoints.up('sm')]: {
                minHeight: 64,
            }
        },
        brand: {
            fontFamily: "Montserrat Alternates, Nunito",
            fontWeight: "bold",
            fontSize: "1.2rem",
            letterSpacing: 1
        },
        text: {
            textTransform: 'capitalize',
            fontSize: theme.spacing(2),
        },
        textButton: {
            color: "#6259ff"
        },
        containedButton: {
            backgroundColor: "#6259ff",
            color: "#fff",
            "&:hover": {
                backgroundColor: "#5b51ff",
                boxShadow: "0 6px 13px -2px rgba(107, 99, 255, 0.3)"
            }
        },
        appBar: {
            background: props => props.background,
            boxShadow: "none",
        },
        cssToolbarMixin: {
            minHeight: 56,
            [`${theme.breakpoints.up('xs')} and (orientation: landscape)`]: {
                minHeight: 48
            },
            [theme.breakpoints.up('sm')]: {
                minHeight: 64,
            }
        },
        title: {
            flexGrow: 1,
            display: "flex",
            justifyContent: "flex-start"
        }
    })
);

export default useClasses;