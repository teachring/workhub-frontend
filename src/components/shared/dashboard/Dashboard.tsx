import React, { useEffect } from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import { useHistory, useRouteMatch } from 'react-router-dom'

import { DrawerData, DrawerItem, DrawerList } from '../../../utils/core-types/shared';
import DashboardDrawer from "./DashboardDrawer";
import {DashboardAppBarComp} from "./dashboard-app-bar";


const useStyles = makeStyles(() =>
    createStyles({
        root: {
            display: 'flex',
        },
        content: {
            flexGrow: 1,
            padding: 0,
            width: "100vw"
        },
    }),
);


interface IProp {
    drawerData: DrawerData,
    currentTabUrl:string,
    onTabChange: (newTabUrl) => void,
    classId: number
    //Now when the state changes, everything changes
}

const Dashboard: React.FC<IProp> = ({ children, drawerData,currentTabUrl,onTabChange,classId}) => {

    useEffect(() => {
       console.log("Current Url changed ",currentTabUrl);
    }, [currentTabUrl])

    const classes = useStyles();
    const [mobileOpen, setMobileOpen] = React.useState(false);

    const handleDrawerToggle = () => {
        setMobileOpen(!mobileOpen);
    };


    const handleDrawerChange = route => {
        // history.push(url + route);
        // current = route;
        mobileOpen && handleDrawerToggle();
        onTabChange(route);
    }

    return (
        <div className={classes.root}>
            <CssBaseline />

            <DashboardAppBarComp handleDrawerToggle={handleDrawerToggle} classId= {classId} />

            <DashboardDrawer
                open={mobileOpen}
                current={currentTabUrl}
                drawerData={drawerData}
                handleDrawerToggle={handleDrawerToggle}
                handleDrawerChange={handleDrawerChange}
            />

            <main className={classes.content}>
                {children}
            </main>
        </div>
    );
}

export default Dashboard;
