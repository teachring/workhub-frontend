import React, { useEffect } from 'react';
import { makeStyles, useTheme, Theme, createStyles } from '@material-ui/core/styles';
import { List, ListItem, ListItemText, Typography, ListItemIcon, Icon, Hidden, Drawer } from '@material-ui/core';

import { drawerWidth } from '../../../assets/theme/muiTheme';
import { DrawerData, DrawerList, DrawerItem } from '../../../utils/core-types/shared';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            "& .MuiList-root": {
                padding: 0,
                "& .MuiListItemIcon-root": {
                    color: "inherit"
                }
            },
            "& .MuiTypography-root": {
                color: "#fff"
            },
            "& .MuiListItem-root": {
                padding: 15,
                "&.Mui-selected": {
                    backgroundColor: "#9c27b0",
                    boxShadow: "0 4px 20px 0px rgba(0, 0, 0, 0.14), 0 7px 12px -5px rgba(156, 39, 176, 0.46)",
                    borderRadius: 4
                }
            }
        },
        brand: {
            fontFamily: "Montserrat Alternates, Nunito",
            fontWeight: "bold",
            fontSize: "1.1rem",
            letterSpacing: 1
        },
        drawer: {
            [theme.breakpoints.up('sm')]: {
                width: drawerWidth,
                flexShrink: 0,
            },
        },
        listItemIcon: {
            minWidth: 40,
            "&>svg": {
                fontSize: "1.3rem"
            }
        }
    }),
);

type IProp = {
    open: boolean,
    current: string,
    drawerData: DrawerData,
    handleDrawerToggle: () => void,
    handleDrawerChange: (route: string) => void
}

const DashboardDrawer: React.FC<IProp> = ({ open, current, drawerData, handleDrawerToggle, handleDrawerChange }) => {
    const classes = useStyles();
    const theme = useTheme();

    useEffect(() => {
        console.log("Drawer Rerendered ", current);
    })

    const drawer = (
        <div className={classes.root}>
            <List>
                <ListItem button>
                    <ListItemText className={classes.brand}>
                        <Typography className={classes.brand} variant="h6">Teachring</Typography>
                    </ListItemText>
                </ListItem>
            </List>

            {
                drawerData.lists.map((drawerList: DrawerList, i) => (
                    <List key={i}>
                        {
                            drawerList.heading && (
                                <ListItem>
                                    <ListItemText><Typography variant="h6">{drawerList.heading}</Typography></ListItemText>
                                </ListItem>
                            )
                        }
                        {
                            drawerList.items?.map((drawerItem: DrawerItem, j) => (
                                <ListItem key={j} button onClick={() => handleDrawerChange(drawerItem.url)} selected={current === drawerItem.url}>
                                    {
                                        drawerItem.icon && (
                                            <ListItemIcon className={classes.listItemIcon}>
                                                {drawerItem.icon}
                                            </ListItemIcon>
                                        )
                                    }
                                    <ListItemText primary={drawerItem.text} />
                                </ListItem>
                            ))
                        }
                    </List>
                ))
            }
        </div >
    );

    return (
        <nav className={classes.drawer} aria-label="educator classroom">
            <Hidden smUp implementation="css">
                <Drawer
                    variant="temporary"
                    anchor={theme.direction === 'rtl' ? 'right' : 'left'}
                    open={open}
                    onClose={handleDrawerToggle}
                    ModalProps={{
                        keepMounted: true, // Better open performance on mobile.
                    }}
                >
                    {drawer}
                </Drawer>
            </Hidden>
            <Hidden xsDown implementation="css">
                <Drawer
                    variant="permanent"
                    open
                >
                    {drawer}
                </Drawer>
            </Hidden>
        </nav>
    )
}

export default DashboardDrawer;
