import { RootState } from "../../../../utils/reduxReducers"
import { connect } from "react-redux"
import DashboardAppBar from "./dashboard-app-bar"

const mapStateToProps = (state:RootState,ownProps:{classId:number,handleDrawerToggle: () => void}) => {
    return {
        classTitle: state.ClassInfoList[ownProps.classId].title,
        handleDrawerToggle: ownProps.handleDrawerToggle
    }
}

export default connect(mapStateToProps)(DashboardAppBar)

