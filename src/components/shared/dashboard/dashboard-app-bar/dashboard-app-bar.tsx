import React from 'react';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import { AppBar, Toolbar, Hidden, IconButton, Box, Typography, Badge } from '@material-ui/core';
import {
    Notifications as NotificationsIcon,
    Menu as MenuIcon
} from '@material-ui/icons';
import { grey } from '@material-ui/core/colors';

import { drawerWidth } from '../../../../assets/theme/muiTheme';
import { MenuComp } from "../../gen-top-app-bar/components/top-app-bar-menu";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        appBar: {
            background: theme.palette.background.default,
            color: grey[800],
            boxShadow: "none",
            [theme.breakpoints.up('sm')]: {
                width: `calc(100% - ${drawerWidth}px)`,
                marginLeft: drawerWidth,
            },
        }
    }),
);

interface IProp {
    handleDrawerToggle: () => void,
    classTitle: string
}



const DashboardAppBar: React.FC<IProp> = ({ handleDrawerToggle, classTitle }) => {
    const classes = useStyles();

    return (
        <AppBar position="fixed" className={classes.appBar} color="default">
            <Toolbar>
                <Hidden smUp>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        edge="start"
                        onClick={handleDrawerToggle}
                    >
                        <MenuIcon />
                    </IconButton>
                    <Box mr={2} />
                </Hidden>
                <Typography variant="h6" noWrap>
                    {classTitle}
                </Typography>
                <Box display="flex" marginLeft="auto">
                    <IconButton edge="end">
                        <NotificationsIcon color="primary" fontSize="small" />
                    </IconButton>
                    <Box mx={1} />
                    <MenuComp username=" " />
                </Box>
            </Toolbar>
        </AppBar>
    );
}

export default DashboardAppBar;
