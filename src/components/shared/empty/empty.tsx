import React from "react";
import { Typography, Box } from "@material-ui/core";
import { Interface } from "readline";

type IProp = {
    img: string,
    text: string
}

const EmptyState: React.FC<IProp> = ({ img, text }) => {

    return (
        <Box display="flex" flexDirection="column" margin="auto">
            <img src={img} alt={text} style={{ width: "100%", maxWidth: 100, maxHeight: 100, margin: "3rem auto 2rem auto" }} />
            <Typography>{text}</Typography>
        </Box>
    );
}

export default EmptyState;
