import React from "react";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import { Typography, withStyles, BoxProps, Box } from "@material-ui/core";
import { purple } from "@material-ui/core/colors";
import clsx from "clsx";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: ({
            position: "relative",
            textAlign: "left",
            fontSize: "1.5rem",
            padding: "1rem 0 2rem 0",
            display: prop => prop.center ? "inline-flex" : "block",
            fontFamily: "Roboto Slab",
            "&:after": {
                content: "''",
                position: 'absolute',
                width: prop => prop.underline ? "100%" : "2rem",
                height: 2,
                backgroundColor: purple[500],
                bottom: 23,
                left: 0
            }
        } as any)
    })
);

type IProp = {
    className?: string | undefined,
    underline?: boolean,
    center?: boolean,
}
const Heading: React.FC<BoxProps & IProp> = ({ children, className, underline = false, center = false, ...props }) => {

    const classes = useStyles({ underline, center });

    return (
        <Box {...props}>
            <Typography className={clsx(classes.root, className)}>{children}</Typography>
        </Box>
    );
}

export default Heading;