import React from "react";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import { Typography, withStyles, Box, IconButton } from "@material-ui/core";

import FacebookIcon from '@material-ui/icons/Facebook';
import TwitterIcon from '@material-ui/icons/Twitter';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            minHeight: "auto",
            position: "relative",
        },
        back: {
            position: "absolute",
            top: "35%",
            left: 0,
            transform: "scale(1.5)",
            width: "100%",
            zIndex: -1
        },
        img: {
            width: "50%",
            maxWidth: 350,
        },
        row: {
            display: "flex",
            alignItems: "center",
            marginTop: 50,
            marginBottom: 50,
            justifyContent: "space-evenly",
        },
        footerIcon: {
            color: "#fff"
        },
        footerText: {
            color: "#fff",
            textAlign: "left"
        },
        heading: {
            fontFamily: "Roboto Slab",
            fontSize: "1.6rem",
            marginBottom: "1rem"
        },
        body: {
            letterSpacing: 1,
            color: "inherit"
        },
        [theme.breakpoints.down("xs")]: {
            img: {
                width: "100%",
            },
            row: {
                flexDirection: "column"
            },
            back: {
                bottom: 0,
                top: "auto",
                transform: "scale(2)",
            }
        }
    })
);

const Footer: React.FC = () => {

    const classes = useStyles();

    return (
        <Box className={classes.root}>
            <img className={classes.back} src="https://s3.ap-south-1.amazonaws.com/deploy.teachring.com/assets/images/footer-back.svg" alt="footer background" />

            <Box className={classes.row}>
                <img className={classes.img} src="https://s3.ap-south-1.amazonaws.com/deploy.teachring.com/assets/images/footer-front.svg" alt="footer img" />

                <Box className={classes.footerText}>
                    <Box padding="12px">
                        <Typography className={classes.heading} color="inherit">Contact us</Typography>
                        <Typography className={classes.body} color="inherit">Email: teachringapp@gmail.com</Typography>
                        <Typography className={classes.body} color="inherit">Tel: +91-9790488941</Typography>
                    </Box>

                    <Box mt={2}>
                        <IconButton className={classes.footerIcon}>
                            <FacebookIcon />
                        </IconButton>
                        <IconButton className={classes.footerIcon}>
                            <TwitterIcon />
                        </IconButton>
                    </Box>
                </Box>
            </Box>
        </Box>
    )

}

export default Footer;
