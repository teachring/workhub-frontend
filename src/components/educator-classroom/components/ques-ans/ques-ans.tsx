import React, { useEffect, useState } from 'react';
import { Paper, Box, Typography, TextField, Button, CircularProgress } from '@material-ui/core';
import { Face } from '@material-ui/icons';
import { QuesAnsObj } from '../../../../utils/core-types/shared';
import { User } from '../../../../utils/core-types/user-types';
import Heading from '../../../shared/heading/heading';
import useStyles from "./ques-ans-styles";
import { timeSince } from '../../../../utils/helpers';
import EmptyImg from "../../../../assets/images/no-educator-qa.svg";
import EmptyState from '../../../shared/empty/empty';

type IProp = {
    quesAnsList?: QuesAnsObj[],
    fetchQuesAnsIfNeeded: () => void,
    closeQuesReq: (quesId: number, answer?: string) => Promise<boolean>,
    getUserDetails: (userId: number) => User
}

const EducatorQuesAns: React.FC<IProp> = ({ quesAnsList, fetchQuesAnsIfNeeded, closeQuesReq, getUserDetails }) => {

    const classes = useStyles();

    const [waiting, setWaiting] = useState({});
    const [answer, setAnswer] = useState({});

    useEffect(() => {
        fetchQuesAnsIfNeeded()
    }, [])

    const handleSubmit = (qa: QuesAnsObj) => {
        setWaiting(prevState => {
            return {
                ...prevState,
                [qa.questionId]: false
            }
        });
        closeQuesReq(qa.questionId, answer[qa.questionId])
            .then(resolve => {
                setWaiting(prevState => {
                    return {
                        ...prevState,
                        [qa.questionId]: true
                    }
                });
            })
    }

    const handleChange = (qa: QuesAnsObj, value: string) => {
        setAnswer(prevAnswer => {
            return {
                ...prevAnswer,
                [qa.questionId]: value
            }
        })
    }

    const card = (qa: QuesAnsObj, user: User) => (
        <Paper key={qa.questionId} className={classes.paper} elevation={2}>
            <Face className={classes.icon} />
            <Box className={classes.body}>
                <Box className={classes.details}>
                    <Typography>{user.userName}</Typography> <Typography>{timeSince(qa.askedDateTime)}</Typography>
                </Box>

                <Typography className={classes.question}>{qa.questionBody}</Typography>

                {
                    qa.answerBody
                        ? <Typography className={classes.answerBody}>{qa.answerBody}</Typography>
                        : <>
                            <TextField
                                className={classes.answerInput}
                                label="Answer"
                                multiline
                                rows="4"
                                color="primary"
                                disabled={waiting[qa.questionId] ?? false}
                                value={answer[qa.questionId]}
                                onChange={({ target: { value } }) => handleChange(qa, value)}
                            />

                            <Box display="flex" mt={4} alignItems="center">
                                <Button color="primary" variant="contained" disabled={waiting[qa.questionId]} onClick={() => handleSubmit(qa)}>Submit</Button>
                                <Box mr={2} />
                                <Button color="primary" disabled={waiting[qa.questionId]} onClick={() => handleSubmit(qa)}>Close</Button>
                                {waiting[qa.questionId] ? <CircularProgress className={classes.circularProgress} /> : ""}
                            </Box>
                        </>
                }

            </Box>
        </Paper>
    )

    const sortQ = (quesAnsList: QuesAnsObj[] | undefined, type: QuesAnsObj["status"]): QuesAnsObj[] | undefined => {
        return quesAnsList?.filter((qa: QuesAnsObj) => qa.status === type)
            .sort((a: QuesAnsObj, b: QuesAnsObj) => {
                return a.askedDateTime > b.askedDateTime
                    ? -1
                    : (a.askedDateTime < b.askedDateTime ? 1 : 0)
            });
    }

    const pendingQ = sortQ(quesAnsList, "PENDING");
    const closedQ = sortQ(quesAnsList, "CLOSED");

    return (
        <Box className={classes.root}>
            {
                pendingQ?.length === 0 && closedQ?.length === 0
                    ? <>
                        <Heading className={classes.heading}>Learner Questions</Heading>
                        <EmptyState img="https://s3.ap-south-1.amazonaws.com/deploy.teachring.com/assets/images/no-educator-qa.svg" text="No questions yet" />
                    </>
                    : pendingQ && pendingQ.length > 0
                        ? <>
                            <Heading className={classes.heading}>Pending Questions</Heading>
                            {pendingQ?.map((qa: QuesAnsObj) => card(qa, getUserDetails(qa.askedBy)))}
                        </>
                        : closedQ && closedQ.length > 0
                            ? <>
                                <Box mt={5} />
                                <Heading className={classes.heading}>Closed Questions</Heading>
                                {closedQ?.map((qa: QuesAnsObj) => card(qa, getUserDetails(qa.askedBy)))}
                            </>
                            : ""
            }
        </Box>
    );
}

export default EducatorQuesAns;