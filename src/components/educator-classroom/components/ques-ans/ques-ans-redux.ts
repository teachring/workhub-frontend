import {connect} from 'react-redux';
import EducatorQuesAns from './ques-ans';
import { RootState } from '../../../../utils/reduxReducers';
import {fetchQuesAnsIfNeeded,closeQuestion} from './actions';
 
const mapStateToProps = (state:RootState, ownProps:any) => {
    return {
        quesAnsList: state.EducatorState.QuesAns[ownProps.classId],
        getUserDetails: (userId:number) => {
            return state.UsersDB[userId];
        }
    }
}

const mapDispatchToProps = (dispatch:Function,ownProps:any) => {
    return {
        fetchQuesAnsIfNeeded: ():void => {
            dispatch(fetchQuesAnsIfNeeded(ownProps.classId))
        },
        closeQuesReq: (quesId:number,answer?:string): Promise<boolean> => {
            return dispatch(closeQuestion(ownProps.classId,quesId,answer))
        }, 
    }
}


export default connect(mapStateToProps,mapDispatchToProps)(EducatorQuesAns);
