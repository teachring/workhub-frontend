import { RootState } from "../../../../utils/reduxReducers";
import { educatorFetchQuestions, educatorCloseQuestion } from '../../../../utils/apis/ques-ans';
import { fetchUserDetailsIfNeeded } from "../../../../utils/reduxActions/users-db";
import { QuesAnsObj } from "../../../../utils/core-types/shared";


export type EducatorReceiveQuesAnsAction = {
    type: 'RECEIVE_EDUCATOR_QUES_ANS',
    classId:number,
    quesAnsList: QuesAnsObj[]
}

export type EducatorQuesCloseAction = {
    type: 'EDUCATOR_CLOSE_QUESTION',
    classId: number,
    quesId: number,
    updatedQuesObj: QuesAnsObj
}


export const fetchQuesAnsIfNeeded = (classId:number) => {
    return async function(dispatch:(arg0:EducatorReceiveQuesAnsAction | any) => any,getState:() => RootState) {
        if(!getState().EducatorState.QuesAns[classId]) {
            let quesAnsList = await educatorFetchQuestions(classId);
            let userIds:number[] = []
            quesAnsList.forEach(quesObj => {
                userIds.push(quesObj.askedBy);
                if(quesObj.closedBy) { userIds.push(quesObj.closedBy)}
            });
            await dispatch(fetchUserDetailsIfNeeded(userIds))
            dispatch({type:'RECEIVE_EDUCATOR_QUES_ANS',classId:classId,quesAnsList:quesAnsList})
        }

        return ;
    }
}


export const closeQuestion = (classId:number,quesId:number,answer?: string) => {
    return function(dispatch:(arg0:EducatorQuesCloseAction)=>void) {
        return educatorCloseQuestion(classId,quesId,answer)
        .then(newQuesObj => {
            dispatch({type:'EDUCATOR_CLOSE_QUESTION',classId:classId, quesId: quesId, updatedQuesObj: newQuesObj})
            return true;
        },
        err => {
            return false
        }
    )

    }
}