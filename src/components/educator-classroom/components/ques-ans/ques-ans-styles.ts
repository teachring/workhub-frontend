import { makeStyles, useTheme, Theme, createStyles } from '@material-ui/core/styles';
import { purple } from '@material-ui/core/colors';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            width: "100%",
            maxWidth: 700,
            margin: "auto",
            padding: "2rem"
        },
        paper: {
            margin: "2rem auto",
            padding: "2rem",
            display: "flex"
        },
        icon: {
            color: purple[500]
        },
        body: {
            paddingLeft: "1rem",
            textAlign: "left",
            flex: 1,
            width: "100%",
            paddingRight: "1rem"
        },
        details: {
            fontWeight: "lighter",
            "& p": {
                fontSize: "0.9rem",
            }
        },
        question: {
            textAlign: "left",
            fontSize: "1.2rem",
            // fontFamily: "Roboto Slab",
            margin: "1rem 0",
        },
        answerInput: {
            maxWidth: 400,
            width: "100%",
            fontWeight: "lighter",
            fontSize: "1.1rem"
        },
        answerBody: {
            fontSize: "1.1rem",
            fontWeight: "lighter"
        },
        circularProgress: {
            width: "22px !important",
            height: "22px !important",
            margin: 0
        },
        heading: {
            marginLeft: 0
        },
        [theme.breakpoints.down("xs")]: {
            root: {
                padding: "1rem"
            },
            body: {
                padding: 0
            },
            icon: {
                display: "none"
            }
        }
    })
);

export default useStyles;
