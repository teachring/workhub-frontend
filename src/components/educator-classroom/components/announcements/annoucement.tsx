import React, { useEffect } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";

import useStyles from './anncmnt-styles';
import { AnnouncementObj } from '../../../../utils/core-types/educator-types';
import { User } from '../../../../utils/core-types/user-types';
import { Paper, IconButton } from '@material-ui/core';
import { Face, Telegram } from '@material-ui/icons';
import { timeSince } from '../../../../utils/helpers';
import Heading from '../../../shared/heading/heading';
import WaitingDialog from '../../../dialog-screens/waiting-dialog';
import { AlertMessageType } from '../../../alert-snackbar/alert-snackbar-actions';
import EmptyState from '../../../shared/empty/empty';
import EmptyImg from "../../../../assets/images/no-announcement.svg";

type IProp = {
    announcementList?: AnnouncementObj[],
    fetchAnnouncementsIfNeeded: () => void,
    postNewAnnouncement: (message: string) => Promise<boolean>,
    getUserDetails: (userId: number) => User,
    publishAlerts: (type: AlertMessageType, message: string) => void
}

const Annoucements: React.FC<IProp> = ({ announcementList, fetchAnnouncementsIfNeeded, postNewAnnouncement, getUserDetails, publishAlerts }) => {
    const classes = useStyles();

    const [announce, setAnnounce] = React.useState("");
    const [waiting, setWaiting] = React.useState(false);
    const [newAnnouncementDialogOpen, setNewAnnouncementDialogOpen] = React.useState(false);

    useEffect(() => {
        fetchAnnouncementsIfNeeded()
    }, [])

    const handleAnnounce = (value: string) => {
        if (value.length <= 160)
            setAnnounce(value);
    }

    const handleConfirmationDialogClose = (confirm: boolean) => {
        if (confirm) {
            setWaiting(true);
            postNewAnnouncement(announce)
                .then((isSuccess) => {
                    setWaiting(false);
                    setNewAnnouncementDialogOpen(false);
                    if (isSuccess) {
                        setAnnounce('');
                        publishAlerts(AlertMessageType.SUCCESS_MESSAGE, "SMS announcement is sent successfully to all the learners");
                    } else {
                        publishAlerts(AlertMessageType.ERROR_MESSAGE, "Announcement could not be made, please try again");
                    }
                })

        } else {
            setNewAnnouncementDialogOpen(false)
        }


    }

    const handleSend = () => {
        setNewAnnouncementDialogOpen(true);
    }

    const card = (annoucement: AnnouncementObj, index: number) => (
        <Paper className={classes.paper} key={index} elevation={2}>
            <Typography className={classes.message}>{annoucement.message}</Typography>
            <Box className={classes.detailsBox}>
                <Face fontSize="small" className={classes.icon} />
                <Typography className={classes.details}>{getUserDetails(annoucement.createdBy).userName}</Typography>
                <Typography className={classes.details}>{timeSince(annoucement.createDateTime)}</Typography>
            </Box>
        </Paper>
    );

    return (
        <Box className={classes.root}>
            <Heading>Create announcement</Heading>

            <Box my={3}>
                <Paper elevation={2} className={classes.newAnnoucementContainer}>
                    <TextField
                        className={classes.newAnnoucement}
                        placeholder="Enter annoucement"
                        helperText={announce.length + " / 160 characters"}
                        multiline
                        value={announce}
                        onChange={({ target: { value } }) => handleAnnounce(value)}
                        autoFocus
                        disabled={waiting}
                    />
                    <Button
                        className={classes.sendBtn}
                        onClick={handleSend}
                        disabled={announce.length == 0}
                        variant="contained" color="primary">
                        SEND
                    </Button>
                </Paper>
            </Box>

            <WaitingDialog open={newAnnouncementDialogOpen} waiting={waiting} onDialogClose={handleConfirmationDialogClose} dialogTitle='Make an Announcement' okBtnText="Announce Now">
                <Typography>An instant SMS will be sent to all the learners of your class. Do you want to continue?</Typography>
            </WaitingDialog>

            <Heading>Announcements</Heading>
            {
                announcementList && announcementList.length > 0
                    ? announcementList.map(card)
                    : <EmptyState img="https://s3.ap-south-1.amazonaws.com/deploy.teachring.com/assets/images/no-announcement.svg" text="No announcements yet" />
            }
        </Box>
    );
}

export default Annoucements;
