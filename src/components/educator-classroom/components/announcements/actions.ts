import { AnnouncementObj } from "../../../../utils/core-types/educator-types"
import { RootState } from "../../../../utils/reduxReducers"
import { fetchAnnouncements,postAnnouncement } from "../../../../utils/apis/educators"
import { fetchUserDetailsIfNeeded } from "../../../../utils/reduxActions/users-db"


export type EducatorAnncmntReceiveAction = {
    type: 'RECEIVE_EDUCATOR_ANNOUNCEMENT',
    classId:number,
    announcementList: AnnouncementObj[]
}

export type EducatorAnncmntPostAction = {
    type: 'EDUCATOR_POST_ANNOUNCEMENT',
    classId: number,
    announcement: AnnouncementObj
}

export const fetchAnnouncementsIfNeeded = (classId:number) => {
    return async function(dispatch:Function,getState:()=> RootState) {
        if(!getState().EducatorState.AnnouncementsDone[classId]) {
            let aList = await fetchAnnouncements(classId)
            let userIds = aList.map(anncmnt=> {return anncmnt.createdBy})
            await dispatch(fetchUserDetailsIfNeeded(userIds));
            dispatch({type:'RECEIVE_EDUCATOR_ANNOUNCEMENT',classId:classId,announcementList:aList})
        }
        return;
    }
}

export const postNewAnnouncement = (classId:number,msg:string)=> {
    return function(dispatch:(arg0:EducatorAnncmntPostAction)=>void) {
        return postAnnouncement(classId,msg)
        .then(newObj => {
            dispatch({type:'EDUCATOR_POST_ANNOUNCEMENT',classId:classId,announcement:newObj})
            return true;
        },
        err => {
            return false
        }
    )

    }
}
