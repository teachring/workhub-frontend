import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import red from '@material-ui/core/colors/red';
import { purple } from '@material-ui/core/colors';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: "inline-flex",
            flexDirection: "column",
            padding: "2rem",
            width: "100%",
            maxWidth: 700
        },
        circularProgress: {
            width: "20px !important",
            height: "20px !important"
        },
        submitBtn: {
            background: "linear-gradient(60deg, #ab47bc, #8e24aa)",
            boxShadow: "0 4px 20px 0 rgba(0, 0, 0,.14), 0 7px 10px -5px rgba(156, 39, 176,.4)",
            borderRadius: 3,
            padding: "8px 20px",
            "&.Mui-disabled": {
                background: "transparent",
                border: "1px solid rgba(0,0,0,0.12)"
            }
        },
        paper: {
            padding: "2rem",
            textAlign: "left",
            width: "100%",
            maxWidth: "650px",
            margin: "2rem auto",
        },
        message: {
            fontSize: "1.2rem",
            marginBottom: "1rem"
        },
        icon: {
            color: purple[500]
        },
        detailsBox: {
            display: "flex",
            flexWrap: "wrap",
            alignItems: "center",
            justifyContent: "flex-start"
        },
        details: {
            fontSize: ".9rem",
            fontWeight: "lighter",
            marginLeft: 10
        },
        newAnnoucementContainer: {
            display: "flex",
            flexWrap: "wrap",
            alignItems: "center",
            background: "#fff",
            padding: "2rem",
            position: "relative",
            overflow: "hidden",
            "&:before": {
                content: "''",
                position: "absolute",
                top: 0,
                left: 0,
                background: "#9c28b0",
                width: 3,
                height: "100%",
            }
        },
        newAnnoucement: {
            flex: 1,
            minWidth: 200,
            marginRight: "2rem"
        },
        sendBtn: {
            minWidth: 100
        },
        [theme.breakpoints.down("xs")]: {
            root: {
                padding: "1rem"
            }
        }
    }),
);

export default useStyles;