import {connect} from 'react-redux';
import Annoucements from './annoucement';
import { RootState } from '../../../../utils/reduxReducers';
import {fetchAnnouncementsIfNeeded,postNewAnnouncement} from './actions';
import { dispatchAlertAction, AlertMessageType } from '../../../alert-snackbar/alert-snackbar-actions';

const mapStateToProps = (state:RootState,ownProps:any) => {
    return {
        announcementList: state.EducatorState.AnnouncementsDone[ownProps.classId],
        getUserDetails: (userId:number) => {
            return state.UsersDB[userId];
        }
    }
}

const mapDispatchToProps = (dispatch:Function,ownProps:any) => {
    return {
        fetchAnnouncementsIfNeeded: () => {
            dispatch(fetchAnnouncementsIfNeeded(ownProps.classId))
        },
        postNewAnnouncement: (message:string) :Promise<boolean> => {
            return dispatch(postNewAnnouncement(ownProps.classId,message))
        },
        publishAlerts : (type:AlertMessageType,message:string) => {
            dispatchAlertAction(dispatch,type,message);
        },
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Annoucements);