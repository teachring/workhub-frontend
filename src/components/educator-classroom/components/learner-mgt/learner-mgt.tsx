import React, { useEffect } from 'react';
import MaterialTable, { Column, MTableToolbar } from 'material-table';
import { Box, Button, CircularProgress, LinearProgress, Typography, DialogContentText, TextField } from '@material-ui/core';

import { User } from '../../../../utils/core-types/user-types';
import { RegisteredLearner } from '../../../../utils/core-types/educator-types';
import { RegFormQuestion, RegFormQuesType } from "../../../../utils/core-types/learner-reg";
import tableIcons from "./material-table-icons";
import useStyles from "./learner-mgt-styles";
import ReminderDialog from './payment-remind-dialog';
import MarkPaidDialog from './mark-paid-dialog';
import WaitingDialog from '../../../dialog-screens/waiting-dialog';
import EmptyState from '../../../shared/empty/empty';
import EmptyImg from "../../../../assets/images/no-participant.svg";
import Heading from '../../../shared/heading/heading';

type IProp = {
    registeredLearnersList?: RegisteredLearner[],
    fetchRegdLearnersIfNeeded: () => void,
    markLearnersOfflinePaid: (userIds: number[]) => Promise<boolean>,
    sendPayReminders: (userIds: number[], msg: string) => Promise<boolean>,
    getUserDetails: (userId: number) => User
}

interface TableData {
    id: number,
    checked: boolean
}

interface TableState {
    columns: any;
    data: any;
}

const getColumns = (registeredLearnersList: RegisteredLearner[] | undefined) => {
    let columns = [
        { title: "Name", field: "name" },
        { title: "Payment", field: "paid" }
    ];

    if (registeredLearnersList?.[0]?.regFormData) {
        Object.values(registeredLearnersList[0].regFormData)
            .forEach((formdata: RegFormQuestion) => {
                columns.push({ title: formdata.question, field: formdata.question });
            });
    }
    return columns;
}

const getData = (registeredLearnersList: RegisteredLearner[] | undefined, getUserDetails: (userId: number) => User) => {
    let data: any = [];
    let user: User;

    registeredLearnersList?.forEach((learner: RegisteredLearner, i) => {
        user = getUserDetails(learner.userId);
        data[i] = {
            user,
            name: user.userName,
            paid: learner.paymentDetails?.isPaid && "paid" || "not paid",
            tableData: {
                id: i,
                checked: false
            }
        };

        if (learner.regFormData)
            Object.values(learner.regFormData)
                .forEach((formdata: RegFormQuestion) => {

                    switch (formdata.type) {

                        case RegFormQuesType.TEXT:
                            data[i][formdata.question] = formdata.answer ?? "";
                            break;

                        case RegFormQuesType.RADIO:
                            data[i][formdata.question] = formdata.selectedOption ?? "";
                            break;

                        case RegFormQuesType.CHECKBOX:
                            let ans = formdata.options!.reduce((acc, item) => item.selected ? acc + item.text + ", " : acc, "");
                            if (ans.length > 0)
                                ans = ans.slice(0, -2);
                            data[i][formdata.question] = ans;
                            break;

                        default:
                            break;

                    }
                })

    });
    return data;
}

const getSelectionCount = (state: TableState): number => {
    if (state.data)
        return state.data.filter(row => (row.tableData as TableData).checked).length;
    return 0;
}

const getDisabledCount = (registeredLearnersList: RegisteredLearner[] | undefined): number => {
    return registeredLearnersList?.filter(learner => !learner.paymentDetails?.isPaid).length || 0;
}

const isDisabledRow = row => row.paid === "paid";

const LearnerMgt: React.FC<IProp> = ({ fetchRegdLearnersIfNeeded, registeredLearnersList, markLearnersOfflinePaid, sendPayReminders, getUserDetails }) => {

    const classes = useStyles();

    let columns: any = getColumns(registeredLearnersList);
    let data: any = [];

    const [state, setState] = React.useState<TableState>({ columns, data });

    let disabledCount: number = getDisabledCount(registeredLearnersList);
    let selectionCount: number = getSelectionCount(state);

    const [remindAction, setRemindAction] = React.useState("REMIND_ALL");

    const [message, setMessage] = React.useState("Hello, this is a friendly reminder that your payment for the registered class is overdue. Please pay the amount as soon as possible. Thanks!");
    const [waiting, setWaiting] = React.useState(false);
    const [reminderOpen, setReminderOpen] = React.useState(false);
    const [markPaidOpen, setMarkPaidOpen] = React.useState(false);

    const handleReminderOpen = () => {
        setReminderOpen(true);
    };
    const handleReminderClose = (confirm: boolean) => {
        if (confirm) {
            if (remindAction === "REMIND_ALL") {
                setWaiting(true);
                sendPayReminders(state.data.filter(row => !isDisabledRow(row)).map(row => row.user.userId), message)
                    .then(resolve => {
                        setWaiting(false);
                        setReminderOpen(false);
                    })
            } else if (remindAction === "REMIND_SELECTED") {
                setWaiting(true);
                sendPayReminders(state.data.filter(row => row.tableData.checked), message)
                    .then(resolve => {
                        setWaiting(false);
                        setReminderOpen(false);
                        deselectAll();
                    })
            }
        } else {
            setReminderOpen(false);
        }
    };

    const handleMarkPaidOpen = () => {
        setMarkPaidOpen(true);
    }

    const handleMarkPaidClose = (confirm: boolean) => {
        if (confirm) {
            setWaiting(true);
            markLearnersOfflinePaid(state.data.filter(row => row.tableData.checked).map(row => row.user.userId))
                .then(resolve => {
                    setWaiting(false);
                    setMarkPaidOpen(false);
                    deselectAll();
                })
        } else {
            setMarkPaidOpen(false);
        }
    }

    const handleRemindAll = () => {
        setRemindAction("REMIND_ALL");
        handleReminderOpen();
    }

    const handleRemindSelected = () => {
        setRemindAction("REMIND_SELECTED");
        handleReminderOpen();
    }

    const selectAll = () => {
        // select all rows except the disable ones
        setState((prevState: TableState) => {
            selectionCount = 0;
            return {
                ...prevState,
                data: prevState.data.map(row => {
                    if (isDisabledRow(row)) {
                        (row.tableData as TableData).checked = false;
                    } else {
                        (row.tableData as TableData).checked = true;
                        selectionCount++;
                    }
                    return row;
                })
            }
        });
    }

    const deselectAll = () => {
        // deselect all rows
        setState((prevState: TableState) => {
            selectionCount = 0;
            return {
                ...prevState,
                data: prevState.data.map(row => {
                    (row.tableData as TableData).checked = false;
                    return row;
                })
            }
        })
    }

    const handleSelection = (selectedRows: any[], clickedRow?) => {
        // override the select-all checkbox action
        if (clickedRow === undefined) {
            // clickedRow is undefined for select-all checkbox
            if (selectionCount < disabledCount) {
                selectAll();
            } else {
                deselectAll();
            }

        } else {
            selectionCount = selectedRows.length;
        }
    }

    useEffect(() => {
        fetchRegdLearnersIfNeeded()
    }, [])

    useEffect(() => {
        registeredLearnersList && setState((currentState: TableState) => {
            return {
                ...currentState,
                columns: getColumns(registeredLearnersList),
                data: getData(registeredLearnersList, getUserDetails)
            }
        })
    }, [registeredLearnersList])

    const handleReminderMsgOnChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        if (e.target.value.length < 160)
            setMessage(e.target.value)
    }

    return (

        registeredLearnersList === undefined
            ? <CircularProgress color="primary" />
            : <Box>

                {/* <ReminderDialog open={reminderOpen} message={message} waiting={waiting} setMessage={setMessage} handleClose={handleReminderClose} /> */}

                <WaitingDialog open={reminderOpen} waiting={waiting} onDialogClose={handleReminderClose} dialogTitle='Send payment reminder?' okBtnText='Send' cancelBtnText='Cancel'>
                    <DialogContentText> Enter payment reminder message </DialogContentText>
                    <TextField
                        value={message}
                        onChange={handleReminderMsgOnChange}
                        autoFocus
                        label="Message"
                        fullWidth
                        required
                        error={message.length === 0}
                        helperText={message.length + " / 160"}
                        multiline
                        rows={5}
                        disabled={waiting}
                    />
                </WaitingDialog>

                {/* <MarkPaidDialog open={markPaidOpen} waiting={waiting} handleClose={handleMarkPaidClose} /> */}

                <WaitingDialog open={markPaidOpen} waiting={waiting} onDialogClose={handleMarkPaidClose} dialogTitle='Mark selected as paid?' />

                <Heading ml="24px">Participants</Heading>

                {
                    registeredLearnersList.length > 0
                        ? <>
                            <Box my={3}>
                                <Box className={classes.statBox}>
                                    <Typography className={classes.statNum}>20</Typography>
                                    <Typography>registered</Typography>
                                </Box>

                                <Box className={classes.statBox}>
                                    <Typography className={classes.statNum}>10</Typography>
                                    <Typography>paid</Typography>
                                </Box>

                                <Box className={classes.statBox}>
                                    <Typography className={classes.statNum}>05</Typography>
                                    <Typography>settled</Typography>
                                </Box>
                            </Box>

                            <MaterialTable
                                style={{
                                    borderRadius: 0,
                                    boxShadow: "none"
                                }}
                                icons={tableIcons}
                                title="Registered users"
                                columns={state.columns}
                                data={state.data}
                                options={{
                                    selection: true,
                                    showSelectAllCheckbox: true,
                                    selectionProps: rowData => ({
                                        disabled: isDisabledRow(rowData)
                                    }),
                                    search: true,
                                    showTextRowsSelected: false,
                                    searchFieldStyle: {
                                        width: 250,
                                        margin: "15px 22px 15px 0px"
                                    }
                                }}
                                onSelectionChange={handleSelection}
                                components={{
                                    Toolbar: props => (
                                        <Box className={classes.toolbar} data-selected={getSelectionCount(state) > 0}>
                                            <MTableToolbar {...props} />
                                            {
                                                getSelectionCount(state) === 0
                                                    ? <Button className={classes.tableActionBtn} variant="contained" color="primary" onClick={handleRemindAll}>Remind all</Button>
                                                    : <div>
                                                        <Button className={classes.tableActionBtn} variant="contained" color="primary" onClick={handleMarkPaidOpen}>Mark as paid</Button>
                                                        <Button className={classes.tableActionBtn} variant="contained" color="primary" onClick={handleRemindSelected}>Remind selected</Button>
                                                    </div>
                                            }
                                        </Box>
                                    ),
                                }}
                            />
                        </>
                        : <EmptyState img="https://s3.ap-south-1.amazonaws.com/deploy.teachring.com/assets/images/no-participant.svg" text="No participants yet" />
                }
            </Box>

    );
}

export default LearnerMgt;