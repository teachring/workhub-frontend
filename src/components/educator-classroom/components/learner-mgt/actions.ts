import { RegisteredLearner } from "../../../../utils/core-types/educator-types";
import { sendPayReminders, markOfflinePaid,fetchRegisteredLearners} from '../../../../utils/apis/educators';
import { RootState } from "../../../../utils/reduxReducers";
import { fetchUserDetailsIfNeeded } from "../../../../utils/reduxActions/users-db";

export type EducatorRecvLearnersAction = {
    type:'EDUCATOR_RECEIVE_LEARNERS',
    classId: number,
    learnerList: RegisteredLearner[]
}

export type EducatorSendPayReminderAction = {
    type: 'EDUCATOR_SENT_PAY_REMINDER',
    classId: number,
    userIds: number[],
    sentDateTime: Date
}

export type EducatorRecvOfflinePayAction = {
    type: 'EDUCATOR_RECEIVE_OFFLINE_PAID',
    classId: number,
    userIds: number[],
    payDateTime:Date
}


export const sendPaymentReminders = (classId:number,userIds:number[],msg:string) => {
    return function(dispatch:(arg0:EducatorSendPayReminderAction)=>any) {
        return sendPayReminders(classId,userIds,msg)
        .then((sentDateTime) => {
            dispatch({type:'EDUCATOR_SENT_PAY_REMINDER',classId:classId,sentDateTime:sentDateTime,userIds:userIds})
            return true;
        }, 
        err => {
            return false;
        })
    }
}

export const markWhenPaidOffline = (classId:number,userIds:number[]) => {
    return function(dispatch:(arg0:EducatorRecvOfflinePayAction)=>any) {
        return markOfflinePaid(classId,userIds)
        .then((payDateTime) => {
            dispatch({type:'EDUCATOR_RECEIVE_OFFLINE_PAID',payDateTime:payDateTime,classId:classId,userIds:userIds})
            return true;
        },
        err => {
            return false
        })
    }
}

export const fetchRegLearnersIfNeeded = (classId:number) => {
    return async function(dispatch:(arg0:EducatorRecvLearnersAction | any)=> any,getState:()=>RootState) {
        if(!getState().EducatorState.RegisteredLearners[classId]){
            
            let learnerList = await fetchRegisteredLearners(classId)
            let userIds = learnerList.map(learnerObj => learnerObj.userId)
            await dispatch(fetchUserDetailsIfNeeded(userIds));
            dispatch({type:'EDUCATOR_RECEIVE_LEARNERS',learnerList:learnerList,classId:classId})

        }
        return;
    }
}

//Now when they are sent, you need to check that 