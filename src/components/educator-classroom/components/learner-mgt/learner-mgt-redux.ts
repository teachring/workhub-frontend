import {connect} from 'react-redux';
import { RootState } from '../../../../utils/reduxReducers';
import LearnerMgt from './learner-mgt';
import { fetchRegLearnersIfNeeded,markWhenPaidOffline,sendPaymentReminders} from './actions';


const mapStateToProps = (state:RootState,ownProps:any) => {
    return {
        registeredLearnersList: state.EducatorState.RegisteredLearners[ownProps.classId],
        getUserDetails: (userId:number) => {
            return state.UsersDB[userId];
        }
    }

}

const mapDispatchToProps = (dispatch:Function,ownProps:any) => {
    return {
        fetchRegdLearnersIfNeeded: ():void => {
            dispatch(fetchRegLearnersIfNeeded(ownProps.classId))
        },
        sendPayReminders: (userIds:number[],msg:string):Promise<boolean> => {
            return dispatch(sendPaymentReminders(ownProps.classId,userIds,msg))
        },
        markLearnersOfflinePaid:(userIds:number[]):Promise<boolean> => {
            return dispatch(markWhenPaidOffline(ownProps.classId,userIds))
        }
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(LearnerMgt);