import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Box from '@material-ui/core/Box';
import LinearProgress from '@material-ui/core/LinearProgress';

interface IProp {
    open: boolean,
    waiting: boolean,
    handleClose: (confirm: boolean) => void
}

const MarkPaidDialog: React.FC<IProp> = ({ open, waiting, handleClose }) => {

    return (
        <Dialog open={open} aria-labelledby="mark-paid-title">
            <DialogTitle id="mark-paid-title">Mark selected as paid?</DialogTitle>
            <DialogContent>
                {waiting && <Box my={1}><LinearProgress color="secondary" /></Box>}
            </DialogContent>
            <DialogActions>
                <Button disabled={waiting} onClick={() => handleClose(false)} color="secondary"> Cancel </Button>
                <Button disabled={waiting} onClick={() => handleClose(true)} color="secondary" autoFocus> OK </Button>
            </DialogActions>
        </Dialog>
    );
}

export default MarkPaidDialog;
