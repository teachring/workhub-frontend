import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        tableActionBtn: {
            marginRight: "1rem"
        },
        toolbar: {
            display: "flex",
            flexWrap: "wrap",
            alignItems: "center",
            position: "sticky",
            background: "#fff",
            zIndex: 20,
            borderBottom: "1px solid rgba(224, 224, 224, 1)",
            "& .MuiToolbar-root": {
                display: "flex",
                flexWrap: "wrap",
                flex: 1,
                padding: "10px 15px"
            },
            "&[data-selected='true']": {
                color: "#f50057",
                backgroundColor: "rgb(255, 226, 236)"
            },
            [theme.breakpoints.down("sm")]: {
                top: 56,
                textAlign: "left",
                "& .MuiToolbar-root": {
                    paddingTop: 15
                }
            },
            [theme.breakpoints.up("sm")]: {
                top: 64
            },
            [`${theme.breakpoints.down("xs")} and (orientation: landscape)`]: {
                top: 48
            },
        },
        statBox: {
            display: "inline-flex",
            flexDirection: "column",
            padding: "1rem 3rem",
        },
        statNum: {
            fontSize: "3rem",
            fontFamily: "Roboto Slab"
        },
        [theme.breakpoints.down("xs")]: {
            tableActionBtn: {
                marginBottom: "1rem",
                marginLeft: "2rem"
            },
        }
    }),
);

export default useStyles;
