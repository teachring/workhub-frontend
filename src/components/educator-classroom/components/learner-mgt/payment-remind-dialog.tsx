import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import LinearProgress from '@material-ui/core/LinearProgress';
import Box from '@material-ui/core/Box';

interface IProp {
    open: boolean,
    message: string,
    waiting: boolean,
    setMessage: React.Dispatch<React.SetStateAction<string>>,
    handleClose: (confirm: boolean) => void
}

const ReminderDialog: React.FC<IProp> = ({ open, message, waiting, setMessage, handleClose }) => {

    const handleOnChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        if (e.target.value.length < 160)
            setMessage(e.target.value)
    }

    return (
        <div>
            <Dialog open={open} aria-labelledby="reminder-dialog">
                <DialogTitle id="reminder-dialog">Send payment reminder?</DialogTitle>
                <DialogContent>
                    <DialogContentText> Enter payment reminder message </DialogContentText>
                    <TextField
                        value={message}
                        onChange={handleOnChange}
                        autoFocus
                        label="Message"
                        fullWidth
                        required
                        error={message.length === 0}
                        helperText={message.length + " / 160"}
                        multiline
                        rows={5}
                        disabled={waiting}
                    />
                    {waiting && <Box mt={2}><LinearProgress color="secondary" /></Box>}
                </DialogContent>
                <DialogActions>
                    <Button disabled={waiting} onClick={() => handleClose(false)} color="secondary"> Cancel </Button>
                    <Button disabled={waiting} onClick={() => handleClose(true)} color="secondary"> Send </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}

export default ReminderDialog;
