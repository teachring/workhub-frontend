import {connect} from 'react-redux';
import { RootState } from "../../../../utils/reduxReducers";
import { fetchQueriesIfNeeded,closeQuery } from './actions';
import RegQueries from './reg-queries';

const mapStateToProps = (state:RootState,ownProps:any) => {
    return {
        regQueries : state.EducatorState.RegQueries[ownProps.classId],
        getUserDetails: (userId:number) => {
            return state.UsersDB[userId];
        }
    }
}

const mapDispatchToProps = (dispatch:Function,ownProps:any) => {
    return {
        fetchqueriesIfNeeded: ():void => {
            dispatch(fetchQueriesIfNeeded(ownProps.classId));
        },
        queryCloseReq: (queryId:number):Promise<boolean> => {
            return dispatch(closeQuery(ownProps.classId,queryId))
        }
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(RegQueries);