import { EducatorRegQuery } from "../../../../utils/core-types/educator-types"
import { RootState } from "../../../../utils/reduxReducers";
import { educatorFetchRegEnquiries,educatorRegQueryStatusUpdate } from '../../../../utils/apis/reg-enquires';
import { fetchUserDetailsIfNeeded } from "../../../../utils/reduxActions/users-db";


export type EducatorReceiveRegQueriesAction = {
    type: 'RECEIVE_EDUCATOR_REG_QUERY',
    classId:number,
    regQueries: EducatorRegQuery[]
}

export type EducatorRegQueryCloseAction = {
    type: 'EDUCATOR_CLOSE_REG_QUERY',
    classId: number,
    queryId: number
}


export const fetchQueriesIfNeeded = (classId:number) => {
    return async function(dispatch:(arg0:EducatorReceiveRegQueriesAction | any) => any,getState:() => RootState) {
        if(!getState().EducatorState.RegQueries[classId]) {
            let queryList = await educatorFetchRegEnquiries(classId)
            let userIds = queryList.map(query => query.askedBy);
            await dispatch(fetchUserDetailsIfNeeded(userIds))
            dispatch({type:'RECEIVE_EDUCATOR_REG_QUERY',classId:classId,regQueries:queryList})
        }
        return;
    }
}


export const closeQuery = (classId:number,queryId:number) => {
    return function(dispatch:(arg0:EducatorRegQueryCloseAction) => void) {
        return educatorRegQueryStatusUpdate(classId,queryId)
        .then(() => {
            dispatch({type:'EDUCATOR_CLOSE_REG_QUERY',classId:classId,queryId:queryId})
            return true;
        },
        err => {
            return false;
        })
    }
}