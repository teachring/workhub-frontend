import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import { purple, pink } from '@material-ui/core/colors';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: "inline-flex",
            flexDirection: "column",
            padding: "2rem",
            width: "100%",
            maxWidth: 700,
        },
        card: {
            maxWidth: 750,
            margin: "2rem 0",
            padding: "1rem"
        },
        icontext: {
            display: "flex",
            padding: "0.5rem 1rem",
            alignItems: "center",
        },
        icon: {
            marginRight: 10,
            color: purple[400],
            // color: orange[500],
            // color: pink[500],
            fontSize: "1.4rem",
            borderRadius: "50%",
        },
        detail: {
            fontWeight: "lighter",
            fontSize: "0.9rem"
        },
        question: {
            fontSize: "1.3rem",
            textAlign: "left",
            alignSelf: "flex-start"
        },
        questionIcon: {
            alignSelf: "flex-start",
            marginTop: 8
        },
        closeIcon: {
            marginRight: 6,
            fontSize: "1.2rem",
        },
        closeFab: {
            backgroundColor: "#fff",
            border: "1px solid",
            borderColor: purple[500],
            color: purple[500],
            "&:hover": {
                backgroundColor: purple[500],
                color: "#fff",
                boxShadow: "0 2px 10px 0px rgba(0, 0, 0, 0.14), 0 4px 10px -5px rgba(156, 39, 176, 0.46)",
            }
        },
        [theme.breakpoints.down("xs")]: {
            root: {
                padding: "1rem"
            },
            card: {
                margin: "1rem 0",
            }
        }
    }),
);

export default useStyles;