import React, { useEffect } from 'react';
import { Box, Typography, Paper, makeStyles, Theme, createStyles, Fab, CircularProgress } from '@material-ui/core';
import { Face, Call, Help, QueryBuilderOutlined, CloseOutlined } from '@material-ui/icons';
import clsx from "clsx";
import { purple } from '@material-ui/core/colors';
import useStyles from "./reg-queries-styles";
import { EducatorRegQuery } from '../../../../utils/core-types/educator-types';
import { User } from '../../../../utils/core-types/user-types';
import { timeSince } from '../../../../utils/helpers';
import Heading from '../../../shared/heading/heading';
import EmptyState from '../../../shared/empty/empty';
import EmptyImg from "../../../../assets/images/no-enquiry.svg";

type IProp = {
    regQueries?: EducatorRegQuery[], //if unedfined, means fetching..
    fetchqueriesIfNeeded: () => void,
    queryCloseReq: (queryId: number) => Promise<boolean>,
    getUserDetails: (userId: number) => User
}

const RegQueries: React.FC<IProp> = ({ regQueries, fetchqueriesIfNeeded, queryCloseReq, getUserDetails }) => {

    const classes = useStyles();

    useEffect(() => {
        fetchqueriesIfNeeded()
    }, [])

    const card = (query: EducatorRegQuery, user: User) => (
        <Paper elevation={2} className={classes.card}>
            <Box className={classes.icontext}>
                {/* <Help className={clsx(classes.icon, classes.questionIcon)} /> */}
                <Typography className={classes.question}>{query.query}</Typography>
            </Box>
            <Box display="flex" alignItems="center" mt={2} flexWrap="wrap">
                <Box className={classes.icontext}>
                    <Face className={classes.icon} />
                    <Typography className={classes.detail}>{user.userName}</Typography>
                </Box>
                <Box className={classes.icontext}>
                    <Call className={classes.icon} />
                    <Typography className={classes.detail}>{user.phoneNumber}</Typography>
                </Box>
            </Box>
            <Box display="flex" alignItems="center" flexWrap="wrap">
                <Box className={classes.icontext}>
                    <QueryBuilderOutlined className={classes.icon} />
                    <Typography className={classes.detail}>{timeSince(query.askedDateTime)}</Typography>
                </Box>
                <Box ml="auto">
                    <Fab className={classes.closeFab} variant="extended" size="medium" aria-label="close" >
                        <CloseOutlined className={classes.closeIcon} /> close
                    </Fab>
                </Box>
            </Box>
        </Paper>
    )

    return (
        <Box className={classes.root}>
            <Heading>Registration Enquiries</Heading>
            {
                regQueries
                    ? regQueries.length > 0
                        ? regQueries.map((query: EducatorRegQuery) => (
                            card(query, getUserDetails(query.askedBy))
                        ))
                        : <EmptyState img="https://s3.ap-south-1.amazonaws.com/deploy.teachring.com/assets/images/no-enquiry.svg" text="No enquiries yet" />
                    : <CircularProgress color="primary" />
            }
        </Box>
    );
}


export default RegQueries;