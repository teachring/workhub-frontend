import React, { useEffect } from 'react';
import { User } from '../../../../utils/core-types/user-types';
import useStyles from "./moderators-styles";
import { Box, TextField, IconButton, Paper, List, ListItem, ListItemAvatar, Avatar, ListItemText, CircularProgress, Typography } from '@material-ui/core';
import {
    PhoneRounded as PhoneRoundedIcon,
    SearchRounded as SearchRoundedIcon,
    Face,
    Call
} from "@material-ui/icons";
import SearchDialog from "./dialog";
import Heading from '../../../shared/heading/heading';
import EmptyState from '../../../shared/empty/empty';
import EmptyImg from "../../../../assets/images/no-collaborator.svg";

type IProp = {
    moderatorList?: User[],  //undefined when waiting for data to be fetched.
    fetchModeratorsIfNeeded: () => void,
    searchByPhoneNum: (phoneNum: string) => Promise<User | null>  // null when no user is found for the phone number
    addNewModerator: (userId: number) => Promise<boolean>
}

const ModeratorMgt: React.FC<IProp> = ({ moderatorList, addNewModerator, searchByPhoneNum, fetchModeratorsIfNeeded }) => {
    const classes = useStyles();

    const [searchText, setSearchText] = React.useState("");
    const handleSearch = (event: React.ChangeEvent<HTMLInputElement>) => {
        setSearchText(event.target.value);
    };

    const [open, setOpen] = React.useState(false);
    const handleOpen = () => {
        setOpen(true);
    };
    const handleClose = (confirm: boolean) => {
        if (confirm && searchResult)
            addNewModerator((searchResult as User).id);

        setOpen(false);
    };

    const [searchResult, setSearchResult] = React.useState<User | null | undefined>(undefined);

    const search = () => {
        handleOpen();
        searchByPhoneNum(searchText)
            .then(result => setSearchResult(result));
    }

    useEffect(() => {
        fetchModeratorsIfNeeded()
    }, []);

    return (
        <Box className={classes.root}>
            <Heading>Collaborators</Heading>
            <Box mb={4} alignSelf="center">
                <TextField
                    label="Enter mobile no."
                    value={searchText}
                    onChange={handleSearch}
                />
                <IconButton className={classes.searchBtn} color="primary" aria-label="search" onClick={search}>
                    <SearchRoundedIcon />
                </IconButton>
                <SearchDialog result={searchResult} open={open} handleClose={handleClose} />
            </Box>
            {
                moderatorList
                    ? moderatorList.length > 0
                        ? moderatorList.map((moderator: User) => (
                            <Paper className={classes.card} elevation={2}>
                                <Box className={classes.icontext}>
                                    <Face color="primary" /><Typography>{moderator.userName}</Typography>

                                </Box>
                                {
                                    moderator.phoneNumber
                                        ? <Box className={classes.icontext}>
                                            <Call color="primary" /><Typography>{moderator.phoneNumber}</Typography>
                                        </Box>
                                        : ""
                                }

                            </Paper>
                        ))
                        : <EmptyState img="https://s3.ap-south-1.amazonaws.com/deploy.teachring.com/assets/images/no-collaborator.svg" text="No collaborators yet" />
                    : <CircularProgress color="secondary" />
            }
        </Box>
    );
}

export default ModeratorMgt;