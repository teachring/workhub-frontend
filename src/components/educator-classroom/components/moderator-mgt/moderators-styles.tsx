import { createStyles, Theme, makeStyles } from '@material-ui/core/styles';
import { purple } from '@material-ui/core/colors';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: "inline-flex",
            flexDirection: "column",
            alignItems: "flex-start",
            maxWidth: 400,
            width: "100%",
            padding: "2rem"
        },
        paper: {
            maxWidth: 400,
            width: "100%",
            margin: "auto",
            boxShadow: "0 1px 4px 0 rgba(0, 0, 0, 0.14)"
        },
        searchBtn: {
            position: "absolute"
        },
        card: {
            width: "100%",
            margin: "2rem 0 3rem 0",
            padding: "2rem",
            paddingLeft: "3rem",
            position: "relative",
            overflow: "hidden",
            "&:after": {
                content: "''",
                position: "absolute",
                top: 0,
                left: 0,
                backgroundColor: purple[500],
                width: 5,
                height: "100%"
            }
        },
        icontext: {
            display: "flex",
            alignItems: "center",
            "&:first-child": {
                marginBottom: "1rem"
            },
            "& svg": {
                marginRight: "1rem"
            }
        },
        [theme.breakpoints.down("xs")]: {
            root: {
                padding: "1rem"
            }
        }
    }),
);

export default useStyles;