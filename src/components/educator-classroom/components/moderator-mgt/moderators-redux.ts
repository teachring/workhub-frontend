import { connect } from 'react-redux';
import { RootState } from '../../../../utils/reduxReducers';
import { addNewModerator, findModeratorByPhone, fetchModeratorsIfNeeded } from './actions';
import { User } from '../../../../utils/core-types/user-types';
import ModeratorMgt from './moderators';

const mapStateToProps = (state: RootState, ownProps: any) => {

    var modUserList: User[] = []
    const idList = state.ClassInfoList[ownProps.classId].moderatorList
        .filter((userId: number) => {
            return userId !== state.AppState.userId
        });

    idList.forEach((userId) => {
        if (state.UsersDB[userId]) {
            modUserList.push(state.UsersDB[userId])
        }
    })

    return {
        moderatorList: (modUserList.length === idList.length) ? modUserList : undefined
    }
}

const mapDispatchToProps = (dispatch: Function, ownProps: any) => {
    return {
        searchByPhoneNum: (phoneNum: string): Promise<User | null> => {
            return dispatch(findModeratorByPhone(phoneNum))
        },
        addNewModerator: (userId: number): Promise<boolean> => {
            return dispatch(addNewModerator(ownProps.classId, userId))
        },
        fetchModeratorsIfNeeded: (): void => {
            return dispatch(fetchModeratorsIfNeeded(ownProps.classId));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ModeratorMgt);