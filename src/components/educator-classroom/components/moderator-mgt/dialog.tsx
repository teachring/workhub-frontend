import React from 'react';
import { Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, Button } from "@material-ui/core";
import { User } from '../../../../utils/core-types/user-types';

interface IProp {
    open: boolean;
    handleClose: (confirm: boolean) => void;
    result?: User | null
}

const SearchDialog: React.FC<IProp> = ({ open, handleClose, result }) => {

    var heading = "Fetching...";
    var body = "";
    var userFound = false;

    if (result !== undefined) {
        if (result === null) {
            heading = "no user found";
        } else {
            userFound = true;
            heading = "Are you sure?";
            body = `Do you want to add ${result!.userName} (${result!.phoneNumber}) as a moderator?`;
        }
    }

    return (
        <div>
            <Dialog
                open={open}
                aria-labelledby="confirm-dialog"
                aria-describedby="confirm-add-moderator"
            >
                <DialogTitle id="confirm-dialog">{heading}</DialogTitle>
                {
                    userFound ? (
                        <DialogContent>
                            <DialogContentText id="confirm-add-moderator">{body}</DialogContentText>
                        </DialogContent>
                    ) : ""
                }
                <DialogActions>
                    {
                        userFound ? (
                            <Button onClick={() => handleClose(false)} color="primary" autoFocus>
                                CANCEL
                            </Button>
                        ) : ""
                    }
                    <Button onClick={() => handleClose(true)} color="primary" autoFocus>
                        OK
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}

export default SearchDialog;