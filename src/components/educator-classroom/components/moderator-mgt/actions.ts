import {addModerator,findModByPhoneNum} from '../../../../utils/apis/educators';
import {UserDBAction,fetchUserDetailsIfNeeded} from '../../../../utils/reduxActions/users-db';
import { RootState } from '../../../../utils/reduxReducers';

export type AddModeratorAction = {
    type: 'EDUCATOR_ADD_MODERATOR',
    classId:number,
    userId:number
}

export const addNewModerator = (classId:number,userId:number) => {
    return function(dispatch:(arg0:AddModeratorAction) => any) {
        return addModerator(classId,userId)
        .then(() => {
            dispatch({type:'EDUCATOR_ADD_MODERATOR',classId:classId,userId:userId})
            return true;
        },
        err => {
            return false
        }
    )}
}

export const findModeratorByPhone = (phoneNo:string) => {
    return function(dispatch:(arg0:UserDBAction) => any) {
        return findModByPhoneNum(phoneNo)
        .then((userObj) => {
            if(userObj!=null){
                dispatch({type:'RECEIVE_USER_LIST',users:[userObj]})
            }
            return userObj;
        },
        err => {return null})
    }
}

export const fetchModeratorsIfNeeded = (classId:number) => {
    return function(dispatch:(arg0:any) =>any,getState:()=>RootState){
        dispatch(fetchUserDetailsIfNeeded(getState().ClassInfoList[classId].moderatorList))
    }
}