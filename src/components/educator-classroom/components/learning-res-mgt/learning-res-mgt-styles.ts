import { makeStyles, createStyles, Theme } from '@material-ui/core';
import { purple } from '@material-ui/core/colors';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        newResourceBox: {
            width: "100%",
            padding: "1rem 2rem",
            textAlign: "left",
        },
        saveBtn: {
            marginTop: "1rem",
            marginRight: "2rem"
        },
        content: {
            fontWeight: "lighter"
        },
        titleInput: {
            marginTop: "1rem",
            marginRight: "2rem"
        },
        ckeditor: {
            minHeight: 300
        },
        // circularProgress: {
        //     margin: "1rem auto"
        // },
        [theme.breakpoints.down("xs")]: {
            paper: {
                maxWidth: "100%",
                margin: "2rem 0"
            },
            titleInput: {
                flex: "100%",
                marginRight: 0
            },
            saveBtn: {
                marginRight: 0,
                flex: "100%"
            }
        },
    }),
);

export default useStyles;