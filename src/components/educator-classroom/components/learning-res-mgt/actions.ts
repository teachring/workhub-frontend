import { LearningRes } from "../../../../utils/core-types/shared";
import { RootState } from "../../../../utils/reduxReducers";
import {educatorFetchAllDocs,educatorPostNewDoc} from '../../../../utils/apis/learning-resources';
import { fetchUserDetailsIfNeeded } from "../../../../utils/reduxActions/users-db";

export type RecvLearningResAction = {
    type: 'RECEIVE_LEARNING_RESOURCE',
    classId: number,
    resList: LearningRes[]
}

export type EducatorPublishLearnResAction = {
    type: 'EDUCATOR_PUBLISH_LEARNING_RESOURCE',
    classId: number,
    learningRes: LearningRes
}

export const fetchLearningResIfNeeded = (classId:number) => {
    return async function(dispatch:Function,getState:() => RootState) {
        if(!getState().ClassLearningRes[classId]) {
            let resList = await educatorFetchAllDocs(classId);
            let userIds = resList.map(resObj=> {return resObj.createdBy})
            await dispatch(fetchUserDetailsIfNeeded(userIds));
            dispatch({type:'RECEIVE_LEARNING_RESOURCE',classId:classId,resList:resList})
        }
        return;
    }
}

export const publishNewLearningRes = (classId:number,docTitle:string,docBody:string) => {
    return function(dispatch:(arg0:EducatorPublishLearnResAction)=>any) {
        return educatorPostNewDoc(classId,docTitle,docBody)
        .then(newObj => {
            dispatch({type:'EDUCATOR_PUBLISH_LEARNING_RESOURCE',classId:classId,learningRes:newObj})
            return true;
        },
        err => {
            return false
        }
    )

    }
}