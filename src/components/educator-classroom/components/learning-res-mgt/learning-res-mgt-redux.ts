import { RootState } from "../../../../utils/reduxReducers";
import { connect } from "react-redux";
import LearningResMgt from "./learning-res-mgt";
import { fetchLearningResIfNeeded,publishNewLearningRes} from './actions';
import { AlertMessageType,dispatchAlertAction } from "../../../alert-snackbar/alert-snackbar-actions";

const mapStateToProps = (state:RootState,ownProps:{classId:number}) => {
    return {
        resList: state.ClassLearningRes[ownProps.classId],
        getUserDetails: (userId: number) => {
            return state.UsersDB[userId];
        }
    }

}

const mapDispatchToProps = (dispatch:Function,ownProps:{classId:number}) => {
    return {
        fetchResourcesIfNeeded: () => {
            dispatch(fetchLearningResIfNeeded(ownProps.classId))
        },
        onNewResourcePublishReq: (title:string,body:string) : Promise<boolean> => {
            return dispatch(publishNewLearningRes(ownProps.classId,title,body))
        },
        publishAlerts : (type:AlertMessageType,message:string) => {
            dispatchAlertAction(dispatch,type,message);
        },
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(LearningResMgt);