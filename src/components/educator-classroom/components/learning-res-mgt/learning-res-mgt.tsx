import React, { useEffect } from 'react';
import { LearningRes } from '../../../../utils/core-types/shared';
import { User } from '../../../../utils/core-types/user-types';
import { Paper, Box, Typography, Button, Collapse, TextField, CircularProgress, IconButton } from '@material-ui/core';
import { Description, Face, ExpandLess, ExpandMore } from '@material-ui/icons';
import CKEditor from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import Heading from '../../../shared/heading/heading';
import clsx from "clsx";
import useStyles from './learning-res-mgt-styles';
import WaitingDialog from '../../../dialog-screens/waiting-dialog';
import { AlertMessageType } from '../../../alert-snackbar/alert-snackbar-actions';
import ResourceCard from '../../../shared/cards/ResourceCard';
import EmptyImg from "../../../../assets/images/no-learning-res.svg";
import EmptyState from '../../../shared/empty/empty';

type IProp = {
    resList?: LearningRes[],
    fetchResourcesIfNeeded: () => void,
    onNewResourcePublishReq: (title: string, body: string) => Promise<boolean>,
    getUserDetails: (userId: number) => User,
    publishAlerts: (type: AlertMessageType, message: string) => void
}


const LearningResMgt: React.FC<IProp> = ({ resList, fetchResourcesIfNeeded, onNewResourcePublishReq, getUserDetails, publishAlerts }) => {

    const classes = useStyles();

    const [saveError, setSaveError] = React.useState("");
    const [waiting, setWaiting] = React.useState(false);
    const [publishDialogOpen, setPublishDialogOpen] = React.useState(false);

    const [title, setTitle] = React.useState("");
    const handleTitle = e => {
        setTitle(e.target.value);
    }
    const [doc, setDoc] = React.useState("");

    const handleSave = () => {
        console.log({ doc })
        if (title.trim().length === 0) {
            setSaveError("Title is required");
        } else if (doc.trim().length === 0) {
            setSaveError("Content is required");
        } else {
            setSaveError("");
            setPublishDialogOpen(true);
        }
    }

    const [open, setOpen] = React.useState(false);
    const toggleEditor = () => {
        setOpen(prev => !prev);
    }

    const [expand, setExpand] = React.useState({});

    const handleExpand = (resId) => {
        console.log(resId)
        setExpand(prevState => {
            return {
                ...prevState,
                [resId]: !prevState[resId]
            }
        })
    }

    useEffect(() => {
        //console.log(expand)
    }, [expand])

    useEffect(() => {
        fetchResourcesIfNeeded();
    }, []);

    const createThumbnail = (htmlString: string) => {
        const maxTagsCount = 4;
        const pattern = /(\<[^\/]*?\>.*?<\/.*?>)/g;
        const tags = htmlString.split(pattern);
        let i = 0;
        let count = 0;
        let result = "";
        while (count < maxTagsCount && i < tags.length) {
            if (tags[i].length > 0) {
                result += tags[i];
                count++;
            }
            i++;
        }
        return result;
    }

    const handlePublishDialogClose = (confirm: boolean) => {
        if (confirm) {
            setWaiting(true);
            onNewResourcePublishReq(title, doc)
                .then((isSuccess: boolean) => {
                    setWaiting(false);
                    setPublishDialogOpen(false);
                    //Now when you say that 
                    if (isSuccess) {
                        toggleEditor()
                        publishAlerts(AlertMessageType.SUCCESS_MESSAGE, title + ' published successfully, learners can access it now.')
                        setTitle('')
                        setDoc('')
                    } else {
                        publishAlerts(AlertMessageType.ERROR_MESSAGE, 'Material could not be published, please try again');
                    }

                })
        } else {
            setPublishDialogOpen(false);
        }
    }

    return (
        <Box display="flex" alignItems="flex-start" flexWrap="wrap">


            <Heading ml="2rem">Learning materials</Heading>
            <Box className={classes.newResourceBox}>
                {
                    !open
                        ? <Button variant="contained" color="primary" onClick={toggleEditor}>Add new material</Button>
                        : <Heading>New material</Heading>
                }


                <Collapse in={open} timeout="auto">
                    <Box mt={4} mb={2} display="flex" flexWrap="wrap" alignItems="center">
                        <TextField
                            className={classes.titleInput}
                            value={title}
                            onChange={handleTitle}
                            variant="outlined"
                            label="Enter title"
                            disabled={waiting}
                        />
                        {/* {waiting && <CircularProgress className={classes.circularProgress} color="primary" />} */}
                        <Button className={classes.saveBtn} variant="contained" color="primary" onClick={handleSave} disabled={waiting}>Publish</Button>
                        <Button className={classes.saveBtn} variant="outlined" color="primary" onClick={toggleEditor} disabled={waiting}>Cancel</Button>
                    </Box>

                    <Box mb={1}>
                        <Typography color="error" variant="body2">{saveError}</Typography>
                    </Box>

                    <CKEditor
                        className={classes.ckeditor}
                        config={{
                            removePlugins: ['Image', 'ImageCaption', 'ImageStyle', 'ImageToolbar', 'ImageUpload', 'MediaEmbed']
                        }}
                        data={doc}
                        onChange={(event, editor) => setDoc(editor.getData())}
                        editor={ClassicEditor}
                        onInit={editor => {
                            editor.ui.view.editable.editableElement && (editor.ui.view.editable.editableElement.style.height = "400px !important");
                        }}
                        disabled={waiting}
                    />
                    <WaitingDialog open={publishDialogOpen} waiting={waiting} onDialogClose={handlePublishDialogClose} dialogTitle='Are you sure ?' okBtnText="Publish">
                        <Typography>Once published, you cannot edit the material and it will be accessible by all the learners in your class</Typography>
                    </WaitingDialog>
                </Collapse>
            </Box>

            {
                resList
                    ? resList.length > 0
                        ?
                        resList.map(resource => (
                            <ResourceCard resource={resource} getUserDetails={getUserDetails} />
                        ))
                        : <Box width="100%" margin="auto" mt={5}>
                            <EmptyState img="https://s3.ap-south-1.amazonaws.com/deploy.teachring.com/assets/images/no-learning-res.svg" text="No materials yet" />
                        </Box>
                    : <Box textAlign="center" width="100%" mt={3}><CircularProgress color="primary" /></Box>
            }

        </Box>
    );
}

export default LearningResMgt;