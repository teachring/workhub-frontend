import React from 'react';
import useStyles from './overview-styles';
import { ButtonBase, Box, Typography } from '@material-ui/core';
import { useHistory } from 'react-router-dom';

import { ROUTES } from "../../../../utils/core-types/educator-types";
import LearningResourcesIcon from "../../../../assets/icons/LearningResources";
import QuesAnsIcon from "../../../../assets/icons/QuesAns";
import AnnouncementsIcon from "../../../../assets/icons/Announcements";
import QueriesIcon from "../../../../assets/icons/Enquiries";
import ParticipantsIcon from "../../../../assets/icons/Participants";
import CollaboratorsIcon from "../../../../assets/icons/Collaborators";

const Overview: React.FC = () => {
    const classes = useStyles();

    let history = useHistory();

    const handleClick = (route: ROUTES) => {
        history.push(history.location.pathname.replace(/\/$/, "") + route);
    }

    return (
        <Box className={classes.root}>
            <ButtonBase className={classes.card} onClick={() => handleClick(ROUTES.RESOURCES)}>
                <LearningResourcesIcon className={classes.icon} />
                <Box>
                    <Typography className={classes.title}>Learning materials</Typography>
                    <Typography className={classes.desc}>Publish and distribute your learning resources, lesson plans</Typography>
                </Box>
            </ButtonBase>

            <ButtonBase className={classes.card} onClick={() => handleClick(ROUTES.QA)}>
                <QuesAnsIcon className={classes.icon} />
                <Box>
                    <Typography className={classes.title}>Learner Q &amp; A</Typography>
                    <Typography className={classes.desc}>Read student questions and feedback, send instant replies.</Typography>
                </Box>
            </ButtonBase>

            <ButtonBase className={classes.card} onClick={() => handleClick(ROUTES.ANNOUCEMENTS)}>
                <AnnouncementsIcon className={classes.icon} />
                <Box>
                    <Typography className={classes.title}>Announcements</Typography>
                    <Typography className={classes.desc}>Send instant SMS notification to your class</Typography>
                </Box>
            </ButtonBase>

            <ButtonBase className={classes.card} onClick={() => handleClick(ROUTES.QUERIES)}>
                <QueriesIcon className={classes.icon} />
                <Box>
                    <Typography className={classes.title}>Enquiries</Typography>
                    <Typography className={classes.desc}>Follow up on registration enquiries for your class</Typography>
                </Box>
            </ButtonBase>
            <ButtonBase className={classes.card} onClick={() => handleClick(ROUTES.PARTICIPANTS)}>
                <ParticipantsIcon className={classes.icon} />
                <Box>
                    <Typography className={classes.title}>Participants</Typography>
                    <Typography className={classes.desc}>Check out student information, manage payments</Typography>
                </Box>
            </ButtonBase>
            <ButtonBase className={classes.card} onClick={() => handleClick(ROUTES.MODERATORS)}>
                <CollaboratorsIcon className={classes.icon} />
                <Box>
                    <Typography className={classes.title}>Collaborators</Typography>
                    <Typography className={classes.desc}>Add collaborators to your class, teach together</Typography>
                </Box>
            </ButtonBase>
        </Box>
    );
}

export default Overview;
