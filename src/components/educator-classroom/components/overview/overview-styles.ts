import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import red from '@material-ui/core/colors/red';
import { purple } from '@material-ui/core/colors';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: "flex",
            flexWrap: "wrap",
            padding: "2rem",
            justifyContent: "center"
        },
        icon: {
            fontSize: "5rem",
        },
        title: {
            marginTop: "1rem",
            fontSize: "1.2rem",
            marginBottom: ".6rem"
        },
        desc: {
            fontWeight: "lighter"
        },
        card: {
            width: "100%",
            maxWidth: 350,
            padding: "3rem",
            margin: "2rem",
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            boxShadow: theme.shadows[1],
            backgroundColor: theme.palette.background.paper,
            borderRadius: theme.shape.borderRadius,
            "&:hover": {
                boxShadow: theme.shadows[3]
            }
        },
        [theme.breakpoints.down("sm")]: {
            root: {
                padding: 0
            }
        }
    }),
);

export default useStyles;