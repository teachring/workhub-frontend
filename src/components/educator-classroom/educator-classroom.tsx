import React from 'react';
import { Route, Switch, useRouteMatch, useHistory, useParams } from 'react-router-dom'
import {
    DashboardRounded as DashboardRoundedIcon,
    Book as BookIcon,
    Announcement as AnnouncementIcon,
    QuestionAnswer as QuestionAnswerIcon,
    LiveHelp as LiveHelpIcon,
    PeopleAlt as PeopleAltIcon,
    AssessmentRounded as AssessmentRoundedIcon,
    GroupAddRounded as GroupAddRoundedIcon
} from '@material-ui/icons';

import { ROUTES } from "../../utils/core-types/educator-types";
import { AnnouncementComp } from "./components/announcements";
import { RegQueryComp } from "./components/reg-queries";
import { EducatorQuesAnsComp } from "./components/ques-ans";
import { ModeratorMgtComp } from "./components/moderator-mgt";
import { LearnerMgtComp } from "./components/learner-mgt";
import { LearningResMgtComp } from './components/learning-res-mgt';
import { DrawerData } from '../../utils/core-types/shared';
import Dashboard from "../shared/dashboard/Dashboard";
import { extractIdFromClassUrl } from '../../utils/helpers';
import { OverviewComp } from './components/overview';

const EducatorDashboardData: DrawerData = {
    base: ROUTES.BASE,
    lists: [
        {
            items: [
                {
                    url: ROUTES.EMPTY,
                    text: "Class Overview",
                    icon: <DashboardRoundedIcon />
                }
            ]
        },
        {
            heading: "Educator",
            items: [
                {
                    url: ROUTES.RESOURCES,
                    text: "Learning materials",
                    icon: <BookIcon />
                },
                {
                    url: ROUTES.QA,
                    text: "Learner Q&A",
                    icon: <QuestionAnswerIcon />
                },
                {
                    url: ROUTES.ANNOUCEMENTS,
                    text: "Annoucements",
                    icon: <AnnouncementIcon />
                }
            ]
        },
        {
            heading: "Admin",
            items: [
                {
                    url: ROUTES.QUERIES,
                    text: "Enquiries",
                    icon: <LiveHelpIcon />
                },
                {
                    url: ROUTES.PARTICIPANTS,
                    text: "Participants",
                    icon: <PeopleAltIcon />
                },
                {
                    url: ROUTES.MODERATORS,
                    text: "Collaborators",
                    icon: <GroupAddRoundedIcon />
                }
            ]
        }
    ]
}

const EducatorClassroom: React.FC = () => {

    //Now yaha par I need to find out the class id all those stuff

    const { params, url } = useRouteMatch();
    const classId = extractIdFromClassUrl(params.class_name!);
    const history = useHistory();
    var currentTabUrl = history.location.pathname.split('/').slice(4).join();
    if (currentTabUrl != '') currentTabUrl = '/' + currentTabUrl;

    const handleTabChange = (newTabUrl) => {
        //console.log("URL ",url);
        //console.log("Hello World",newTabUrl);
        history.push(url + newTabUrl);
    }


    ///var currentTabUrl = //I need to find out the right 

    return (
        <Dashboard drawerData={EducatorDashboardData} currentTabUrl={currentTabUrl} onTabChange={handleTabChange} classId={classId}>
            <Switch>
                <Route exact path={ROUTES.BASE}>
                    <OverviewComp />
                </Route>
                <Route path={ROUTES.BASE + ROUTES.RESOURCES}>
                    <LearningResMgtComp classId={classId} />
                </Route>
                <Route path={ROUTES.BASE + ROUTES.QA}>
                    <EducatorQuesAnsComp classId={classId} />
                </Route>
                <Route path={ROUTES.BASE + ROUTES.ANNOUCEMENTS}>
                    <AnnouncementComp classId={classId} />
                </Route>
                <Route path={ROUTES.BASE + ROUTES.QUERIES}>
                    <RegQueryComp classId={classId} />
                </Route>
                <Route path={ROUTES.BASE + ROUTES.PARTICIPANTS}>
                    <LearnerMgtComp classId={classId} />
                </Route>
                <Route path={ROUTES.BASE + ROUTES.MODERATORS}>
                    <ModeratorMgtComp classId={classId} />
                </Route>
            </Switch>
        </Dashboard>
    )
}

export default EducatorClassroom;
