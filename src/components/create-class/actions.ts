import { NewDraftObj, SavedDraftInfo } from "../../utils/core-types/educator-types"
import { ClassInfo, NewClassObj } from "../../utils/core-types/class-types"
import {publishClassFromDraft, createNewDraft,createNewClass,updateExistingDraft} from '../../utils/apis/educators';

export type DraftCreateAction = {
    type: 'DRAFT_CREATE'
    newDraft: SavedDraftInfo
}

export type ClassCreateAction = {
    type: 'CLASS_PUBLISH' 
    newClass:ClassInfo
}


export const onDraftCreate = (draftObj:NewDraftObj,draftId?:number) => {
    return function(dispatch:(arg0:DraftCreateAction)=> any) : Promise<boolean> {
        var draftSavePromise:Promise<boolean>;
        if(draftId === undefined) {
            draftSavePromise = createNewDraft(draftObj)
            .then((newDraft) => {
                dispatch({type:'DRAFT_CREATE',newDraft:newDraft})
                return true;
            },
            err => {
                return false
            })
        } else {
            draftSavePromise = updateExistingDraft(draftObj,draftId)
            .then((newDraft) => {

                dispatch({type:'DRAFT_CREATE',newDraft:newDraft})
                return true;
            }, 
            err => {
                return false;
            })
        }

        return draftSavePromise;
    }

}

export const onClassCreate = (classObj:NewClassObj,draftId?:number) => {

    return function(dispatch:(arg0:ClassCreateAction)=> any) : Promise<boolean> {
        var newClassPromise:Promise<boolean>;
        if(draftId === undefined) {
            newClassPromise = createNewClass(classObj)
            .then((newClass) => {
                dispatch({type:'CLASS_PUBLISH',newClass:newClass})
                return true;
            },
            err => {
                return false
            })
        } else {
            newClassPromise = publishClassFromDraft(classObj,draftId)
            .then((newClass) => {

                dispatch({type:'CLASS_PUBLISH',newClass:newClass})
                return true;
            }, 
            err => {
                return false;
            })
        }

        return newClassPromise;
    }
    
}