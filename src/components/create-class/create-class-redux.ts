import { RootState } from "../../utils/reduxReducers";
import { NewClassObj } from "../../utils/core-types/class-types";
import { onClassCreate,onDraftCreate} from './actions';
import { NewDraftObj } from "../../utils/core-types/educator-types";
import { connect } from "react-redux";
import CreateClass from './create-class';
import { AlertMessageType,dispatchAlertAction } from "../alert-snackbar/alert-snackbar-actions";
import { withRouter } from "react-router-dom";

const mapStateToProps = (state:RootState,ownProps:any) => {
    console.log("HOLLO",ownProps.history.location)
    if(ownProps.history.location.state?.draftId){
        return {
            savedDraft : state.EducatorState.DraftList?.[ownProps.history.location.state.draftId]
        }
    } else {
        return {}
    }
}

const mapDispatchToProps = (dispatch:Function,ownProps:any) => {

    //const draftId = ownProps.history.state?.draftId

    return {
        onClassCreate: (newClassObj:NewClassObj,draftId?: number) : Promise<boolean> => {
            return dispatch(onClassCreate(newClassObj,draftId))
        },
        onDraftCreate: (newDraftObj:NewDraftObj,draftId?: number) : Promise<boolean> => {
            return dispatch(onDraftCreate(newDraftObj,draftId))

        },
        publishAlerts : (type:AlertMessageType,message:string) => {
            dispatchAlertAction(dispatch,type,message);
        },
    }
}

export default withRouter(connect(mapStateToProps,mapDispatchToProps)(CreateClass));