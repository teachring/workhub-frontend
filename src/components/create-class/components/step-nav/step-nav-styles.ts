import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
const styles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            backgroundColor: "#fff",
            boxShadow: "0px 6px 10px -7px rgba(0,0,0,0.2)"
        },
        toolbar: {
            flexWrap: "wrap",
            maxWidth: 750,
            margin: "auto",
            width: "100%",
            padding: 0
        },
        stretchHeight: {
            minHeight: 80,
            minWidth: 80,
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
        },
        stepNumber: {
            background: theme.palette.primary.main,
            marginRight: "2rem",
            fontWeight: "lighter",
            minWidth: 80,
            "&>.MuiTypography-root": {
                color: "#fff",
                letterSpacing: "0.2em"
            }
        },
        stepName: {
            fontWeight: 200
        },
        stepActionBtn: {
            marginRight: "1rem",
        },
        stepActions: {
            marginLeft: "auto"
        },
        [theme.breakpoints.down("xs")]: {
            stepActions: {
                margin: "auto"
            },
            stepActionBtn: {
                margin: theme.spacing(1)
            }
        }
    })
);

export default styles;