import React from "react";
import Box from "@material-ui/core/Box";
import { AttachMoney, Event, Room, ChevronRight, ChevronLeft } from "@material-ui/icons";
import { Typography, TextField, FormGroup, FormControlLabel } from "@material-ui/core";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Button from "@material-ui/core/Button";
import useStyles from './step-nav-styles';
import { RegFormState } from "../../create-class";
import clsx from "clsx";

type IProp = {
    stepData: RegFormState,
    step: any,
    handleStep: React.Dispatch<any>,
    onDraftSave: () => void,
    onClassPublishReq: () => void
}

const StepNav: React.FC<IProp> = ({ stepData, step, handleStep, onDraftSave, onClassPublishReq }) => {

    const classes = useStyles();

    return (
        <AppBar className={classes.root} position="sticky">
            <Toolbar className={classes.toolbar}>
                <div className={clsx(classes.stepNumber, classes.stretchHeight)}>
                    <Typography variant="h6">{step.currentStep + 1}/{step.titles.length}</Typography>
                </div>
                <Typography className={classes.stepName} variant="h6">{step.titles[step.currentStep]}</Typography>

                <Box className={clsx(classes.stepActions, classes.stretchHeight)}>
                    <Button
                        className={classes.stepActionBtn}
                        variant="outlined"
                        color="primary"
                        disabled={Object.keys(stepData.errors).length !== 0}
                        onClick={() =>
                            onDraftSave()
                        }
                    >
                        Save as draft
                    </Button>

                    {
                        step.currentStep !== 0 ? (
                            <Button
                                className={classes.stepActionBtn}
                                variant="outlined"
                                color="primary"
                                onClick={() => handleStep("previous")}
                            >
                                <ChevronLeft />
                            </Button>
                        ) : null
                    }
                    {
                        step.currentStep === step.titles.length - 1 ? (
                            <Button
                                className={classes.stepActionBtn}
                                variant="outlined"
                                color="primary"
                                disabled={Object.keys(stepData.errors).length !== 0}
                                onClick={() => onClassPublishReq()}
                            >
                                Submit
                                </Button>
                        ) : (
                                <Button
                                    className={classes.stepActionBtn}
                                    variant="outlined"
                                    color="primary"
                                    disabled={Object.keys(stepData.errors).length !== 0}
                                    onClick={() => handleStep("next")}
                                >
                                    <ChevronRight />
                                </Button>
                            )
                    }


                </Box>
            </Toolbar>
        </AppBar>
    );
}

export default StepNav;