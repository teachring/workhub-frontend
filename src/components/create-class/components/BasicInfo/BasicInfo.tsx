import React from "react";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import { Typography, TextField, FormGroup, Grid, Box, withStyles, IconButton } from "@material-ui/core";
import clsx from "clsx";
import DateFnsUtils from '@date-io/date-fns';
import { KeyboardDatePicker, MuiPickersUtilsProvider, TimePicker } from '@material-ui/pickers';

import commonStyles from "../../common-style";
import { StepperProp } from "../../create-class";
import { Drafts } from "@material-ui/icons";
import Heading from "../../../shared/heading/heading";
import { startOfWeek, endOfWeek, isWithinInterval, isSameDay, isPast, format, isBefore, sub, addDays, subDays, isAfter, max, set } from "date-fns";

const styles = makeStyles((theme: Theme) =>
    createStyles({
        wsPicker: {
            width: "auto",
            "&::nth-child(1)": {
                background: "red"
            }
        },
        wsDatePicker: {
            marginRight: theme.spacing(2)
        },

        // date picker styles
        dayWrapper: {
            position: "relative",
        },
        day: {
            width: 36,
            height: 36,
            fontSize: theme.typography.caption.fontSize,
            color: "inherit",
        },
        customDayHighlight: {
            position: "absolute",
            top: 0,
            bottom: 0,
            left: "2px",
            right: "2px",
            border: `1px solid ${theme.palette.secondary.main}`,
            borderRadius: "50%",
        },
        disabled: {
            color: theme.palette.text.disabled,
        },
        highlightDisabled: {
            color: "#bbb",
        },
        highlight: {
            background: theme.palette.primary.light,
            color: theme.palette.text.primary,
        },
        selectedHighlight: {
            background: theme.palette.primary.main,
            color: theme.palette.common.white,
            borderRadius: "50%",
            "&:hover": {
                background: theme.palette.primary.main,
                color: theme.palette.common.white,
            }
        },
        firstHighlight: {
            background: theme.palette.primary.light,
            color: theme.palette.text.primary,
            borderTopLeftRadius: "50%",
            borderBottomLeftRadius: "50%",
        },
        endHighlight: {
            background: theme.palette.primary.light,
            color: theme.palette.text.primary,
            borderTopRightRadius: "50%",
            borderBottomRightRadius: "50%",
        },
        today: {
            "&::after": {
                content: "''",
                position: "absolute",
                width: 5,
                height: 5,
                top: "100%",
                left: "50%",
                background: theme.palette.primary.main,
                borderRadius: "50%",
                transform: "translate(-50%, -150%)",
            }
        }
    })
);

const BasicInfo: React.FC<StepperProp> = ({ stepData, handleStepData }) => {
    const classes = styles();
    const commonClasses = commonStyles();

    const handleChange = (e: any) => {
        handleStepData({
            type: "edit-form-data",
            payload: {
                name: e.target.name,
                value: e.target.value
            }
        })
    }

    const handleDateChange = (name: string, date: Date | null) => {
        if (date instanceof Date && !isNaN(date.getTime())) {
            handleStepData({
                type: "edit-form-data",
                payload: {
                    name: name,
                    value: date
                }
            })

        } else {
            handleStepData({
                type: "edit-form-data",
                payload: {
                    name: name,
                    value: undefined
                }
            })
        }
    };

    let regStart = stepData.draft.regStartDateTime && set(new Date(stepData.draft.regStartDateTime), { hours: 0, minutes: 0, seconds: 0, milliseconds: 0 });
    let regEnd = stepData.draft.regEndDateTime && set(new Date(stepData.draft.regEndDateTime), { hours: 0, minutes: 0, seconds: 0, milliseconds: 0 });
    let classStart = stepData.draft.startDateTime && set(new Date(stepData.draft.startDateTime), { hours: 0, minutes: 0, seconds: 0, milliseconds: 0 });
    let classEnd = stepData.draft.endDateTime && set(new Date(stepData.draft.endDateTime), { hours: 0, minutes: 0, seconds: 0, milliseconds: 0 });

    const highlightDay = (date, selectedDate, dayInCurrentMonth: boolean, context: "START_DATE" | "END_DATE") => {
        let dayIsBetween, isFirstDay, isLastDay;

        if (regStart && regEnd) {
            if (context === "START_DATE") {
                // user is selecting classStart date
                if (classEnd && isBefore(selectedDate, classEnd)) {
                    //  2 intervals:
                    //      from regStart to regEnd
                    //      from selectedDate to classEnd
                    if (isAfter(selectedDate, regEnd) && isBefore(selectedDate, classEnd)) {
                        // non overlapping intervals
                        dayIsBetween = isWithinInterval(date, { start: regStart, end: regEnd }) || isWithinInterval(date, { start: selectedDate, end: classEnd });
                        isFirstDay = isSameDay(date, regStart) || isSameDay(date, selectedDate);
                        isLastDay = isSameDay(date, regEnd) || isSameDay(date, classEnd);
                    } else {
                        // overlapping intervals
                        dayIsBetween = isWithinInterval(date, { start: regStart, end: classEnd })
                        isFirstDay = isSameDay(date, regStart);
                        isLastDay = isSameDay(date, classEnd);
                    }
                } else {
                    // 1 interval:
                    //      from regStart to regEnd
                    dayIsBetween = isWithinInterval(date, { start: regStart, end: regEnd });
                    isFirstDay = isSameDay(date, regStart);
                    isLastDay = isSameDay(date, regEnd);
                }
            } else {
                // user is selecting classEnd date
                if (classStart && isBefore(classStart, selectedDate)) {
                    // 2 intervals:
                    //      from regStart to regEnd
                    //      from classStart to selectedDate
                    if (isAfter(classStart, regEnd) && isBefore(classStart, selectedDate)) {
                        // non overlapping intervals
                        dayIsBetween = isWithinInterval(date, { start: regStart, end: regEnd }) || isWithinInterval(date, { start: classStart, end: selectedDate });
                        isFirstDay = isSameDay(date, regStart) || isSameDay(date, classStart);
                        isLastDay = isSameDay(date, regEnd) || isSameDay(date, selectedDate);
                    } else {
                        // overlapping intervals
                        dayIsBetween = isWithinInterval(date, { start: regStart, end: max([selectedDate, regEnd]) })
                        isFirstDay = isSameDay(date, regStart);
                        isLastDay = isSameDay(date, max([selectedDate, regEnd]));
                    }
                } else if (classStart && isSameDay(classStart, selectedDate)) {
                    // 1 interval:
                    //      from regStart to regEnd
                    dayIsBetween = isWithinInterval(date, { start: regStart, end: regEnd });
                    isFirstDay = isSameDay(date, regStart);
                    isLastDay = isSameDay(date, regEnd);
                }
            }
        } else {
            // reg interval not present
            if (context === "START_DATE" && classEnd && isBefore(selectedDate, classEnd)) {
                // 1 interval:
                //      from selectedDate to classEnd
                dayIsBetween = isWithinInterval(date, { start: selectedDate, end: classEnd });
                isFirstDay = isSameDay(date, selectedDate);
                isLastDay = isSameDay(date, classEnd);
            } else if (context === "END_DATE" && classStart && isBefore(classStart, selectedDate)) {
                // 1 interval:
                //      from classStart to selectedDate
                dayIsBetween = isWithinInterval(date, { start: classStart, end: selectedDate });
                isFirstDay = isSameDay(date, classStart);
                isLastDay = isSameDay(date, selectedDate);
            }
        }

        const pastDay = isPast(addDays(date, 1));   // add one day so as to enable today's date

        const isSelectedDay = isSameDay(date, selectedDate);
        const isToday = isSameDay(date, new Date());

        const wrapperClassName = clsx({
            [classes.highlight]: dayIsBetween,
            [classes.firstHighlight]: isFirstDay,
            [classes.endHighlight]: isLastDay
        });

        const dayClassName = clsx(classes.day, {
            [classes.disabled]: !dayInCurrentMonth || pastDay || (context === "START_DATE" && shouldDisableStartDate(date)) || (context === "END_DATE" && shouldDisableEndDate(date)),
            [classes.highlightDisabled]: !dayInCurrentMonth && dayIsBetween,
            [classes.selectedHighlight]: isSelectedDay,
            [classes.today]: isToday
        });

        return (
            <div className={wrapperClassName}>
                <IconButton className={dayClassName}>
                    <span> {format(date, "d")} </span>
                </IconButton>
            </div>
        );
    }

    const shouldDisableStartDate = date => {
        let disabled = false;
        if (date) {
            if (regStart)
                disabled = isBefore(addDays(date, 1), regStart);
            if (classEnd)
                disabled = disabled || isAfter(date, classEnd);
        }
        return disabled;
    }

    const shouldDisableEndDate = date => {
        let disabled = false;
        if (date) {
            if (classStart)
                disabled = isBefore(addDays(date, 1), classStart);
            else if (regStart)
                disabled = disabled || isBefore(addDays(date, 1), regStart);
        }
        return disabled;
    }

    return (
        <FormGroup className={commonClasses.form}>

            <Heading mb={3}>Step 1/4: Basic Info</Heading>

            <Grid container spacing={7}>
                <Grid className={commonClasses.formlabel} item sm={3}>
                    <Typography>Organizer</Typography>
                </Grid>
                <Grid className={commonClasses.formfield} item xs={12} sm={9}>
                    <TextField
                        name="organizerName"
                        value={stepData.draft.organizerName}
                        onChange={(e: any) => {
                            if (!(e.target.value.length > 50))
                                handleChange(e);
                        }}
                        variant="outlined"
                        error={stepData.errors.organizerName != undefined}
                        helperText={stepData.errors.organizerName || (stepData.draft.organizerName).length + " / 50 characters"}
                    />
                </Grid>

                <Grid className={commonClasses.formlabel} item sm={3}>
                    <Typography>Title</Typography>
                </Grid>
                <Grid className={commonClasses.formfield} item xs={12} sm={9}>
                    <TextField
                        name="title"
                        value={stepData.draft.title}
                        onChange={(e: any) => {
                            if (!(e.target.value.length > 50))
                                handleChange(e);
                        }}
                        variant="outlined"
                        error={stepData.errors.title != undefined}
                        helperText={stepData.errors!.title || (stepData.draft.title).length + " / 50 characters"}
                    />
                </Grid>

                <Grid className={commonClasses.formlabel} item sm={3}>
                    <Typography>Description</Typography>
                </Grid>
                <Grid className={commonClasses.formfield} item xs={12} sm={9}>
                    <TextField
                        name="description"
                        helperText={(stepData.draft.description || '').length + " / 50 characters"}
                        value={stepData.draft.description || ''}
                        onChange={(e: any) => {
                            if (!(e.target.value.length > 50))
                                handleChange(e);
                        }}
                        variant="outlined"
                    />
                </Grid>

                <Grid className={commonClasses.formlabel} item sm={3}>
                    <Typography>Venue</Typography>
                </Grid>
                <Grid className={commonClasses.formfield} item xs={12} sm={9}>
                    <TextField
                        name="venue"
                        multiline
                        value={stepData.draft.venue || ''}
                        onChange={(e: any) => {
                            handleChange(e);
                        }}
                        variant="outlined"
                    />
                </Grid>

                <Grid className={commonClasses.formlabel} item sm={3}>
                    <Typography>Starts on</Typography>
                </Grid>
                <Grid className={commonClasses.formfield} item xs={12} sm={9}>
                    <Box display="flex">
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <KeyboardDatePicker
                                name="startDateTime"
                                value={stepData.draft.startDateTime || stepData.draft.regStartDateTime || null}
                                onChange={date => handleDateChange("startDateTime", date)}
                                className={clsx(classes.wsPicker, classes.wsDatePicker)}
                                disableToolbar
                                maxDate={stepData.draft.endDateTime}
                                minDate={stepData.draft.regStartDateTime || new Date()}
                                format="dd/MM/yyyy"
                                KeyboardButtonProps={{
                                    'aria-label': 'change start date',
                                }}
                                renderDay={(date, selectedDate, dayInCurrentMonth) => highlightDay(date, selectedDate, dayInCurrentMonth, "START_DATE")}
                                shouldDisableDate={shouldDisableStartDate}
                            />
                            <TimePicker
                                name="startDateTime"
                                value={stepData.draft.startDateTime || null}
                                onChange={date => handleDateChange("startDateTime", date)}
                                className={classes.wsPicker}
                                // KeyboardButtonProps={{
                                //     'aria-label': 'change start time',
                                // }}
                                minutesStep={15}
                            />
                        </MuiPickersUtilsProvider>
                    </Box>
                </Grid>

                <Grid className={commonClasses.formlabel} item sm={3}>
                    <Typography>Ends on</Typography>
                </Grid>
                <Grid className={commonClasses.formfield} item xs={12} sm={9}>
                    <Box display="flex">
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <KeyboardDatePicker
                                name="endDateTime"
                                value={stepData.draft.endDateTime || stepData.draft.regStartDateTime || null}
                                onChange={date => handleDateChange("endDateTime", date)}
                                className={clsx(classes.wsPicker, classes.wsDatePicker)}
                                disableToolbar
                                minDate={stepData.draft.startDateTime || stepData.draft.regStartDateTime || new Date()}
                                variant="dialog"
                                format="dd/MM/yyyy"
                                KeyboardButtonProps={{
                                    'aria-label': 'change end date',
                                }}
                                renderDay={(date, selectedDate, dayInCurrentMonth) => highlightDay(date, selectedDate, dayInCurrentMonth, "END_DATE")}
                                shouldDisableDate={shouldDisableEndDate}
                            />
                            <TimePicker
                                name="endDateTime"
                                value={stepData.draft.endDateTime || null}
                                onChange={date => handleDateChange("endDateTime", date)}
                                className={classes.wsPicker}
                                // KeyboardButtonProps={{
                                //     'aria-label': 'change end time',
                                // }}
                                minutesStep={15}
                            />
                        </MuiPickersUtilsProvider>
                    </Box>
                </Grid>

            </Grid>

        </FormGroup>
    );
}

export default BasicInfo;
