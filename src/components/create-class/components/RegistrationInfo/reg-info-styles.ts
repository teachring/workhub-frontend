
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";

const styles = makeStyles((theme: Theme) =>
    createStyles({
        wsPicker: {
            width: "auto",
            "&::nth-child(1)": {
                background: "red"
            }
        },
        wsDatePicker: {
            marginRight: theme.spacing(2)
        },
        customForm: {
            width: "100%"
        },
        dynOption: {
            width: "100%",
            "& .Mui-disabled": {
                cursor: "pointer"
            }
        },
        [theme.breakpoints.down("xs")]: {
            formlabel: {
                paddingBottom: "0 !important",
                paddingTop: "30px !important",
                "&>p": {
                    lineHeight: "1 !important"
                }
            },
            form: {
                padding: 30
            }
        },

        // date picker styles
        dayWrapper: {
            position: "relative",
        },
        day: {
            width: 36,
            height: 36,
            fontSize: theme.typography.caption.fontSize,
            color: "inherit",
        },
        customDayHighlight: {
            position: "absolute",
            top: 0,
            bottom: 0,
            left: "2px",
            right: "2px",
            border: `1px solid ${theme.palette.secondary.main}`,
            borderRadius: "50%",
        },
        disabled: {
            color: theme.palette.text.disabled,
        },
        highlightDisabled: {
            color: "#bbb",
        },
        highlight: {
            background: theme.palette.primary.light,
            color: theme.palette.text.primary,
        },
        selectedHighlight: {
            background: theme.palette.primary.main,
            color: theme.palette.common.white,
            borderRadius: "50%",
            "&:hover": {
                background: theme.palette.primary.main,
                color: theme.palette.common.white,
            }
        },
        firstHighlight: {
            background: theme.palette.primary.light,
            color: theme.palette.text.primary,
            borderTopLeftRadius: "50%",
            borderBottomLeftRadius: "50%",
        },
        endHighlight: {
            background: theme.palette.primary.light,
            color: theme.palette.text.primary,
            borderTopRightRadius: "50%",
            borderBottomRightRadius: "50%",
        },
        today: {
            "&::after": {
                content: "''",
                position: "absolute",
                width: 5,
                height: 5,
                top: "100%",
                left: "50%",
                background: theme.palette.primary.main,
                borderRadius: "50%",
                transform: "translate(-50%, -150%)",
            }
        }
    })
);

export default styles;