import React from "react";
import clsx from "clsx";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import { AttachMoney, Event, Room, AccountCircle } from "@material-ui/icons";
import CloseIcon from '@material-ui/icons/Close';
import { Typography, TextField, FormGroup, FormControlLabel, StepIcon, InputAdornment, ListItemIcon, ButtonBase, Button, InputBase } from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import ListItemText from '@material-ui/core/ListItemText';
import Select from '@material-ui/core/Select';
import Checkbox from '@material-ui/core/Checkbox';
import Chip from '@material-ui/core/Chip';
import OutlinedInput from '@material-ui/core/OutlinedInput';

import AccessAlarmIcon from '@material-ui/icons/AccessAlarm';
import ShortTextIcon from '@material-ui/icons/ShortText';
import SubjectIcon from '@material-ui/icons/Subject';
import RadioButtonCheckedIcon from '@material-ui/icons/RadioButtonChecked';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import ArrowDropDownCircleIcon from '@material-ui/icons/ArrowDropDownCircle';
import RadioButtonUncheckedIcon from '@material-ui/icons/RadioButtonUnchecked';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import DeleteIcon from '@material-ui/icons/Delete';
import Switch from '@material-ui/core/Switch';
import Divider from '@material-ui/core/Divider';

import useStyles from './reg-form-ques-styles';
import { RegFormQuestion, RegFormQuesType, RegFormQuesOption } from "../../../../../utils/core-types/learner-reg";

type IProp = {
    question: RegFormQuestion,
    handleQuestion: React.Dispatch<any>
}

const Question: React.FC<IProp> = ({ question, handleQuestion }) => {
    const classes = useStyles();

    return (
        <Grid container spacing={3} className={classes.root}>
            <Grid item xs={12} sm={6}>
                <FormControl className={classes.customForm} variant="outlined">
                    <TextField
                        label="Question"
                        variant="outlined"
                        value={question.question}
                        onChange={e =>
                            handleQuestion({
                                type: "edit-question",
                                payload: {
                                    qid: question.qid,
                                    newValue: e.target.value
                                }
                            })
                        } />
                </FormControl>
            </Grid>

            <Grid item xs={12} sm={6}>
                <FormControl className={classes.customForm} variant="outlined">
                    <InputLabel id={"q-type-" + question.qid}>Type</InputLabel>
                    <Select
                        className={classes.qTypeSelect}
                        labelId={"q-type-" + question.qid}
                        input={<OutlinedInput labelWidth={70} />}
                        value={question.type}
                        onChange={e =>
                            handleQuestion({
                                type: "select-type",
                                payload: {
                                    qid: question.qid,
                                    newType: e.target.value
                                }
                            })}
                    >
                        <MenuItem value={RegFormQuesType.TEXT} selected={question.type === RegFormQuesType.TEXT}>
                            <ListItemIcon><ShortTextIcon /></ListItemIcon>
                            <ListItemText primary="Short answer" />
                        </MenuItem>

                        <MenuItem value={RegFormQuesType.RADIO} selected={question.type === RegFormQuesType.RADIO}>
                            <ListItemIcon><RadioButtonCheckedIcon /></ListItemIcon>
                            <ListItemText primary="Single choice" />
                        </MenuItem>

                        <MenuItem value={RegFormQuesType.CHECKBOX} selected={question.type === RegFormQuesType.CHECKBOX}>
                            <ListItemIcon><CheckBoxIcon /></ListItemIcon>
                            <ListItemText primary="Multiple choice" />
                        </MenuItem>
                    </Select>
                </FormControl>
            </Grid>

            {
                (question.type === RegFormQuesType.RADIO || question.type === RegFormQuesType.CHECKBOX) && (
                    <Grid item xs={12}>
                        {
                            question!.options!.map((option: RegFormQuesOption, index: number) => (
                                <Box mb={2} key={index}>
                                    <Grid container spacing={2} alignItems="flex-end" className={classes.dynOption}>
                                        <Grid item>
                                            {question.type === RegFormQuesType.RADIO && <RadioButtonUncheckedIcon />}
                                            {question.type === RegFormQuesType.CHECKBOX && <CheckBoxOutlineBlankIcon />}
                                        </Grid>
                                        <Grid item xs>
                                            <TextField
                                                className={classes.dynOption}
                                                value={option.text}
                                                onChange={e =>
                                                    handleQuestion({
                                                        type: "edit-option",
                                                        payload: {
                                                            qid: question.qid,
                                                            optionIndex: index,
                                                            newValue: e.target.value
                                                        }
                                                    })
                                                }
                                            />
                                        </Grid>
                                        <Grid item className={classes.optionDelete}>
                                            <IconButton
                                                onClick={() => handleQuestion({
                                                    type: "delete-option",
                                                    payload: {
                                                        qid: question.qid,
                                                        optionIndex: index
                                                    }
                                                })}
                                            >
                                                <CloseIcon />
                                            </IconButton>
                                        </Grid>
                                    </Grid>
                                </Box>
                            ))
                        }
                        <Box mb={2}>
                            <Grid container spacing={2} alignItems="flex-end" className={classes.dynOption}>
                                <Grid item>
                                    {question.type === RegFormQuesType.RADIO && <RadioButtonUncheckedIcon />}
                                    {question.type === RegFormQuesType.CHECKBOX && <CheckBoxOutlineBlankIcon />}
                                </Grid>
                                <Grid item xs>
                                    <Box
                                        className={classes.disabledTextField}
                                        onClick={() => {
                                            handleQuestion({
                                                type: "add-option",
                                                payload: { qid: question.qid }
                                            })
                                        }}
                                        tabIndex={0}
                                        onKeyPress={e => {
                                            if (e.key === "Enter" || e.key === " ") {
                                                handleQuestion({
                                                    type: "add-option",
                                                    payload: { qid: question.qid }
                                                })
                                            }
                                        }}
                                    >
                                        Add option
                                    </Box>
                                </Grid>
                            </Grid>
                        </Box>
                    </Grid>
                )
            }

            <Grid item xs={12}>
                <Grid container spacing={3} justify="flex-end" alignItems="center">
                    <IconButton aria-label="delete"
                        onClick={e => {
                            handleQuestion({
                                type: "delete-question",
                                payload: {
                                    qid: question.qid
                                }
                            })
                        }}>
                        <DeleteIcon />
                    </IconButton>
                    <Divider orientation="vertical" className={classes.divider} />
                    <FormControlLabel
                        control={
                            <Switch
                                checked={question.required}
                                color="primary"
                                onChange={e => handleQuestion({
                                    type: "toggle-required",
                                    payload: {
                                        qid: question.qid
                                    }
                                })}
                            />
                        }
                        label="Required"
                    />
                </Grid>
            </Grid>

        </Grid>
    );
}

export default Question;
