import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";

const styles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            border: "1px solid #ddd",
            padding: "30px",
            paddingBottom: "30px !important",
            marginBottom: "30px",
            margin: 28
        },
        customForm: {
            width: "100%"
        },
        qTypeSelect: {
            "& .MuiSelect-selectMenu": {
                display: "flex",
                paddingTop: 12,
                paddingBottom: 12,
                color: "#717a86"
            },
            "& .MuiListItemIcon-root": {
                alignItems: "center"
            }
        },
        dynOption: {
            "& .MuiTextField-root": {
                width: "100%"
            },
            "& .MuiSvgIcon-root": {
                color: "#717a86"
            },
            "& .Mui-disabled": {
                cursor: "pointer"
            }
        },
        optionDelete: {
            padding: "0 !important"
        },
        divider: {
            height: "35px",
            margin: "0 15px 0 5px"
        },
        disabledTextField: {
            position: "relative",
            color: theme.palette.text.disabled,
            height: "1.1875em",
            padding: "6px 0 7px",
            textAlign: "left",
            cursor: "pointer",
            "&::before": {
                left: 0,
                right: 0,
                bottom: 0,
                content: "''",
                position: "absolute",
                transition: "border-bottom-color 200ms cubic-bezier(0.4, 0, 0.2, 1) 0ms",
                borderBottom: "1px solid rgba(0, 0, 0, 0.42)",
                borderBottomStyle: "dotted",
                pointerEvents: "none",
            }
        }
    })
);

export default styles;