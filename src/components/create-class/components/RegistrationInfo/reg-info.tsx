import React from "react";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import { AttachMoney, Event, Room, AccountCircle } from "@material-ui/icons";
import { Typography, TextField, FormGroup, FormControlLabel, StepIcon, InputAdornment, IconButton } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import clsx from "clsx";
import DateFnsUtils from '@date-io/date-fns';
import { KeyboardDatePicker, MuiPickersUtilsProvider, TimePicker } from '@material-ui/pickers';


import Question from "./components/reg-form-ques"
import commonStyles from "../../common-style";
import { NewDraftObj } from "../../../../utils/core-types/educator-types";
import useStyles from './reg-info-styles';
import { StepperProp } from '../../create-class';
import Heading from "../../../shared/heading/heading";
import { isBefore, isAfter, isWithinInterval, isSameDay, max, isPast, addDays, format, set } from "date-fns";


const RegistrationInfo: React.FC<StepperProp> = ({ stepData, handleStepData }) => {
    const classes = useStyles();
    const commonClasses = commonStyles();

    const handleChange = (name: keyof NewDraftObj, value: any) => {
        handleStepData({
            type: "edit-form-data",
            payload: { name, value }
        })
    }

    const handleDateChange = (name: string, date: Date | null) => {
        if (date instanceof Date && !isNaN(date.getTime())) {
            handleStepData({
                type: "edit-form-data",
                payload: {
                    name: name,
                    value: date
                }
            })

        } else {
            handleStepData({
                type: "edit-form-data",
                payload: {
                    name: name,
                    value: undefined
                }
            })
        }
    };

    let regStart = stepData.draft.regStartDateTime && set(new Date(stepData.draft.regStartDateTime), { hours: 0, minutes: 0, seconds: 0, milliseconds: 0 });
    let regEnd = stepData.draft.regEndDateTime && set(new Date(stepData.draft.regEndDateTime), { hours: 0, minutes: 0, seconds: 0, milliseconds: 0 });
    let classStart = stepData.draft.startDateTime && set(new Date(stepData.draft.startDateTime), { hours: 0, minutes: 0, seconds: 0, milliseconds: 0 });
    let classEnd = stepData.draft.endDateTime && set(new Date(stepData.draft.endDateTime), { hours: 0, minutes: 0, seconds: 0, milliseconds: 0 });

    const highlightDay = (date, selectedDate, dayInCurrentMonth: boolean, context: "START_DATE" | "END_DATE") => {

        let dayIsBetween, isFirstDay, isLastDay;

        if (classStart && classEnd) {
            if (context === "START_DATE") {
                // user is selecting regStart date
                if (regEnd && isBefore(selectedDate, regEnd)) {
                    //  2 intervals:
                    //      from selectedDate to regEnd
                    //      from classStart to classEnd
                    if (isAfter(classStart, regEnd) && isBefore(selectedDate, regEnd)) {
                        // non overlapping intervals
                        dayIsBetween = isWithinInterval(date, { start: selectedDate, end: regEnd }) || isWithinInterval(date, { start: classStart, end: classEnd });
                        isFirstDay = isSameDay(date, selectedDate) || isSameDay(date, classStart);
                        isLastDay = isSameDay(date, regEnd) || isSameDay(date, classEnd);
                    } else {
                        // overlapping intervals
                        dayIsBetween = isWithinInterval(date, { start: selectedDate, end: classEnd })
                        isFirstDay = isSameDay(date, selectedDate);
                        isLastDay = isSameDay(date, classEnd);
                    }
                } else {
                    // 1 interval:
                    //      from classStart to classEnd
                    dayIsBetween = isWithinInterval(date, { start: classStart, end: classEnd });
                    isFirstDay = isSameDay(date, classStart);
                    isLastDay = isSameDay(date, classEnd);
                }
            } else {
                // user is selecting regEnd date
                if (regStart && isBefore(regStart, selectedDate)) {
                    // 2 intervals:
                    //      from regStart to selectedDate
                    //      from classStart to classEnd
                    if (isAfter(classStart, selectedDate) && isBefore(regStart, selectedDate)) {
                        // non overlapping intervals
                        dayIsBetween = isWithinInterval(date, { start: regStart, end: selectedDate }) || isWithinInterval(date, { start: classStart, end: classEnd });
                        isFirstDay = isSameDay(date, regStart) || isSameDay(date, classStart);
                        isLastDay = isSameDay(date, selectedDate) || isSameDay(date, classEnd);

                    } else {
                        // overlapping intervals
                        dayIsBetween = isWithinInterval(date, { start: regStart, end: max([selectedDate, classEnd]) })
                        isFirstDay = isSameDay(date, regStart);
                        isLastDay = isSameDay(date, max([selectedDate, classEnd]));
                    }
                } else if (regStart && isSameDay(regStart, selectedDate)) {
                    // 1 interval:
                    //      from classStart to classEnd
                    dayIsBetween = isWithinInterval(date, { start: classStart, end: classEnd });
                    isFirstDay = isSameDay(date, classStart);
                    isLastDay = isSameDay(date, classEnd);
                }
            }
        } else {
            // class interval not present
            if (context === "START_DATE" && regEnd && isBefore(selectedDate, regEnd)) {
                // 1 interval:
                //      from selectedDate to regEnd
                dayIsBetween = isWithinInterval(date, { start: selectedDate, end: regEnd });
                isFirstDay = isSameDay(date, selectedDate);
                isLastDay = isSameDay(date, regEnd);
            } else if (context === "END_DATE" && regStart && isBefore(regStart, selectedDate)) {
                // 1 interval:
                //      from regStart to selectedDate
                dayIsBetween = isWithinInterval(date, { start: regStart, end: selectedDate });
                isFirstDay = isSameDay(date, regStart);
                isLastDay = isSameDay(date, selectedDate);
            }
        }

        const pastDay = isPast(addDays(date, 1));   // add one day so as to enable today's date

        const isSelectedDay = isSameDay(date, selectedDate);
        const isToday = isSameDay(date, new Date());

        const wrapperClassName = clsx({
            [classes.highlight]: dayIsBetween,
            [classes.firstHighlight]: isFirstDay,
            [classes.endHighlight]: isLastDay
        });

        const dayClassName = clsx(classes.day, {
            [classes.disabled]: !dayInCurrentMonth || pastDay || (context === "START_DATE" && shouldDisableStartDate(date)) || (context === "END_DATE" && shouldDisableEndDate(date)),
            [classes.highlightDisabled]: !dayInCurrentMonth && dayIsBetween,
            [classes.selectedHighlight]: isSelectedDay,
            [classes.today]: isToday
        });

        return (
            <div className={wrapperClassName}>
                <IconButton className={dayClassName}>
                    <span> {format(date, "d")} </span>
                </IconButton>
            </div>
        );
    }

    const shouldDisableStartDate = date => {
        let disabled = false;
        if (date) {
            if (regEnd)
                disabled = isAfter(date, regEnd);
            if (classEnd)
                disabled = disabled || isAfter(date, classEnd);
        }
        return disabled;
    }

    const shouldDisableEndDate = date => {
        let disabled = false;
        if (date) {
            if (regStart)
                disabled = isBefore(date, regStart);
            if (classEnd)
                disabled = disabled || isAfter(date, classEnd);
        }
        return disabled;
    }

    return (
        <FormGroup className={commonClasses.form}>

            <Heading mb={3}>Step 4/4: Registration Info</Heading>

            <Grid container spacing={7}>

                <Grid className={commonClasses.formlabel} item sm={4}>
                    <Typography>Registration starts on</Typography>
                </Grid>
                <Grid className={commonClasses.formfield} item xs={12} sm={8}>
                    <Box display="flex">
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <KeyboardDatePicker
                                className={clsx(classes.wsPicker, classes.wsDatePicker)}
                                disableToolbar
                                variant="dialog"
                                minDate={new Date()}
                                maxDate={stepData.draft.regEndDateTime || stepData.draft.endDateTime}
                                format="dd/MM/yyyy"
                                value={stepData.draft.regStartDateTime || null}
                                onChange={date => handleDateChange("regStartDateTime", date)}
                                KeyboardButtonProps={{
                                    'aria-label': 'change start date',
                                }}
                                renderDay={(date, selectedDate, dayInCurrentMonth) => highlightDay(date, selectedDate, dayInCurrentMonth, "START_DATE")}
                                shouldDisableDate={shouldDisableStartDate}
                            />
                            <TimePicker
                                className={classes.wsPicker}
                                value={stepData.draft.regStartDateTime || null}
                                onChange={time => handleDateChange("regStartDateTime", time)}
                                // KeyboardButtonProps={{
                                //     'aria-label': 'change start time',
                                // }}
                                minutesStep={15}
                            />
                        </MuiPickersUtilsProvider>
                    </Box>
                </Grid>

                <Grid className={commonClasses.formlabel} item sm={4}>
                    <Typography>Registration ends on</Typography>
                </Grid>
                <Grid className={commonClasses.formfield} item xs={12} sm={8}>
                    <Box display="flex">
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <KeyboardDatePicker
                                className={clsx(classes.wsPicker, classes.wsDatePicker)}
                                disableToolbar
                                variant="dialog"
                                minDate={stepData.draft.regStartDateTime || new Date()}
                                maxDate={stepData.draft.endDateTime}
                                format="dd/MM/yyyy"
                                value={stepData.draft.regEndDateTime || null}
                                onChange={date => handleDateChange("regEndDateTime", date)}
                                KeyboardButtonProps={{
                                    'aria-label': 'change end date',
                                }}
                                renderDay={(date, selectedDate, dayInCurrentMonth) => highlightDay(date, selectedDate, dayInCurrentMonth, "END_DATE")}
                                shouldDisableDate={shouldDisableEndDate}
                            />
                            <TimePicker
                                className={classes.wsPicker}
                                value={stepData.draft.regEndDateTime || null}
                                onChange={time => handleDateChange("regEndDateTime", time)}
                                // KeyboardButtonProps={{
                                //     'aria-label': 'change end time',
                                // }}
                                minutesStep={15}
                            />
                        </MuiPickersUtilsProvider>
                    </Box>
                </Grid>

                <Grid className={commonClasses.formlabel} item sm={12} data-formlabel="vertical">
                    <Typography>Custom form</Typography>
                </Grid>

                {
                    stepData.draft.regFormData && (
                        Object.keys(stepData.draft.regFormData)
                            .map(qid => (
                                <Question key={qid} question={stepData.draft.regFormData?.[qid]} handleQuestion={handleStepData} />
                            ))
                    )
                }

                <Grid item xs={12}>
                    <Button
                        className={commonClasses.addNewFieldBtn}
                        variant="outlined"
                        color="primary"
                        onClick={() => handleStepData({ type: "add-field" })}
                    >
                        Add new field
                    </Button>
                </Grid>

            </Grid>

        </FormGroup >
    );
}

export default RegistrationInfo;
