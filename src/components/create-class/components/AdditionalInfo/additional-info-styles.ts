
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
const styles = makeStyles((theme: Theme) =>
    createStyles({
        question: {
            marginBottom: theme.spacing(1),
            paddingBottom: 0
        },
        iconButton: {
            marginLeft: "auto"
        },
        wsPicker: {
            width: "auto",
            "&::nth-child(1)": {
                background: "red"
            }
        },
        wsDatePicker: {
            marginRight: theme.spacing(2)
        },
        richEditor: {
            color: "#717a86"
        }
    })
);

export default styles;