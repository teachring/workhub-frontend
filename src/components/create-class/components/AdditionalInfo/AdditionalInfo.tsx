import React from "react";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import { AttachMoney, Event, Room } from "@material-ui/icons";
import { Typography, TextField, FormGroup, FormControlLabel } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from '@material-ui/icons/Delete';
import DirectionsIcon from "@material-ui/icons/Directions";
import Input from '@material-ui/core/Input';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import CKEditor from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";

import commonStyles from "../../common-style";
import { NewDraftObj } from "../../../../utils/core-types/educator-types";
import { FAQ } from "../../../../utils/core-types/class-types";
import useStyles from './additional-info-styles';
import { StepperProp } from '../../create-class';
import Heading from "../../../shared/heading/heading";

const AdditionalInfo: React.FC<StepperProp> = ({ stepData, handleStepData }) => {

    const classes = useStyles();
    const commonClasses = commonStyles();

    const handleChange = (name: keyof NewDraftObj, value: string) => {
        handleStepData({
            type: "edit-form-data",
            payload: { name, value }
        })
    }

    const handleAgenda = (event: any, editor: any) => handleChange("agenda", editor.getData());
    const handlePrerequirement = (event: any, editor: any) => handleChange("prerequirement", editor.getData());
    const handleOther = (event: any, editor: any) => handleChange("otherDetails", editor.getData());

    const handleFAQ = (type: keyof FAQ, index: number, val: string) => {
        let data = stepData.draft.faq!;
        data[index][type] = val;

        handleStepData({
            type: "edit-form-data",
            payload: {
                name: "faq",
                value: data
            }
        })
    }

    const deleteFAQ = (index: number) => {
        let data = stepData.draft.faq!;
        data.splice(index, 1);

        handleStepData({
            type: "edit-form-data",
            payload: {
                name: "faq",
                value: data
            }
        })
    }

    const addFAQ = () => {
        handleStepData({
            type: "edit-form-data",
            payload: {
                name: "faq",
                value: [...(stepData.draft.faq || []), { q: "", a: "" }]
            }
        })
    }

    return (
        <FormGroup className={commonClasses.form}>

            <Heading mb={3}>Step 2/4: Additional Info</Heading>

            <Grid container spacing={7}>

                <Grid className={commonClasses.formlabel} item xs={12} data-formlabel="vertical">
                    <Typography>What you will learn</Typography>
                </Grid>
                <Grid className={commonClasses.formfield} item xs={12}>
                    <CKEditor
                        config={{
                            removePlugins: ['Image', 'ImageCaption', 'ImageStyle', 'ImageToolbar', 'ImageUpload', 'MediaEmbed']
                        }}
                        data={stepData.draft.agenda}
                        onChange={handleAgenda}
                        editor={ClassicEditor}
                    />
                </Grid>

                <Grid className={commonClasses.formlabel} item xs={12} data-formlabel="vertical">
                    <Typography>Pre-requirements</Typography>
                </Grid>
                <Grid className={commonClasses.formfield} item xs={12}>
                    <CKEditor
                        config={{
                            removePlugins: ['Image', 'ImageCaption', 'ImageStyle', 'ImageToolbar', 'ImageUpload', 'MediaEmbed']
                        }}
                        data={stepData.draft.prerequirement}
                        onChange={handlePrerequirement}
                        editor={ClassicEditor}
                    />
                </Grid>

                <Grid className={commonClasses.formlabel} item xs={12} data-formlabel="vertical">
                    <Typography>Other details</Typography>
                </Grid>
                <Grid className={commonClasses.formfield} item xs={12}>
                    <CKEditor
                        config={{
                            removePlugins: ['Image', 'ImageCaption', 'ImageStyle', 'ImageToolbar', 'ImageUpload', 'MediaEmbed']
                        }}
                        data={stepData.draft.otherDetails}
                        onChange={handleOther}
                        editor={ClassicEditor}
                    />
                </Grid>

                <Grid className={commonClasses.formlabel} item xs={12} data-formlabel="vertical">
                    <Typography>FAQ</Typography>
                </Grid>

                {
                    stepData.draft.faq != undefined ? stepData.draft.faq.map((FAQ, i) => (
                        <Grid className={commonClasses.formfield} item xs={12} key={i}>
                            <FormControl className={classes.question} variant="outlined">
                                <InputLabel htmlFor={"faq-question-" + i}>Question</InputLabel>
                                <OutlinedInput
                                    id={"faq-question-" + i}
                                    type="text"
                                    labelWidth={70}
                                    endAdornment={
                                        <InputAdornment position="end">
                                            <IconButton
                                                aria-label="delete question"
                                                onClick={e => deleteFAQ(i)}
                                            >
                                                <DeleteIcon />
                                            </IconButton>
                                        </InputAdornment>
                                    }
                                    value={FAQ.q}
                                    onChange={e => handleFAQ("q", i, e.target.value)}
                                />
                            </FormControl>
                            <TextField
                                label="Answer"
                                multiline
                                rows="4"
                                variant="outlined"
                                value={FAQ.a}
                                onChange={e => handleFAQ("a", i, e.target.value)}
                            />
                        </Grid>
                    )) : null
                }

                <Grid item xs={12}>
                    <Button
                        className={commonClasses.addNewFieldBtn}
                        variant="outlined"
                        color="primary"
                        onClick={addFAQ}
                    > Add new field </Button>
                </Grid>

            </Grid>
        </FormGroup>
    );
}

export default AdditionalInfo;
