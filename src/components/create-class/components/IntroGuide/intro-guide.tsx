import React from "react";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import { Typography, TextField, FormGroup, Box, Grid, FormControlLabel, Switch, Paper } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import classes from "*.module.css";
import Heading from "../../../shared/heading/heading";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        paper: {
            margin: "3rem auto",
            padding: "3rem",
            maxWidth: 750,
            boxSizing: "border-box"
        },
        [theme.breakpoints.down("xs")]: {
            paper: {
                margin: 0
            }
        }
    })
);


type IProp = {
    handleIntroProceedClick: () => void
}

const IntroGuide: React.FC<IProp> = ({ handleIntroProceedClick }) => {

    const classes = useStyles();

    return (
        <Paper className={classes.paper} elevation={2}>
            <Heading center={true} underline={true}>Important Guidelines</Heading>

            <Box textAlign="left" mt={3}>

                <Typography>Lorem ipsum dolor sit amet consectetur adipisicing elit. Suscipit ipsam, cum rem qui nostrum cumque ex veritatis ipsa quasi fuga sequi. Harum doloremque itaque enim atque molestiae aut eos quis?</Typography>
                <ul>
                    <li><Typography>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Corporis, nostrum!</Typography></li>
                    <li><Typography>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Corporis, nostrum!</Typography></li>
                    <li><Typography>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Corporis, nostrum!</Typography></li>
                </ul>
            </Box>

            <Box mt={3}><Button variant="contained" color="primary" onClick={handleIntroProceedClick}> Proceed</Button></Box>
        </Paper>
    );
}

export default IntroGuide;
