import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";

const styles = makeStyles((theme: Theme) =>
    createStyles({
        wsPicker: {
            width: "auto",
            "&::nth-child(1)": {
                background: "red"
            }
        },
        wsDatePicker: {
            marginRight: theme.spacing(2)
        },
        paymentModeBtn: {
            border: "1px solid",
            borderRadius: 0,
            padding: 30,
            width: 220,
            maxWidth: "100%",
            borderColor: "inherit",
            "& .MuiButton-label": {
                display: "flex",
                flexWrap: "wrap"
            },
            "& .MuiButton-startIcon": {
                width: "100%",
                display: "block",
                margin: "auto"
            },
            "& .MuiSvgIcon-root": {
                fontSize: 40
            },
            "&[data-payment='online']": {
                marginRight: 20
            },
            "&[data-payment='cash']": {
            },
            "&[data-selected='true']": {
                borderColor: "transparent"
            }
        },
        labelPlacementStart: {
            marginRight: "1rem",
        },
        [theme.breakpoints.down("xs")]: {
            formlabel: {
                paddingBottom: "0 !important",
                paddingTop: "30px !important",
                "&>p": {
                    lineHeight: "1 !important"
                }
            },
            form: {
                padding: 30
            },
            paymentModeBtn: {
                margin: "20px 0 !important"
            }
        }
    })
);

export default styles;