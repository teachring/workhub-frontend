import React from "react";
import { Typography, TextField, FormGroup, Box, Grid, FormControlLabel, Switch } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import MoneyOffIcon from '@material-ui/icons/MoneyOff';
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import MoneyIcon from '@material-ui/icons/Money';
import AddToHomeScreenIcon from '@material-ui/icons/AddToHomeScreen';

import commonStyles from "../../common-style";
import useStyles from './payment-info-styles';
import { StepperProp } from '../../create-class';
import { NewDraftObj } from "../../../../utils/core-types/educator-types";
import CashPaymentIcon from "../../../../assets/icons/CashPayment";
import OnlinePaymentIcon from "../../../../assets/icons/OnlinePayment";
import RupeeLight from "../../../../assets/icons/RupeeLight";
import Heading from "../../../shared/heading/heading";

const PaymentInfo: React.FC<StepperProp> = ({ stepData, handleStepData }) => {

    const classes = useStyles();
    const commonClasses = commonStyles();

    const handleChange = (name: keyof NewDraftObj, value: any) => {

        handleStepData({
            type: "edit-form-data",
            payload: { name, value }
        })
        if (name == 'isPaid' && value == true && !stepData.draft.offlinePayAvl) {
            handleStepData({
                type: "edit-form-data",
                payload: { name: "offlinePayAvl", value: true }
            })
        }


    }

    return (
        <FormGroup className={commonClasses.form}>

            <Heading mb={3}>Step 3/4: Payment Info</Heading>

            <Grid container spacing={7}>
                <Grid className={commonClasses.formlabel} item sm={3}>
                    <Typography>Payment</Typography>
                </Grid>

                <Grid className={commonClasses.formfield} item xs={12} sm={9}>
                    <Box display="flex">
                        <FormControlLabel
                            value="end"
                            control={<Switch color="primary" />}
                            label={stepData.draft.isPaid ? "PAID" : "FREE"}
                            labelPlacement="start"
                            checked={stepData.draft.isPaid}
                            onClick={e => handleChange("isPaid", !stepData.draft.isPaid)}
                            classes={{
                                label: classes.labelPlacementStart
                            }}
                        />
                    </Box>
                </Grid>

                <Grid className={commonClasses.formlabel} item sm={12}>
                    <Typography>
                        {/* payment tnc goes here */}
                    </Typography>
                </Grid>
                {
                    stepData.draft.isPaid ? (
                        <React.Fragment>
                            <Grid className={commonClasses.formlabel} item sm={6}>
                                <Typography>Fee per attendee (Rs.)</Typography>
                            </Grid>
                            <Grid className={commonClasses.formfield} item xs={12} sm={6}>
                                <TextField
                                    variant="outlined"
                                    value={stepData.draft.feeAmt || ''}
                                    type="number"
                                    onChange={e => {
                                        if (e.target.value && parseInt(e.target.value) > 0)
                                            handleChange("feeAmt", parseInt(e.target.value))
                                        else
                                            handleChange("feeAmt", undefined);
                                    }}
                                />
                            </Grid>

                            <Grid className={commonClasses.formlabel} item xs={12} data-formlabel="vertical">
                                <Typography>Payment mode</Typography>
                            </Grid>

                            <Grid className={commonClasses.formlabel} item sm={12}>
                                <Button
                                    color="primary"
                                    data-payment="online"
                                    data-selected={stepData.draft.offlinePayAvl}
                                    className={classes.paymentModeBtn}
                                    startIcon={<OnlinePaymentIcon />}
                                    onClick={() => handleChange("offlinePayAvl", false)}
                                >
                                    Online only
                                </Button>
                                <Button
                                    color="primary"
                                    data-payment="cash"
                                    data-selected={!stepData.draft.offlinePayAvl}
                                    className={classes.paymentModeBtn}
                                    startIcon={<CashPaymentIcon />}
                                    onClick={() => handleChange("offlinePayAvl", true)}
                                >
                                    Online + Cash
                                </Button>
                            </Grid>
                        </React.Fragment>
                    ) : null
                }


            </Grid>

        </FormGroup>
    );
}

export default PaymentInfo;
