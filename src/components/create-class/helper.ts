import { NewDraftObj, SavedDraftInfo } from "../../utils/core-types/educator-types";
import { ErrorType } from "./create-class";
import { NewClassObj } from "../../utils/core-types/class-types";

export const mapSavedDraftToNewDraftObj = (draftInfo:SavedDraftInfo) : NewDraftObj => {
   var newDraftObj:NewDraftObj = {title: draftInfo.title,organizerName:draftInfo.organizerName,isPaid:draftInfo.isPaid}

   Object.keys(draftInfo).forEach(key => {
       if(!['draftId','createdBy','lastUpdateDateTime'].includes(key)){
           newDraftObj[key] = draftInfo[key]
       }
   })

   if(newDraftObj['startDateTime'] && newDraftObj['startDateTime']<new Date()) {
       newDraftObj['startDateTime'] = newDraftObj['endDateTime'] = newDraftObj['regEndDateTime'] = newDraftObj['regStartDateTime'] = undefined;
   }

   if(newDraftObj['regStartDateTime'] && newDraftObj['regStartDateTime']<new Date()) {
    newDraftObj['regEndDateTime'] = newDraftObj['regStartDateTime'] = undefined;
   }

   return newDraftObj;

}





export function validateBeforeSubmit(draft: NewDraftObj) : ErrorType {
    let error:ErrorType = {}
    if(!draft.title) 
        error.title = 'In Step 1, you must provide a title for the class.'
    if(!draft.organizerName) 
        error.organizerName = 'In step 1, you must provide the name of the organizing body';
    if(!draft.description) 
        error.description = 'In step 1, you must provide a short description about your class';
        
    if(!draft.startDateTime) 
        error.startDateTime = 'In step 1, Class start date is missing'
    if(!draft.endDateTime) 
        error.endDateTime = 'In step 1, Class end date is missing'
    if(!draft.venue) 
        error.venue = 'In step 1, you must provide a venue for the class'
    if(!draft.agenda)
        error.agenda = 'In step 2, you must provide agenda for your class';
    if(!draft.prerequirement) 
        error.prerequirement = 'In step 2, you must provide Prerequisites for the class';
    if(draft.isPaid && !draft.feeAmt) 
        error.feeAmt = 'In step 3, you must provide the Fee amount'
    if(!draft.regStartDateTime) 
        error.regStartDateTime = 'In step 4, Reg Start date is missing'
    if(!draft.regEndDateTime) 
        error.title = 'In step 4, Reg End date is missing';
    if(draft.startDateTime && draft.endDateTime ){
        if(draft.startDateTime<new Date() || draft.endDateTime<draft.startDateTime) {
            error.startDateTime = 'In step 1, Please enter valid Class start and end Date-Time'
        }
    }
    if(draft.regStartDateTime && draft.regEndDateTime ){
        if(draft.regStartDateTime<new Date() || draft.regEndDateTime<draft.regStartDateTime) {
            error.regStartDateTime = 'In step 4, Please enter valid Registration start and end Date-Time'
        }
    }

    return error;
}

export const mapNewDraftToNewClassObj = (draftObj:NewDraftObj) : NewClassObj => {
    const classObj:NewClassObj =  {
        organizerName: draftObj.organizerName,
        title: draftObj.title,
        description: draftObj.description!,
        startDateTime: draftObj.startDateTime!,
        endDateTime: draftObj.endDateTime!,
        venue: draftObj.venue!,
        isPaid: draftObj.isPaid,
        regStartDateTime: draftObj.regStartDateTime!,
        regEndDateTime: draftObj.regEndDateTime!,
        regFormData: draftObj.regFormData,
        agenda: draftObj.agenda!,
        prerequirement: draftObj.prerequirement!,
        otherDetails: draftObj.otherDetails,
        faq: draftObj.faq,
    }
    if(classObj.isPaid){
        classObj.paymentDetails = {feeAmt: draftObj.feeAmt!,offlinePayAvl:draftObj.offlinePayAvl!}
    }
    return classObj;
}

export function getDefaultStartDate() :Date{
    var now = new Date();
    now.setDate(now.getDate() + 1)
    now.setHours(8);
    now.setMinutes(0);
    now.setMilliseconds(0);
    return now
}

export function getDefaultEndDate() : Date {
    var now = new Date();
    now.setDate(now.getDate() + 4)
    now.setHours(17);
    now.setMinutes(0);
    now.setMilliseconds(0);
    return now
}