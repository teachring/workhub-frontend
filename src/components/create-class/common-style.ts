import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";

const commonStyles = makeStyles((theme: Theme) =>
    createStyles({
        form: {
            margin: "auto",
            maxWidth: 750,
            padding: "0 50px 130px 50px"
        },
        formlabel: {
            color: "#717a86",
            fontWeight: 400,
            "&[data-formlabel='vertical']": {
                paddingBottom: 0
            },
            "&>p": {
                lineHeight: 3,
                textAlign: "left",
            }
        },
        formfield: {
            "&>*": {
                width: "100%",
                margin: 0,
                paddingBottom: theme.spacing(8)
            },
            "&>*:last-child": {
                paddingBottom: 0
            },
            "& input": {
                color: "#717a86"
            },
            "& .MuiInputBase-root": {
                borderRadius: 0
            }
        },
        addNewFieldBtn: {
            padding: "1rem 2rem",
            borderRadius: 0
        },
        [theme.breakpoints.down("xs")]: {
            formlabel: {
                paddingBottom: "0 !important",
                paddingTop: "30px !important",
                "&>p": {
                    lineHeight: "1 !important"
                }
            },
            form: {
                padding: 30,
                paddingBottom: "9rem"
            }
        }
    })
);

export default commonStyles;
