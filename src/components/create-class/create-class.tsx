import React from "react";
import { Typography, Button, Box } from "@material-ui/core";

import StepNav from "./components/step-nav/step-nav";
import BasicInfo from "./components/BasicInfo/BasicInfo";
import AdditionalInfo from "./components/AdditionalInfo/AdditionalInfo";
import PaymentInfo from "./components/PaymentInfo/payment-info";
import RegistrationInfo from "./components/RegistrationInfo/reg-info";
import { dispatchAlertAction, AlertMessageType } from "../alert-snackbar/alert-snackbar-actions";
import dispatch from "../alert-snackbar/alert-snackbar-redux";
import { SavedDraftInfo, NewDraftObj } from "../../utils/core-types/educator-types";
import { RegFormQuesOption, RegFormQuestion, RegFormQuesType } from "../../utils/core-types/learner-reg";
import { mapSavedDraftToNewDraftObj, getDefaultStartDate, validateBeforeSubmit, mapNewDraftToNewClassObj } from "./helper";
import { NewClassObj } from "../../utils/core-types/class-types";
import WaitingDialog from "../dialog-screens/waiting-dialog";
import FeedbackDialog from "../dialog-screens/feedback-dialog";
import IntroGuide from "./components/IntroGuide/intro-guide";
import useStyles from './create-class-styles';
import { ChevronLeft, ChevronRight } from "@material-ui/icons";
import { useHistory } from "react-router-dom";

export type ErrorType = { [P in keyof NewDraftObj]?: string }

export type RegFormState = {
    draft: NewDraftObj,
    errors: ErrorType
}

export type StepperProp = {
    stepData: RegFormState,
    handleStepData: React.Dispatch<any>
};

//used to keep track of question-ids already used in the reg-form.
let qid = 0;


function validate(draft: NewDraftObj): ErrorType {
    let errors: ErrorType = {}

    if (draft.organizerName === "") {
        errors.organizerName = "required";
    }

    if (draft.title === "") {
        errors.title = "required";
    }
    return errors;
}

const stepDataReducer = (state: RegFormState, action: any): RegFormState => {

    if (action.type === "edit-form-data") {
        let newState = { ...state };
        newState.draft[action.payload.name] = action.payload.value;
        console.log("New State ", newState);
        newState.errors = validate(newState.draft);
        return newState;

    } else if (action.type === "add-option") {
        console.log("add-options triggered", state.draft.regFormData)
        let currentQ = state.draft.regFormData![action.payload.qid];
        let newOption: RegFormQuesOption = { text: "new option", selected: false };
        if (currentQ.options) {
            currentQ.options = [...currentQ.options, newOption];
        } else {
            currentQ.options = [newOption]
        }

        state.draft.regFormData = {
            ...state.draft.regFormData,
            [action.payload.qid]: currentQ
        };
        console.log(state.draft.regFormData);
        return { ...state };

    } else if (action.type === "select-type") {

        let currentQ = state.draft.regFormData![action.payload.qid];
        currentQ.type = action.payload.newType;
        state.draft.regFormData = {
            ...state.draft.regFormData,
            [action.payload.qid]: currentQ
        };
        return { ...state };

    } else if (action.type === "edit-option") {

        let currentQ = state.draft.regFormData![action.payload.qid];
        currentQ.options![action.payload.optionIndex].text = action.payload.newValue;
        state.draft.regFormData = {
            ...state.draft.regFormData,
            [action.payload.qid]: currentQ
        };
        return { ...state };

    } else if (action.type === "edit-question") {

        let currentQ = state.draft.regFormData![action.payload.qid];
        currentQ.question = action.payload.newValue;
        state.draft.regFormData = {
            ...state.draft.regFormData,
            [action.payload.qid]: currentQ
        };
        return { ...state };

    } else if (action.type === "add-field") {

        state.draft.regFormData = {
            ...state.draft.regFormData,
            [qid]: { qid, question: '', type: RegFormQuesType.TEXT, required: false, selectedOption: '', options: [] }
        };
        qid++;
        return { ...state };

    } else if (action.type === "delete-question") {

        let newRegFormData = { ...state.draft.regFormData };
        delete newRegFormData[action.payload.qid];
        state.draft.regFormData = newRegFormData
        return { ...state };

    } else if (action.type === "toggle-required") {

        state.draft.regFormData![action.payload.qid].required = !state.draft.regFormData![action.payload.qid].required
        return { ...state };

    } else if (action.type === "delete-option") {

        state.draft.regFormData![action.payload.qid].options!.splice(action.payload.optionIndex, 1);
        return { ...state };
    }

    return state;
}

const stepReducer = (state: any, action: any) => {
    switch (action) {
        case "next":
            if (state.currentStep < state.titles.length - 1)
                return { ...state, currentStep: state.currentStep + 1 }
            else
                return { ...state, currentStep: state.titles.length - 1 }
        case "previous":
            if (state.currentStep >= 1)
                return { ...state, currentStep: state.currentStep - 1 }
        default:
            return state;
    }
}

const initialStateCalc = (savedDraft?: SavedDraftInfo): RegFormState => {

    //intialize the largest question-id in use (if-any)...
    if (savedDraft && savedDraft.regFormData) {
        let formKeys: Array<number> = Object.keys(savedDraft.regFormData).map(key => parseInt(key));
        if (formKeys.length !== 0)
            qid = Math.max(...formKeys) + 1;
    }

    var state: RegFormState
    if (savedDraft) {
        state = {
            draft: mapSavedDraftToNewDraftObj(savedDraft),
            errors: validate(savedDraft)
        }
    } else {
        state = {
            draft: {
                organizerName: '',
                title: '',
                isPaid: false,
            },
            errors: { title: 'required', organizerName: 'required' }
        }
    }
    return state
}


type IProp = {
    savedDraft?: SavedDraftInfo,
    onClassCreate: (newClassObj: NewClassObj, draftId?: number) => Promise<boolean>,
    onDraftCreate: (newDraftObj: NewDraftObj, draftId?: number) => Promise<boolean>,
    publishAlerts: (type: AlertMessageType, message: string) => void
}

const CreateClass: React.FC<IProp> = ({ savedDraft, onClassCreate, onDraftCreate, publishAlerts }) => {

    const classes = useStyles();

    const [stepData, handleStepData] = React.useReducer(stepDataReducer, savedDraft, initialStateCalc);

    const [introGuide, setIntroGuideView] = React.useState(savedDraft == undefined);

    const [draftSaveDialogView, setDraftSaveDialogView] = React.useState(false);
    const [classPublishSuccessDialogView, setClassPublishSuccessDialogView] = React.useState(false);
    const [classPublishErrorDialogView, setClassPublishErrorDialogView] = React.useState(false);
    const [classPublishErrors, setClassPublishErrors] = React.useState<ErrorType>({})
    const [networkWaiting, setNetworkWaiting] = React.useState(false);

    const history = useHistory();


    const [step, handleStep] = React.useReducer(stepReducer, {
        currentStep: 0,
        titles: ["Basic Info", "Additional Info", "Payment Info", "Registration Info"]
    });

    const handleIntroProceedClick = () => {
        setIntroGuideView(false);
    }

    const handleDraftSaveDialogClose = (confirm: boolean) => {
        if (confirm) {
            setNetworkWaiting(true);
            onDraftCreate(stepData.draft, savedDraft?.draftId)
                .then((isSuccess) => {
                    setNetworkWaiting(false);
                    setDraftSaveDialogView(false);
                    if (isSuccess) {
                        publishAlerts(AlertMessageType.SUCCESS_MESSAGE, "Draft Saved Successfully");
                    } else {
                        publishAlerts(AlertMessageType.ERROR_MESSAGE, "Could not save draft , Please try again");
                    }
                })

        } else {
            setDraftSaveDialogView(false);
        }
    }

    const handleClassPublishSuccessDialogClose = (confirm: boolean) => {
        if (confirm) {
            setNetworkWaiting(true);
            onClassCreate(mapNewDraftToNewClassObj(stepData.draft), savedDraft?.draftId)
                .then((isSuccess) => {
                    setNetworkWaiting(false);
                    setClassPublishSuccessDialogView(false);
                    if (isSuccess) {
                        publishAlerts(AlertMessageType.SUCCESS_MESSAGE, "Class Created Successfully");
                        //yaha par redirect kar denge to educator home page
                        history.push('/educator');

                    } else {
                        publishAlerts(AlertMessageType.ERROR_MESSAGE, "Could not create class, Please try again");
                    }

                })

        } else {
            setClassPublishSuccessDialogView(false);
        }

    }

    const onDraftSaveReq = () => {
        setDraftSaveDialogView(true);
    }

    const onClassPublishReq = () => {
        const errorObj = validateBeforeSubmit(stepData.draft);
        if (Object.keys(errorObj).length == 0) {
            setClassPublishSuccessDialogView(true);
        } else {
            setClassPublishErrors(errorObj);
            setClassPublishErrorDialogView(true);
        }
    }

    return (
        <>
            {
                introGuide
                    ? <IntroGuide handleIntroProceedClick={handleIntroProceedClick} />
                    : <>
                        {Object.keys(classPublishErrors).length > 0 && (
                            <Box className={classes.errorBox}>
                                <ul>
                                    {
                                        Object.keys(classPublishErrors).map((fieldKey) => {
                                            return <li key={fieldKey}><Typography color="inherit">{classPublishErrors[fieldKey]}</Typography></li>
                                        })
                                    }
                                </ul>
                            </Box>
                        )}

                        {(() => {
                            switch (step.currentStep) {
                                case 0:
                                    return <BasicInfo stepData={stepData} handleStepData={handleStepData} />;
                                case 1:
                                    return <AdditionalInfo stepData={stepData} handleStepData={handleStepData} />;
                                case 2:
                                    return <PaymentInfo stepData={stepData} handleStepData={handleStepData} />;
                                case 3:
                                    return <RegistrationInfo stepData={stepData} handleStepData={handleStepData} />;
                                default:
                                    break;
                            }
                        })()}

                        <WaitingDialog open={draftSaveDialogView} waiting={networkWaiting} onDialogClose={handleDraftSaveDialogClose} dialogTitle='Are you sure?' okBtnText='Save Draft' cancelBtnText='Cancel'>
                            Do you really want to save the draft?
                        </WaitingDialog>

                        <WaitingDialog open={classPublishSuccessDialogView} waiting={networkWaiting} onDialogClose={handleClassPublishSuccessDialogClose} dialogTitle='Are you sure?' okBtnText='Publish Class' cancelBtnText='Cancel'>
                            Do you really want to save the publish, you cannot revert back?
                        </WaitingDialog>

                        <Box className={classes.bottomNav}>
                            <Button
                                variant="text"
                                color="primary"
                                onClick={() => handleStep("previous")}
                                disabled={step.currentStep === 0}
                                startIcon={step.currentStep === 0 ? <ChevronLeft color="disabled" /> : <ChevronLeft color="primary" />}
                            >
                                Previous
                            </Button>

                            <Button
                                variant="text"
                                color="primary"
                                onClick={() => handleStep("next")}
                                disabled={step.currentStep === step.titles.length - 1 || Object.keys(stepData.errors).length !== 0}
                                endIcon={step.currentStep === step.titles.length - 1 || Object.keys(stepData.errors).length !== 0 ? <ChevronRight color="disabled" /> : <ChevronRight color="primary" />}
                            >
                                Next
                            </Button>

                            <Box className={classes.bottomNavRight}>
                                {
                                    <Button
                                        variant="text"
                                        color="primary"
                                        style={{ marginRight: "1rem" }}
                                    >
                                        Cancel
                                    </Button>
                                }
                                {
                                    step.currentStep === step.titles.length - 1 ? (
                                        <Button
                                            variant="contained"
                                            color="primary"
                                            disabled={Object.keys(stepData.errors).length !== 0}
                                            onClick={() => onClassPublishReq()}
                                        >
                                            Submit
                                        </Button>
                                    ) : (
                                            <Button
                                                variant="contained"
                                                color="primary"
                                                disabled={Object.keys(stepData.errors).length !== 0}
                                                onClick={() => onDraftSaveReq()}
                                            >
                                                Save as draft
                                            </Button>
                                        )
                                }
                            </Box>
                        </Box>

                    </>

            }
        </>
    );
}

export default CreateClass;

