import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import { red } from "@material-ui/core/colors";

const styles = makeStyles((theme: Theme) =>
    createStyles({
        errorBox: {
            margin: "auto",
            maxWidth: 750,
            padding: 50,
            backgroundColor: red[50],
            color: red[500],
            textAlign: "left",
            boxSizing: "border-box"
        },
        introGuideRoot: {
            display: 'flex',
            flexDirection: 'column',
            maxWidth: 700,
            margin: 'auto'
        },
        bottomNav: {
            position: "fixed",
            bottom: 0,
            width: "100%",
            display: "flex",
            flexWrap: "wrap",
            background: "#fff",
            padding: "1rem",
            boxSizing: "border-box",
            borderTop: "1px solid",
            borderColor: "var(--ck-color-widget-blurred-border)",
            zIndex: 1
        },
        bottomNavRight: {
            marginLeft: "auto"
        },
        [theme.breakpoints.down("xs")]: {
            bottomNav: {
                justifyContent: "center"
            },
            bottomNavRight: {
                marginLeft: 0
            }
        }
    })
);

export default styles;