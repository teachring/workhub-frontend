import { Dialog, DialogTitle, DialogContent, Button, DialogActions, Box, LinearProgress } from "@material-ui/core"
import React, { ReactNode } from "react"


// Dialog to be used when a dialog with network waiting needs to be shown.

type IProp = {
    open: boolean,
    children?: ReactNode,
    waiting: boolean,
    onDialogClose: (isOk: boolean) => void,
    dialogTitle: string,
    okBtnText?: string,
    cancelBtnText?: string,
}


const WaitingDialog: React.FC<IProp> = ({ open, children, waiting, okBtnText = 'OK', cancelBtnText = 'Cancel', onDialogClose, dialogTitle }) => {
    return (
        <Dialog open={open} aria-labelledby="reminder-dialog">
            <DialogTitle id="reminder-dialog">{dialogTitle}</DialogTitle>
            <DialogContent>
                {children}
                {waiting && <Box mt={2}><LinearProgress color="secondary" /></Box>}
            </DialogContent>
            <DialogActions>
                <Button disabled={waiting} onClick={() => onDialogClose(false)} color="primary" variant="text"> {cancelBtnText} </Button>
                <Button variant="contained" disabled={waiting} onClick={() => onDialogClose(true)} color="primary"> {okBtnText} </Button>
            </DialogActions>
        </Dialog>
    )
}

export default WaitingDialog;