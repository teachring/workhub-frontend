import { Dialog, DialogTitle, DialogContent, Button, DialogActions, Box, LinearProgress } from "@material-ui/core"
import React, { ReactNode, useState, useEffect } from "react"


type IProp = {
    open:boolean,
    children?: ReactNode,
    onDialogClose: () => void,
    dialogTitle:string,
    okBtnText?: string,
}


const FeedbackDialog:React.FC<IProp> = ({open,children,okBtnText = 'OK',onDialogClose,dialogTitle}) => {

    return (
        <Dialog open={open} aria-labelledby="reminder-dialog">
            <DialogTitle id="reminder-dialog">{dialogTitle}</DialogTitle>
            <DialogContent>
                {children}
            </DialogContent>    
            <DialogActions>
                <Button onClick={() => onDialogClose()} color="secondary"> {okBtnText} </Button>
            </DialogActions>
        </Dialog>
    )
}

export default FeedbackDialog;