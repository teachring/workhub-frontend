//@ts-ignore
import Cookies from 'js-cookie';
import { RegInfo } from '../../utils/core-types/learner-reg';

import { fetchClassDataNotSignedIn, fetchClassDataSignedIn } from '../../utils/apis';

import { ClassesReceivedAction } from '../../utils/reduxReducers/class-reducers';
import { ClassesRegReceivedAction } from '../../utils/reduxReducers/learners/reg-classes';
import { IntKeyDict } from '../../utils/core-types/misc';
import { subscribeToNotifications } from '../../utils/reduxActions/notifications';
import { RootState } from '../../utils/reduxReducers';
export type AppStartAction = {
    type: 'INITIALIZE_APP'
    isLoggedIn: boolean,
    userId?: number,
    userName?: string
}

export type AppInvalidateAction = {
    type: 'INVALIDATE_APP'
}

const AppInitializer = () => {

    //For login
    // Cookies.set("userId", 7)
    // Cookies.set("authJWT", 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySUQiOiJacTNvQ1hiV2c4VzQyTWg2TDdCa3FsRWZCaXYxIiwiaWRUb2tlbiI6ImV5SmhiR2NpT2lKU1V6STFOaUlzSW10cFpDSTZJall6WlRsbFlUaG1Oek5rWldFeE1UUmtaV0k1WVRZME9UY3haREpoTWprek4yUXdZelkzWVdFaUxDSjBlWEFpT2lKS1YxUWlmUS5leUpwYzNNaU9pSm9kSFJ3Y3pvdkwzTmxZM1Z5WlhSdmEyVnVMbWR2YjJkc1pTNWpiMjB2ZDI5eWEyaDFZbVJ5WVdkdmJucDFjbVpsY2lJc0ltRjFaQ0k2SW5kdmNtdG9kV0prY21GbmIyNTZkWEptWlhJaUxDSmhkWFJvWDNScGJXVWlPakUxT0RFeU5qYzBOekVzSW5WelpYSmZhV1FpT2lKYWNUTnZRMWhpVjJjNFZ6UXlUV2cyVERkQ2EzRnNSV1pDYVhZeElpd2ljM1ZpSWpvaVduRXpiME5ZWWxkbk9GYzBNazFvTmt3M1FtdHhiRVZtUW1sMk1TSXNJbWxoZENJNk1UVTRNVEkyTnpRM015d2laWGh3SWpveE5UZ3hNamN4TURjekxDSndhRzl1WlY5dWRXMWlaWElpT2lJck9URTVOemt3TkRnNE9UUXhJaXdpWm1seVpXSmhjMlVpT25zaWFXUmxiblJwZEdsbGN5STZleUp3YUc5dVpTSTZXeUlyT1RFNU56a3dORGc0T1RReElsMTlMQ0p6YVdkdVgybHVYM0J5YjNacFpHVnlJam9pY0dodmJtVWlmWDAuUl91czJ6SXBmbHRHMVhVQ3JFSWJxN252MUxnX2RYT2VaRmV6dW4yd3pJdHdLZDFOMXdEajg5S0xuN0lKWXh2V1dWUk45LXVDLUZVSGswclRBLUg4VW94YVNXVUViRi0xSTByZzZwSkJXbjllVmY3T3BVU3V4b1hJdG1waGl0dXJRX3FFNURldjZxSkgya0o3cVhLOGJrZXdjazVTbEtvU252Z2RRZGgwd0UyTGpyU2gzdWtPTk94dXZsOVhRLXJHc0cxU2Y3bDVwT05SdXJmLXJENUppTHhWTm40YVV6bkZqSExjY2VBVmk3UlU3OGVGYTQtS3hMMnJWNS16eUQwX0ZFUi1mVkVSV1RkRVBBUWtITVdhWVNuUnVTTkp6bWRuei1KcUhTdXQ4SGljNWVyQ1NRaGVoWUVtRGktTFZyTEQ4dEQxdG8yZmJKNmRhMjl6cFoybXd3IiwiZXhwIjoxNTgxMzEwNjc1fQ.E0OAiLafsjIaKIfBVBkKCr6TSHPR5tCYXXZmCJYJh9Y')
    // Cookies.set("userName", "Udit")


    let isLoggedIn = true
    let userId = Cookies.get('userId')
    console.log("COOKIES ", Cookies.get())
    let userName = Cookies.get('userName')
    if (!userId) {
        isLoggedIn = false
    } else {
        userId = parseInt(userId)
    }

    return function (dispatch: (arg0: ClassesReceivedAction | ClassesRegReceivedAction | AppStartAction | Function) => any) {

        if (isLoggedIn) {

            fetchClassDataSignedIn()
                .then(([regClasses, upcomingClasses]) => {

                    dispatch({ type: 'CLASSES_RECEIVED', classList: upcomingClasses.concat(regClasses.map(item => item.classInfo)) })

                    let regInfoDict: IntKeyDict<RegInfo> = {}

                    regClasses.forEach(regClass => {
                        regInfoDict[regClass.classInfo.classId] = regClass.regData
                    })

                    dispatch({ type: 'CLASS_REG_DATA_RECEIVED', regInfoDict: regInfoDict })

                    dispatch({ type: 'INITIALIZE_APP', isLoggedIn: true, userId: userId, userName: userName })

                    dispatch(subscribeToNotifications(userId))
                })

        } else {

            fetchClassDataNotSignedIn()
                .then((upcomingClasses) => {
                    dispatch({ type: 'CLASSES_RECEIVED', classList: upcomingClasses })
                    dispatch({ type: 'INITIALIZE_APP', isLoggedIn: false })
                })
        }
    }

}


export const onAppLogout = (onSuccessAction: () => void) => {
    return function (dispatch: (arg0: AppInvalidateAction) => void, getState: () => RootState) {
        //when somebody logout, then there is a concern that we won't be able to 

        const notificationTimerId = getState().NotificationData.timerId;
        if (notificationTimerId)
            clearTimeout(notificationTimerId);

        Cookies.remove('userId');
        Cookies.remove('userName');
        Cookies.remove('authJWT');
        dispatch({ type: 'INVALIDATE_APP' });
        onSuccessAction();
    }
}


export default AppInitializer;