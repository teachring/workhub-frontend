import React from "react";
import "./App.css";

import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";

import { ClassDetailsComp } from "../class-details";

import { TopAppBarComp } from "../shared/gen-top-app-bar";
import { AlertSnackbarComp } from "../alert-snackbar";
import { Route, Switch, Redirect } from "react-router-dom";
import { NotificationsComp } from "../../routes/notifications";
import { EducatorHomeComp } from "../educator-home";
import { CreateClassComp } from "../create-class";
import { LearnerRegComp } from "../learner-register";

import { EducatorClassroomComp } from "../educator-classroom";
import { LearnerClassroomComp } from "../learner-classroom";
import { LearnerHomeComp } from "../learner-home";
import { LoginSignupComp } from "../login-signup";
import { LearnerClassRouterComp } from "../../routes/learner-class-router/learner-class-router";
import Homepage from "../../routes/homepage/Homepage";
import NotFound from "../shared/404/404";
import { Typography } from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        app: {},
        brand: {
            fontFamily: "Montserrat Alternates, Nunito",
            fontWeight: "bold",
            fontSize: "1.6rem",
            letterSpacing: 1
        },
        splashscreen: {
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
            textAlign: "center"
        },
        loading: {
            width: 150,
            height: 2,
            margin: "10px 0",
            borderRadius: 2,
            backgroundColor: "#cfcfcf",
            position: "relative",
            overflow: "hidden",
            zIndex: 1,
            transition: "transform .3s ease-in, -webkit-transform .3s ease-in",
            "&>div": {
                height: "100%",
                width: 68,
                position: "absolute",
                transform: "translate(-34px, 0)",
                backgroundColor: theme.palette.primary.main,
                borderRadius: 2,
                animation: "$loading 1.5s infinite ease",
            },
        },
        "@keyframes loading": {
            "0%": {},
            "50%": {
                transform: "translate(116px, 0)"
            },
            "100%": {
                transform: "translate(-34px, 0)"
            }
        }
    })
);

type IProp = {
    appReady: Boolean,
    isSignedIn: boolean,
    initializeApp: () => void
}

const App: React.FC<IProp> = ({ appReady, initializeApp, isSignedIn }) => {
    const classes = useStyles();
    if (!appReady) { initializeApp(); }

    return (
        <div>
            {appReady ?
                <div className={"App " + classes.app}>
                    <TopAppBarComp />
                    <Switch>
                        <Route path="/notifications">
                            <NotificationsComp />
                        </Route>
                        <Route path="/educator/classroom/:class_name">
                            <EducatorClassroomComp />
                        </Route>
                        <Route path="/educator/create-class">
                            <CreateClassComp />
                        </Route>
                        <Route path="/class/:class_name">
                            <LearnerClassRouterComp />
                        </Route>
                        <Route path="/educator">
                            <EducatorHomeComp />
                        </Route>
                        <Route path='/browse'>
                            <LearnerHomeComp />
                        </Route>
                        <Route exact path="/">
                            {isSignedIn ? <Redirect to="/browse" /> : <Homepage />}
                        </Route>
                        <Route>
                            <NotFound />
                        </Route>
                    </Switch>
                    <AlertSnackbarComp />
                    <LoginSignupComp />
                </div>
                :
                <div className={classes.splashscreen}>
                    <Typography variant="h6" className={classes.brand}>Teachring</Typography>
                    <div className={classes.loading}><div></div></div>
                </div>
            }

        </div>
    );
};

export default App;
