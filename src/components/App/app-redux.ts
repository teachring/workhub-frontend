import App from './App';
import { connect } from 'react-redux';
import AppInitializer from './actions';
import { RootState } from '../../utils/reduxReducers';


const mapStateToProps = (state: RootState) => {
    return {
        appReady: state.AppState.appReady,
        isSignedIn: state.AppState.isLoggedIn
    }
}

const mapDispatchToProps = (dispatch: Function) => {
    return {
        initializeApp: () => {
            dispatch(AppInitializer());
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)

