import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";

const useStyles = makeStyles((theme: Theme) => ({
    tabBar: {
        flexGrow: 1,
        zIndex: 1,
        position: "sticky",
        boxShadow: "0 5px 15px -8px rgba(0, 0, 0, 0.24)",
        [theme.breakpoints.down("sm")]: {
            top: 56
        },
        [theme.breakpoints.up("sm")]: {
            top: 64
        },
        [`${theme.breakpoints.down("xs")} and (orientation: landscape)`]: {
            top: 48
        }
    },
    tabContainer: {
        backgroundColor: theme.palette.background.paper,
        display: "flex",
        overflowX: "auto"
    },
    tabs: {
        display: "inline-flex",
        minWidth: "max-content"
    },
    tab: {
        padding: 20,
        fontSize: "0.8rem",
        letterSpacing: "0.0875em",
        "&$selected": {
            "& span": {
                color: "red"
            }
        }
    },
    tabActionBtn: {
        marginLeft: "auto",
        marginRight: "1rem",
        minWidth: 170,
        boxShadow: theme.shadows[2],
        [theme.breakpoints.down("sm")]: {
            marginRight: 10
        }
    },
    tabBody: {
        padding: "3rem",
        [theme.breakpoints.down("xs")]: {
            flexDirection: "column-reverse",
            "& .MuiGrid-container": {
                flexDirection: "column-reverse"
            }
        }
    },
    briefDetailsPanel: {
        [theme.breakpoints.down("xs")]: {
            padding: "0",
            marginBottom: 30,
            "&>div": {
                padding: "25px 0"
            }
        }
    },
    descContainer: {
        paddingRight: "4rem",
        textAlign: "left",
        [theme.breakpoints.down("xs")]: {
            padding: "0"
        }
    },
    descHeading: {
        fontFamily: "Roboto Slab",
        fontSize: "1.3rem",
        marginBottom: "1rem"
    },
    faqQuestion: {
        fontSize: "1.2rem",
    },
    briefHeading: {
        display: "flex",
        alignItems: "center",
        justifyContent: "left",
        flex: 1,
        marginBottom: 5,
        "& svg": {
            marginRight: 10
        }
    },
    briefBody: {
        paddingLeft: 35
    },
    askQueryContainer: {
        display: "flex",
        flexWrap: "wrap",
        boxShadow: "0 1px 4px 0 rgba(0, 0, 0, 0.14)",
        borderRadius: 8
    },
    askQueryTextField: {
        flex: 0.75,
        "& .MuiInputBase-root": {
            borderRadius: 0
        }
    },
    submitBtn: {
        color: "#fff",
        border: 0,
        backgroundColor: "#fe6e86",
        boxShadow: "0 3px 5px 0 rgba(255, 105, 135, .3)",
        borderRadius: 3,
        "&.Mui-disabled": {
            color: "rgba(0, 0, 0, 0.26)",
            boxShadow: "none",
            backgroundColor: "rgba(0, 0, 0, 0.12)"
        },
        "&:hover": {
            backgroundColor: "#fe8268",
            boxShadow: "0 3px 5px 2px rgba(255, 105, 135, .3)",
        }
    },
    queryContainer: {
        display: "flex",
        alignItems: "center",
        justifyContent: "space-evenly",
        flex: "100%",
        marginTop: 15,
        marginBottom: 25
    },
    oldQueries: {
        display: "block",
        flex: "100%",
        "&>*": {
            padding: "25px 40px",
            display: "block",
            borderTop: "1px solid #ddd"
        }
    }
}));

export default useStyles;