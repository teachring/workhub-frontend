import { LearnerRegQuery } from "../../utils/core-types/learner-reg";
import { RootState } from "../../utils/reduxReducers";
import { IntKeyDict } from "../../utils/core-types/misc";
import {fetchAllRegEnquiries, postNewRegEnquiry } from '../../utils/apis/reg-enquires';


export type ReceiveLearnerRegQueryAction = {
    type: 'RECEIVE_LEARNER_REG_QUERY',
    classId: number,
    queryList: LearnerRegQuery[]
}

export type PublishLearnerRegQueryAction = {
    type: 'PUBLISH_NEW_LEARNER_REG_QUERY',
    classId: number,
    query: LearnerRegQuery
}

export const fetchQueriesIfNeeded = (classId: number) => {
    return function (dispatch: (arg0: ReceiveLearnerRegQueryAction) => any, getState: () => RootState) {
        //Now I need to check if something is there or not 
        const queries = getState().LearnerState.RegistrationQueries[classId]
        if(!queries){
            fetchAllRegEnquiries(classId)
                .then(queryList => {
                    dispatch({ type: 'RECEIVE_LEARNER_REG_QUERY', classId: classId, queryList: queryList })
                })
        }
    }
}

export const publishNewQuery = (classId: number, newQuery: string) => {
    return function (dispatch: (arg0: PublishLearnerRegQueryAction) => any) {
        return postNewRegEnquiry(classId, newQuery)
            .then(newQueryObj => {
                dispatch({ type: 'PUBLISH_NEW_LEARNER_REG_QUERY', query: newQueryObj, classId: classId })
                return true;
            },
                err => { return false }
            )
    }
}