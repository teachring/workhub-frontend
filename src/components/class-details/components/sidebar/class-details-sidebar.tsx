import React from 'react';
import { Box, Typography } from '@material-ui/core';
import useStyles from './details-sidebar-styles';
import { ClassInfo } from '../../../../utils/core-types/class-types';
import { readDateTime } from '../../../../utils/helpers';
import RupeeBold from '../../../../assets/icons/RupeeBold';
import {
    RoomOutlined as RoomOutlinedIcon,
    LocalLibraryOutlined as LocalLibraryOutlinedIcon,
    PersonAddOutlined as PersonAddOutlinedIcon
} from "@material-ui/icons";

type IProp = {
    classInfoObj?: ClassInfo
}

const DetailsSidebar: React.FC<IProp> = ({ classInfoObj }) => {

    const classes = useStyles();

    return (
        <div className={classes.briefDetailsPanel}>
            <Box p={3} textAlign="left">
                <Box className={classes.briefHeading}>
                    <LocalLibraryOutlinedIcon />
                    <Typography variant="button">Class Timings</Typography>
                </Box>
                <Box className={classes.briefBody}>
                    {classInfoObj && (
                        <Box display="flex">
                            <Box mr={1}>
                                <Box mb={1}><Typography variant="body2">From</Typography></Box>
                                <Typography variant="body2">To</Typography>
                            </Box>
                            <Box>
                                <Box mb={1}><Typography variant="body2">: {readDateTime(classInfoObj.startDateTime, false)}</Typography></Box>
                                <Typography variant="body2">: {readDateTime(classInfoObj.endDateTime, false)}</Typography>
                            </Box>
                        </Box>
                    )}
                </Box>
            </Box>
            <Box p={3} textAlign="left">
                <Box className={classes.briefHeading}>
                    <PersonAddOutlinedIcon />
                    <Typography variant="button">Registration Timings</Typography>
                </Box>
                <Box className={classes.briefBody}>
                    {classInfoObj && (
                        <Box display="flex">
                            <Box mr={1}>
                                <Box mb={1}><Typography variant="body2">From</Typography></Box>
                                <Typography variant="body2">To</Typography>
                            </Box>
                            <Box>
                                <Box mb={1}><Typography variant="body2">: {readDateTime(classInfoObj.regStartDateTime, false)}</Typography></Box>
                                <Typography variant="body2">: {readDateTime(classInfoObj.regEndDateTime, false)}</Typography>
                            </Box>
                        </Box>
                    )}
                </Box>
            </Box>
            <Box p={3} textAlign="left">
                <Box className={classes.briefHeading}>
                    <RoomOutlinedIcon />
                    <Typography variant="button">Venue</Typography>
                </Box>
                <Box className={classes.briefBody}>
                    <Typography variant="body2">
                        {classInfoObj?.venue}
                    </Typography>
                </Box>
            </Box>
            <Box p={3} textAlign="left">
                <Box className={classes.briefHeading}>
                    <Box style={{ transform: "scale(0.7)" }}><RupeeBold /></Box>
                    <Typography variant="button">Fees</Typography>
                </Box>
                <Box className={classes.briefBody}>
                    <Typography variant="body2">
                        {classInfoObj?.isPaid ? `Rs. ${classInfoObj.paymentDetails?.feeAmt} /-` : "FREE"}
                    </Typography>
                </Box>
            </Box>
        </div >
    )
}

export default DetailsSidebar;