import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";

const useStyles = makeStyles((theme: Theme) => ({
    briefDetailsPanel: {
        [theme.breakpoints.down("xs")]: {
            padding: "0",
            marginBottom: 30,
            "&>div": {
                padding: "25px 0"
            }
        }
    },
    descContainer: {
        paddingRight: "4rem",
        textAlign: "left",
        [theme.breakpoints.down("xs")]: {
            padding: "0"
        }
    },
    briefHeading: {
        display: "flex",
        alignItems: "center",
        justifyContent: "left",
        flex: 1,
        marginBottom: 5,
        "& svg": {
            marginRight: 10
        },
        "& .MuiTypography-root": {
            fontSize: "1rem"
        }
    },
    briefBody: {
        paddingLeft: 35
    },
    askQueryContainer: {
        display: "flex",
        flexWrap: "wrap",
        boxShadow: "0 1px 4px 0 rgba(0, 0, 0, 0.14)",
        borderRadius: 8
    }
}));

export default useStyles;