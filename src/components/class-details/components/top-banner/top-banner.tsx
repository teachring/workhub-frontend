import React from 'react';
import { Box, Typography, Button } from '@material-ui/core';
import useStyles from './top-banner-styles';
import { ClassInfo } from '../../../../utils/core-types/class-types';
import { readDate } from '../../../../utils/helpers';


type IProp = {
    isSignedIn: boolean,
    isRegistered: boolean,
    classInfo: ClassInfo,
    onRegRequest?: () => void,
    showActionBtn?: boolean
}

const TopBanner: React.FC<IProp> = ({ isSignedIn, classInfo, onRegRequest, isRegistered, showActionBtn = true }) => {

    const bg_gradient = classInfo.bgColor;
    const bg_color = bg_gradient.substring(bg_gradient.indexOf("#"), bg_gradient.indexOf("#") + 7);

    const classes = useStyles({ bg_gradient, bg_color });

    const now = new Date();
    let subtitle = "";

    let btn_disable = false;

    if (isRegistered) {
        btn_disable = true;

        if (now < classInfo.startDateTime) {
            subtitle = "You are registered. Class starts on " + readDate(classInfo.startDateTime);
        } else if (now < classInfo.endDateTime) {
            subtitle = "You are registered. Class ends on " + readDate(classInfo.endDateTime);
        } else {
            subtitle = "You are registered. Class ended on " + readDate(classInfo.endDateTime);
        }

    } else {

        if (now < classInfo.regStartDateTime) {
            btn_disable = true;
            subtitle = "Registration starts on " + readDate(classInfo.regStartDateTime);
        } else if (now < classInfo.regEndDateTime) {
            btn_disable = false;
            subtitle = "Registration closes on " + readDate(classInfo.regEndDateTime);
        } else {
            btn_disable = true;
            subtitle = "Registration closed on " + readDate(classInfo.regEndDateTime);
        }

    }

    return (
        <div className={classes.cover}>
            <Box width="100%" textAlign="left">
                <Typography variant="overline" className={classes.organizer}>{classInfo.organizerName}</Typography>
                <Typography variant="h5" color="inherit" className={classes.title}>{classInfo.title}</Typography>
                <Typography variant="body1" color="inherit">{subtitle}</Typography>
                {!btn_disable && showActionBtn && <Button className={classes.bannerBtn} onClick={onRegRequest}>ENROLL</Button>}
            </Box>
        </div>
    )
}

export default TopBanner;