import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";

interface CardStyleProps {
    bg_color: string;
    bg_gradient: string;
}

const styles = makeStyles<Theme, CardStyleProps>((theme: Theme) =>
    createStyles({
        cover: {
            display: "flex",
            flexDirection: "column",
            justifyContent: "space-around",
            alignItems: "baseline",
            position: "relative",
            overflow: "hidden",
            padding: 50,
            "&:before": {
                top: 0,
                left: 0,
                zIndex: -1,
                content: "''",
                position: "absolute",
                width: "100%",
                height: "100%",
                backgroundImage: props => props.bg_gradient
            },
            "& .MuiTypography-root": {
                color: "#fff"
            }
        },
        organizer: {
            fontSize: "1rem"
        },
        title: {
            fontWeight: 900,
            textTransform: "uppercase",
            textAlign: "left",
            fontSize: "2rem",
            position: "relative",
            letterSpacing: 3,
            marginBottom: 30,
            "&::after": {
                content: "''",
                position: "absolute",
                width: 50,
                height: 3,
                background: "#fff",
                bottom: -10,
                left: 2
            },
        },
        bannerBtn: {
            background: "#fff",
            borderRadius: 3,
            width: 150,
            height: 60,
            color: "#5f5f5f",
            fontWeight: 400,
            letterSpacing: 3,
            marginTop: 30,
            "&:hover": {
                background: "#fff",
                color: props => props.bg_color
            }
        },
    })
);

export default styles;