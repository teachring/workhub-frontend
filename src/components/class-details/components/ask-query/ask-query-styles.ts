import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";

const useStyles = makeStyles((theme: Theme) => ({
    queryBox: {
        display: "flex",
        flexWrap: "wrap",
        alignItems: "center",
        background: "#fff",
        padding: "2rem",
        position: "relative",
        overflow: "hidden",
        "&:before": {
            content: "''",
            position: "absolute",
            top: 0,
            left: 0,
            background: "#9c28b0",
            width: 4,
            height: "100%",
        }
    },
    queryInput: {
        flex: 1,
        minWidth: 200,
    },
    askBtn: {
        marginLeft: "2rem",
        marginBottom: "1rem",
        minWidth: 100
    },
    queryList: {
        marginTop: "2rem"
    },
    oldQuery: {
        padding: "2rem"
    },
    [theme.breakpoints.down("md")]: {
        queryInput: {
            marginBottom: "1rem"
        }
    }
}));

export default useStyles;