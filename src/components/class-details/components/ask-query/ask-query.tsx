import React from 'react';
import { Box, Typography, TextField, Button, InputBase, Paper } from '@material-ui/core';
import useStyles from './ask-query-styles';
import { LearnerRegQuery } from '../../../../utils/core-types/learner-reg';
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import { HelpOutlineOutlined } from '@material-ui/icons';
import { readDate } from '../../../../utils/helpers';
import WaitingDialog from '../../../dialog-screens/waiting-dialog';
import { AlertMessageType } from '../../../alert-snackbar/alert-snackbar-actions';

type IProp = {
    regQueries?: LearnerRegQuery[],
    onNewQueryPost: (query: string) => Promise<boolean>,
    askOrganizerRef: React.MutableRefObject<null>,
    publishAlerts: (type: AlertMessageType, message: string) => void
}

const AskQuery: React.FC<IProp> = ({ regQueries, onNewQueryPost, askOrganizerRef ,publishAlerts}) => {

    const classes = useStyles();

    const [query, setQuery] = React.useState("");

    const [waiting, setWaiting] = React.useState(false);
    const [openNewQueryDialog, setNewQueryDialogOpen] = React.useState(false);

    const handleQuery = (event: React.ChangeEvent<HTMLInputElement>) => {
        if (event.target.value.length <= 160)
            setQuery(event.target.value);
    }

    const handleNewQueryDialogClose = (confirm:boolean) => {
        if(confirm) {
            setWaiting(true);
            onNewQueryPost(query)
            .then((isSuccess:boolean) => {
                setWaiting(false);
                setNewQueryDialogOpen(false);
                if(isSuccess) {
                    setQuery('');
                    publishAlerts(AlertMessageType.SUCCESS_MESSAGE,"Your enquiry is sent, you will soon receive a callback")
                } else {
                    publishAlerts(AlertMessageType.ERROR_MESSAGE,"Enquiry could not be sent, please try again")
                }
            })
        } else {
            setNewQueryDialogOpen(false);
        }

    }

    return (
        <>
            <Paper elevation={2} className={classes.queryBox}>
                <Box display="flex" flex="1">
                    <Box mt="4px" mr={2}>
                        <HelpOutlineOutlined color="primary" />
                    </Box>
                    <TextField
                        className={classes.queryInput}
                        placeholder="Enter Query"
                        value={query}
                        onChange={handleQuery}
                        helperText={query.length + " / 160 characters"}
                        inputRef={askOrganizerRef}
                    />
                </Box>
                <Button className={classes.askBtn} variant="contained" color="primary" onClick={(e) => {setNewQueryDialogOpen(true)}}> ask </Button>

                <WaitingDialog open={openNewQueryDialog} waiting={waiting}       onDialogClose={handleNewQueryDialogClose} dialogTitle='Submit your Enquiry ?' okBtnText="Send">
                    <Typography>Soon after we receive an enquiry, we respond back to you over phone. It might take us some time to revert back.</Typography>
                </WaitingDialog>

            </Paper>

            {
                regQueries && regQueries.length > 0
                    ? <Paper elevation={2} className={classes.queryList}>
                        {
                            regQueries.map(query => (
                                <Box key={query.queryId} className={classes.oldQuery}>
                                    <Typography>{query.query}</Typography>
                                    <Typography variant="overline">{readDate(query.askedDateTime)}</Typography>
                                </Box>
                            ))
                        }
                    </Paper>
                    : ""
            }

        </>
    )
}

export default AskQuery;