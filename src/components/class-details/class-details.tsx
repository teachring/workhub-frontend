import React, { useEffect, useRef, useState } from "react";
import { Tabs, Tab, Button, Grid, Box, Typography, TextField } from "@material-ui/core";
import {
    RoomOutlined as RoomOutlinedIcon,
    AttachMoneyOutlined as AttachMoneyOutlinedIcon,
    EventOutlined as EventOutlinedIcon,
    LocalLibraryOutlined as LocalLibraryOutlinedIcon,
    PersonAddOutlined as PersonAddOutlinedIcon
} from "@material-ui/icons";
import { useHistory } from "react-router-dom";

import { ClassInfo, FAQ } from "../../utils/core-types/class-types";
import TopBanner from './components/top-banner/top-banner';
import { LearnerRegQuery } from "../../utils/core-types/learner-reg";
import TabPanel from "../login-signup/components/user-form/tab-panel";
import useStyles from "./class-details-styles";
import { readDate, readDateTime } from "../../utils/helpers";
import RupeeLight from "../../assets/icons/RupeeLight";
import RupeeBold from "../../assets/icons/RupeeBold";
import DetailsSidebar from "./components/sidebar/class-details-sidebar";
import AskQuery from "./components/ask-query/ask-query";
import { AlertMessageType } from "../alert-snackbar/alert-snackbar-actions";

interface IProp {
    isSignedIn: boolean
    classInfoObj?: ClassInfo,
    isRegistered: boolean,
    //if undefined, waiting for 
    regQueries?: LearnerRegQuery[],
    onNewQueryPost: (query: string) => Promise<boolean>,
    fetchAllQueriesIfNeeded: () => void,
    showLoginSignupWin: () => void,
    publishAlerts: (type: AlertMessageType, message: string) => void
}

const scrollToRef = (ref: React.MutableRefObject<any>) => {
    if (ref.current) {
        // window.scrollTo({
        //     top: ref.current.offsetTop - 140,
        //     behavior: 'smooth',
        // })
        ref.current.focus();
    }
};

const ClassDetails: React.FC<IProp> = ({ isSignedIn, classInfoObj, fetchAllQueriesIfNeeded, showLoginSignupWin, isRegistered, regQueries, onNewQueryPost, publishAlerts }) => {
    const classes = useStyles();
    const [value, setValue] = React.useState(0);
    const history = useHistory();

    const askOrganizerRef = useRef(null);

    const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
        setValue(newValue);
    };

    useEffect(() => {
        if (isSignedIn) {
            fetchAllQueriesIfNeeded()
        }
    }, [isSignedIn])

    return (
        <div>
            <TopBanner
                isSignedIn={isSignedIn}
                classInfo={classInfoObj!}
                isRegistered={isRegistered}
                onRegRequest={() => {
                    if (!isSignedIn) {
                        showLoginSignupWin();
                    } else {
                        // console.log('Proceed to regiser')
                        history.push(history.location.pathname.replace(/\/$/, "") + '/register')
                    }

                }} />

            <div className={classes.tabBar}>
                <div className={classes.tabContainer}>
                    <Tabs
                        className={classes.tabs}
                        value={value}
                        onChange={handleChange}
                        indicatorColor="primary"
                        textColor="primary"
                    >
                        <Tab className={classes.tab} label="Details" />
                        <Tab className={classes.tab} label="Prerequisites" />
                    </Tabs>
                    <Button
                        className={classes.tabActionBtn}
                        variant="contained"
                        color="primary"
                        onClick={() => scrollToRef(askOrganizerRef)}
                    >
                        Raise Enquiry
                    </Button>
                </div>
            </div>

            <TabPanel value={value} index={0}>
                <Grid
                    container
                    direction="column"
                    justify="flex-start"
                    alignItems="stretch"
                >
                    <Grid item className={classes.tabBody}>
                        <Grid container>
                            <Grid item xs={12} sm={7} md={8} className={classes.descContainer}>

                                <Box mb={4}>
                                    <Typography className={classes.descHeading}>Agenda</Typography>
                                    <Typography>{classInfoObj?.agenda}</Typography>

                                </Box>

                                <Box mb={4}>
                                    <Typography className={classes.descHeading}>Description</Typography>
                                    <Typography>{classInfoObj?.description}</Typography>
                                </Box>

                                {
                                    classInfoObj?.otherDetails && (
                                        <Box mb={4}>
                                            <Typography className={classes.descHeading}>Please note</Typography>
                                            <Typography>{classInfoObj.otherDetails}</Typography>
                                        </Box>
                                    )
                                }

                                {
                                    classInfoObj?.faq && (
                                        <Box mb={4}>
                                            <Typography className={classes.descHeading}>FAQ</Typography>
                                            {
                                                classInfoObj.faq.map((qa: FAQ) => (
                                                    <Box mb={3}>
                                                        <Box display="flex" mb={2}>
                                                            <Box mr={2}><Typography className={classes.faqQuestion}>Q</Typography></Box>
                                                            <Typography className={classes.faqQuestion}>{qa.q}</Typography>

                                                        </Box>
                                                        <Box display="flex">
                                                            <Box mr={2}><Typography className={classes.faqQuestion}>A</Typography></Box>
                                                            <Typography>{qa.a}</Typography>
                                                        </Box>
                                                    </Box>
                                                ))
                                            }
                                        </Box>
                                    )
                                }

                                <AskQuery regQueries={regQueries} onNewQueryPost={onNewQueryPost} askOrganizerRef={askOrganizerRef} publishAlerts={publishAlerts} />

                            </Grid>
                            <Grid item xs={12} sm={5} md={4}>
                                <DetailsSidebar classInfoObj={classInfoObj} />
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </TabPanel>

            <TabPanel value={value} index={1}>
                <Box className={classes.tabBody} textAlign="left" dangerouslySetInnerHTML={{ __html: classInfoObj?.prerequirement || "" }} />
            </TabPanel>

        </div>

    );
}

export default ClassDetails;
