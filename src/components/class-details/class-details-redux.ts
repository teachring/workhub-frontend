import { connect } from 'react-redux';
import ClassDetails from './class-details';
import { RootState } from '../../utils/reduxReducers';
import { fetchQueriesIfNeeded, publishNewQuery } from './actions';
import { withRouter } from 'react-router-dom';
import { extractIdFromClassUrl } from '../../utils/helpers';
import { LoginSignupWinShowAction } from '../login-signup/actions';

// isSignedIn: boolean
// classInfoObj:ClassInfo,
// isRegistered: boolean,
// regQueries?: LearnerRegQuery[]
// onNewQueryPost: (query:string) => Promise<boolean>

const mapStateToProps = (state: RootState, ownProps: any) => {
    //const classId = 1;
    // console.log("HELLOO",state.ClassInfoList[classId]);

    console.log("INSIDE CLASS DETAILS", ownProps.classId);
    return {
        isSignedIn: state.AppState.isLoggedIn,
        classInfoObj: state.ClassInfoList[ownProps.classId],
        isRegistered: (state.LearnerState.RegisteredClassesState[ownProps.classId] ? true : false),
        regQueries: state.LearnerState.RegistrationQueries[ownProps.classId]
    }
}

const mapDispatchToProps = (dispatch: Function, ownProps: any) => {
    //const classId = extractIdFromClassUrl(ownProps.match.params.class_name);
    //const classId = 1;
    return {
        onNewQueryPost: (query: string) => {
            return dispatch(publishNewQuery(ownProps.classId, query))
        },
        fetchAllQueriesIfNeeded: () => {
            return dispatch(fetchQueriesIfNeeded(ownProps.classId))
        },
        showLoginSignupWin: () => {
            const actionObj: LoginSignupWinShowAction = { type: 'LOGIN_SIGNUP_WINDOW_SHOW', isLoginTab: false }
            dispatch(actionObj);
        }
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ClassDetails));