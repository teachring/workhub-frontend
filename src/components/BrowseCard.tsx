import React from "react";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";

import Card from "@material-ui/core/Card";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Typography from "@material-ui/core/Typography";
import { red } from "@material-ui/core/colors";
import FavoriteIcon from "@material-ui/icons/Favorite";
import { Event, Schedule, AttachMoney } from "@material-ui/icons";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import { Grid, Box } from "@material-ui/core";
import Button from "@material-ui/core/Button";


const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    card: {
      display: "flex",
      boxShadow: "none",
      backgroundColor: "transparent",
      flexWrap: "wrap"
    },
    details: {
      display: "flex",
      flexDirection: "column",
      flex: 1,
      minWidth: 390
    },
    content: {
      paddingTop: 0
    },
    cover: {
      minWidth: 333,
      minHeight: 187,
      borderRadius: 6,
      boxShadow:
        "0 5px 15px -8px rgba(0, 0, 0, 0.24), 0 8px 10px -5px rgba(0, 0, 0, 0.2)"
    },
    controls: {
      display: "flex",
      alignItems: "center",
      paddingLeft: theme.spacing(1),
      paddingBottom: theme.spacing(1)
    },
    icontext: {
      display: "flex",
      alignItems: "center",
      justifyContent: "left",
      flex: 1
    },
    btnRegister: {
      background: "linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)",
      borderRadius: 3,
      border: 0,
      color: "white",
      height: 48,
      padding: "0 30px",
      boxShadow: "0 3px 5px 2px rgba(255, 105, 135, .3)"
    }
  })
);

export default function BrowseCard() {
  const classes = useStyles();

  return (
    <Card className={classes.card}>
      <Grid container spacing={4}>
        <Grid item xs={12} sm={6} md={4}>
          <CardMedia
            className={classes.cover}
            image="https://material-ui.com/static/images/cards/paella.jpg"
            title="Live from space album cover"
          />
        </Grid>
        <Grid item xs={12} sm={6} md={8}>
          <CardContent className={classes.content}>
            <Typography variant="h6" align="left">
              React.js workshop
            </Typography>
            <Typography variant="subtitle1" color="textSecondary" align="left">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc a
              euismod metus. Nullam ullamcorper orci eu aliquet aliquet. Mauris
              nec tristique
            </Typography>
          </CardContent>
          <Box display="flex" px={2} py={1}>
            <span className={classes.icontext}>
              <Event />
              <Box pl={2}>10 Oct 8 AM - 12 Oct 5PM</Box>
            </span>
          </Box>
          <Box display="flex" px={2} py={1}>
            <span className={classes.icontext}>
              <AttachMoney /> <Box pl={2}>100 /-</Box>
            </span>
          </Box>
          <Box display="flex" px={2} py={1}>
            <Button variant="contained" className={classes.btnRegister}>
              Register
            </Button>
          </Box>
        </Grid>
      </Grid>
    </Card>
  );
}
