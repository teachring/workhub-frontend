import {getRzrpOrderId } from '../../utils/apis/payments';

declare global {
    interface Window { Razorpay: any}
}

export type RzpIdentifiers = {
    orderId: string,
    paymentId: string,
    signature: string
}

export const initiatePayment = (classId:number,successCallback:(rzpData:RzpIdentifiers) => any) => {
    getRzrpOrderId(classId)
    .then ((orderId:string) => {
            var rzp1 = new window.Razorpay(createRzpOptions(orderId,successCallback));
            rzp1.open();
        },
    err => {
        console.log('Error Occured while fetching orderId');
    })
}

export const mockRzpPayment = () => {
    var rzp1 = new window.Razorpay(createRzpOptions('order_EDqKZuaicuQpnW'));
    rzp1.open();
}

export const fetchRzpOrderId = (classId:number) => {
    return "order_EDqKZuaicuQpnW";
}

const createRzpOptions = (orderId:string,succcessCallback?:(rzpData:RzpIdentifiers) =>any) => {
    return {
        key: 'rzp_test_xstIqWy8nOKK2t',
        order_id: orderId,
        amount: '100', //  = INR 1
        name: 'Teachring',
        description: 'Spider-ReactJS Workshop',
        image: 'https://cdn.razorpay.com/logos/7K3b6d18wHwKzL_medium.png',
        handler: function(response) {
            console.log("Payment Successful")
            alert(response.razorpay_payment_id);
            succcessCallback!({
                orderId: response.razorpay_order_id,
                paymentId: response.razorpay_payment_id,
                signature: response.razorpay_signature
            })
            
        },
        prefill: {
            name: 'Gaurav',
            contact: '9999999999',
            email: 'demo@demo.com'
        },
        notes: {
            address: 'some address'
        },
        theme: {
            color: 'blue',
            hide_topbar: false
        }
    };

}