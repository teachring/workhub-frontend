import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";

const useClasses = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1
        },
        icontext: {
            display: "flex",
            alignItems: "center",
            justifyContent: "left",
            flex: 1,
            marginBottom: 5,
            "& svg": {
                marginRight: 7
            }
        },
        imgContainer: {
            overflow: "hidden",
            maxHeight: "350px"
        },
        cover: {
            color: "#fff",
            display: "flex",
            height: "100%",
            flexDirection: "column",
            justifyContent: "space-around",
            alignItems: "baseline",
            [theme.breakpoints.down("sm")]: {
                marginTop: 25,
            },
            position: "relative",
            overflow: "hidden",
            padding: 50,
            "&:before": {
                top: 0,
                left: 0,
                zIndex: -1,
                content: "''",
                position: "absolute",
                // backgroundImage:
                //     "linear-gradient(rgba(0,0,0,0.65), rgba(0,0,0,0.65)), url(https://cdn.thecollegefever.com/upload/25970844-1577254728583.jpeg)",
                // backgroundRepeat: "no-repeat",
                // backgroundSize: "cover",
                width: "100%",
                height: "100%",
                backgroundImage: "linear-gradient(to right, #4facfe 0%, #00f2fe 100%)",
                // backgroundImage: "linear-gradient(120deg, #f6d365 0%, #fda085 100%)",
                // backgroundImage: "linear-gradient(135deg, #667eea 0%, #764ba2 100%)",
                // backgroundImage: "linear-gradient(to right, #ff758c 0%, #ff7eb3 100%)",
                // backgroundImage: "linear-gradient(to top, #4481eb 0%, #04befe 100%)"
                // backgroundImage: "linear-gradient(-225deg, #5271C4 0%, #B19FFF 48%, #ECA1FE 100%)",
                // backgroundImage: "linear-gradient(-225deg, #65379B 0%, #886AEA 53%, #6457C6 100%)"
            }
        },
        coverIcon: {
            width: 70,
            height: 70
        },
        formfield: {
            "&>.MuiBox-root": {
                margin: "3rem auto 0 auto",
                maxWidth: 700
            },
            "& input": {
                color: "#717a86"
            },
            "& .MuiInputBase-root": {
                borderRadius: 0
            }
        },
        organizer: {
            fontSize: "1rem"
        },
        title: {
            fontWeight: 900,
            textTransform: "uppercase",
            textAlign: "left",
            fontSize: "2rem",
            position: "relative",
            letterSpacing: 3,
            marginBottom: 30,
            "&::after": {
                content: "''",
                position: "absolute",
                width: 50,
                height: 3,
                background: "#fff",
                bottom: -10,
                left: 2
            },
        },
        bannerBtn: {
            border: "1px solid #fff",
            background: "#fff",
            borderRadius: 3,
            width: 150,
            height: 60,
            color: "#6b4b4b",
            fontWeight: 400,
            letterSpacing: 3,
            marginTop: 30,
            "&:hover": {
                background: "#fff",
                color: "#4facfe"
            }
        },
        body: {

        },
        payBtn: {
            width: 200,
            padding: "1rem"
        },
        tnc: {
            maxWidth: 700,
            boxSizing: "border-box",
            margin: "auto",
            marginBottom: "2rem",
            padding: "2rem",
            backgroundColor: theme.palette.primary.light,
            textAlign: "left",
            color: theme.palette.primary.dark
        },
        [theme.breakpoints.down("sm")]: {
            btnRegister: {
                marginRight: 10
            },
            cover: {
                "&>*": {
                    marginBottom: 10
                }
            }
        },
        [theme.breakpoints.down("xs")]: {
            root: {
                padding: "30px 0 0 0"
            },
            formlabel: {
                paddingTop: "30px !important",
                "&>p": {
                    lineHeight: "1 !important"
                }
            }
        }
    })
);

export default useClasses;