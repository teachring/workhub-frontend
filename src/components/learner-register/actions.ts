import { IntKeyDict } from "../../utils/core-types/misc"
import { RegFormQuestion, RegInfo } from "../../utils/core-types/learner-reg"
import { registerByPaying,registerWithoutPaying } from '../../utils/apis/learners';
import { RzpIdentifiers } from "./rzp-helper";
 
export type LearnerRegisterAction = {
    type: 'LEARNER_REGISTER',
    classId: number,
    regInfo:RegInfo
}

//Now you need to send me the payment date-time into my systsem

export const newRegisterWithPay = (classId:number,rzpData:RzpIdentifiers, regFormData?: IntKeyDict<RegFormQuestion>) => {
    return function(dispatch:(arg0:LearnerRegisterAction) => any) {
        return registerByPaying(classId,rzpData,regFormData)
        .then(resObj => {
            if(resObj.paymentStatus) {
                dispatch({
                    type: 'LEARNER_REGISTER',
                    classId: classId,
                    regInfo: {
                        regDateTime: resObj.regDateTime!,
                        regFormData: regFormData!,
                        paymentDetails: {
                            isPaid: true,
                            paymentDateTime: resObj.regDateTime,
                            modeSelected: 'ONLINE'
                        }
                    }})
            }
            
            return resObj.paymentStatus
            
        })
    }

}

export const newRegisterWithoutPay = (classId:number, isPaid:boolean, regFormData?: IntKeyDict<RegFormQuestion>) => {
    return function(dispatch:(arg0:LearnerRegisterAction) => any) {

        return registerWithoutPaying(classId,regFormData)
        .then(
            regDateTime => {
                
                const regInfoObj:RegInfo = {
                    regDateTime: regDateTime,
                    regFormData: regFormData!,
                } 
                if(isPaid) {
                    regInfoObj.paymentDetails = {
                        isPaid: false,
                        modeSelected: 'OFFLINE'
                    }
                }

                dispatch({
                    type: 'LEARNER_REGISTER',
                    classId: classId,
                    regInfo: regInfoObj
                })
                return true
            },
            err => {
                return false
            }
        )
    }
}