import React from "react";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import { AttachMoney, Event, Room } from "@material-ui/icons";
import { Typography, FormGroup } from "@material-ui/core";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import clsx from "clsx";
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import Checkbox from '@material-ui/core/Checkbox';
import TextField from "@material-ui/core/TextField";
import useClasses from './form-question-styles';

import { RegFormQuestion, RegFormQuesType, RegFormQuesOption } from "../../../../utils/core-types/learner-reg";

type IProps = {
    question?: RegFormQuestion,
    handleQuestion: React.Dispatch<any>
};

const Question: React.FC<IProps> = ({ question, handleQuestion }) => {

    const classes = useClasses();

    return (
        question
            ? <div className={classes.root}>
                <Box margin="30" width="100%">
                    <Typography variant="h6" className={classes.question}>{question.question}</Typography>
                    {
                        question.type === RegFormQuesType.RADIO && (
                            <FormControl
                                className={classes.answer}
                                component="fieldset"
                                required={question.required}
                                error={question.required && question.selectedOption == ""}
                            >
                                <RadioGroup
                                    value={question.selectedOption}
                                    onChange={e => handleQuestion({
                                        type: "select-option-radio",
                                        payload: {
                                            qid: question.qid,
                                            option: e.target.value
                                        }
                                    })}
                                >
                                    {
                                        question.options?.map((option: RegFormQuesOption, index: number) => (
                                            <FormControlLabel
                                                key={index}
                                                value={option.text}
                                                control={<Radio />}
                                                label={option.text} />
                                        ))
                                    }
                                </RadioGroup>
                                <FormHelperText>{question.required ? "required" : ""}</FormHelperText>
                            </FormControl>
                        )
                    }

                    {
                        question.type === RegFormQuesType.CHECKBOX && (
                            <FormControl
                                className={classes.answer}
                                component="fieldset"
                                required={question.required}
                                error={question.required && question.options?.filter(o => o.selected).length == 0}
                            >
                                <FormGroup>
                                    {
                                        question.options?.map((option: RegFormQuesOption, index: number) => (
                                            <FormControlLabel
                                                key={index}
                                                control={
                                                    <Checkbox
                                                        value={option.text}
                                                        checked={option.selected}
                                                        onChange={() => handleQuestion({
                                                            type: "select-option-checkbox",
                                                            payload: {
                                                                qid: question.qid,
                                                                optionIndex: index
                                                            }
                                                        })}
                                                    />
                                                }
                                                label={option.text}
                                            />
                                        ))
                                    }
                                </FormGroup>
                                <FormHelperText>{question.required ? "required" : ""}</FormHelperText>
                            </FormControl>
                        )

                    }

                    {
                        question.type === RegFormQuesType.TEXT && (
                            <TextField
                                className={classes.answer}
                                label="Answer"
                                variant="outlined"
                                helperText={question.required ? "required" : ""}
                                value={question.answer || ""}
                                error={question.required && question.answer?.length == 0}
                                onChange={e => handleQuestion({
                                    type: "edit-answer",
                                    payload: {
                                        qid: question.qid,
                                        value: e.target.value
                                    }
                                })}
                            />

                        )
                    }
                </Box>

            </div>
            : <div />
    );
}
export default Question;