import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";

const useClasses = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            padding: 40,
            width: "auto",
            textAlign: "left",
            display: "flex",
            borderRadius: 5,
        },
        question: {
            marginBottom: 20
        },
        answer: {
            marginLeft: 20,
            marginRight: 20,
            width: "calc(100% - 40px)"
        },
        [theme.breakpoints.down("sm")]: {
            root: {
                margin: "0 !important",
                border: "none"
            },
            answer: {
                margin: 0,
                width: "100%"
            }
        }
    })
);

export default useClasses;