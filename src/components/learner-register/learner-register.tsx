import React from "react";
import { Paper, Grid, Box, Typography, Button } from "@material-ui/core";
import { AttachMoney, Event, Room } from "@material-ui/icons";
import IconButton from "@material-ui/core/IconButton";
import clsx from "clsx";
import useClasses from './learner-register-styles';

import { FormQuestionComp } from "./components/form-questions";
import { ClassInfo } from "../../utils/core-types/class-types";
import { IntKeyDict } from "../../utils/core-types/misc";
import { RegFormQuestion, RegFormQuesType } from "../../utils/core-types/learner-reg";
import useScript from "../shared/hooks/useScripts";
import { initiatePayment, RzpIdentifiers, mockRzpPayment } from "./rzp-helper";
import TopBanner from "../class-details/components/top-banner/top-banner";
import Heading from "../shared/heading/heading";
import { AlertMessageType } from "../alert-snackbar/alert-snackbar-actions";

const formReducer = (state: IntKeyDict<RegFormQuestion> | undefined, action: any) => {

    if (action.type === "edit-answer") {
        let newState = { ...state };
        newState[action.payload.qid] = { ...newState[action.payload.qid], answer: action.payload.value }
        return newState;

    } else if (action.type === "select-option-radio") {

        let newState = { ...state };
        newState[action.payload.qid] = { ...newState[action.payload.qid], selectedOption: action.payload.option }
        return newState;

    } else if (action.type === "select-option-checkbox") {

        let newState = { ...state };
        let ques = newState[action.payload.qid];
        let option = ques.options![action.payload.optionIndex];
        option.selected = !option.selected;
        return newState;
    }

    return state;
}

const validate = (state: IntKeyDict<RegFormQuestion> | undefined) => {
    let hasError = false;

    state && Object.values(state).some((question: RegFormQuestion) => {
        if (question.required) {

            if (question.type === RegFormQuesType.TEXT) {

                if (!question.answer || question.answer.trim().length === 0)
                    hasError = true;

            } else if (question.type === RegFormQuesType.RADIO) {

                if (!question.selectedOption || question.selectedOption.trim().length === 0)
                    hasError = true;

            } else if (question.type === RegFormQuesType.CHECKBOX) {

                if (!question.options || question.options.filter(o => o.selected).length === 0)
                    hasError = true;
            }

        }
        return hasError;    // break if at least one error is found
    });

    return !hasError;   // return true if form data is validated successfully
}

type IProp = {
    classInfo: ClassInfo,
    registerAndPay: (classId: number, rzpData: RzpIdentifiers, regFormData?: IntKeyDict<RegFormQuestion>) => Promise<boolean>,
    registerAndNotPay: (classId: number, isPaid: boolean, regFormData?: IntKeyDict<RegFormQuestion>) => Promise<boolean>,
    publishAlerts: (type: AlertMessageType, message: string) => void
}

const LearnerRegister: React.FC<IProp> = ({ classInfo, registerAndPay, registerAndNotPay, publishAlerts }) => {
    const classes = useClasses();
    useScript('https://checkout.razorpay.com/v1/checkout.js');

    const [form, handleForm] = React.useReducer(formReducer, classInfo.regFormData);

    const onSuccessfulPayment = (rzpData: RzpIdentifiers) => {
        registerAndPay(classInfo.classId, rzpData, form);
    }

    const handleRegisterWithoutPay = () => {
        if (validate(form)) {
            registerAndNotPay(classInfo.classId, classInfo.isPaid, form)
                .then(() => {
                    publishAlerts(AlertMessageType.SUCCESS_MESSAGE, "You have been registered successfully!");
                    // TODO: redirect to appropriate page
                })
                .catch(() => {
                    publishAlerts(AlertMessageType.ERROR_MESSAGE, "There was an error in registration. Please try again.")
                })
        } else {
            publishAlerts(AlertMessageType.ERROR_MESSAGE, "Please fill all the required details properly")
        }
    }

    return (
        <div className={classes.root}>
            <TopBanner
                isSignedIn={true}
                classInfo={classInfo}
                isRegistered={false}
                showActionBtn={false} />

            <Grid container spacing={0} className={classes.body}>
                <Grid className={classes.formfield} item xs={12}>
                    <Box>
                        <Heading>Registration Form</Heading>
                        {
                            form != undefined ? Object.keys(form).map((key, idx) => (
                                <FormQuestionComp key={idx} question={form[key]} handleQuestion={handleForm} />
                            )) : null
                        }
                    </Box>
                </Grid>

                <Grid item xs={12}>
                    <Box className={classes.tnc}>
                        <Typography color="inherit">Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae ipsum ea deserunt, porro commodi sapiente dolorem dolore debitis placeat quis!</Typography>
                        <ul>
                            <li><Typography color="inherit">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Magni, nulla?</Typography></li>
                            <li><Typography color="inherit">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Magni, nulla?</Typography></li>
                            <li><Typography color="inherit">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Magni, nulla?</Typography></li>
                            <li><Typography color="inherit">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Magni, nulla?</Typography></li>
                        </ul>
                    </Box>
                </Grid>

                <Grid item xs>
                    <Box pb={4}>
                        {
                            !classInfo.isPaid && (
                                <Button className={classes.payBtn} variant="contained" color="primary" onClick={handleRegisterWithoutPay}>
                                    Register for free
                                </Button>
                            )
                        }
                        <Box display="inline" mr={2}>
                            {
                                classInfo.isPaid && classInfo.paymentDetails!.offlinePayAvl && (
                                    <Button className={classes.payBtn} variant="outlined" color="primary" onClick={handleRegisterWithoutPay}>
                                        Pay later by cash
                                    </Button>
                                )
                            }
                        </Box>
                        {
                            classInfo.isPaid && (
                                <Button className={classes.payBtn} variant="contained" color="primary" onClick={() => initiatePayment(classInfo.classId, onSuccessfulPayment)}>
                                    Pay online
                                </Button>
                            )
                        }

                    </Box>
                </Grid>
            </Grid>
        </div>
    );
}

export default LearnerRegister;