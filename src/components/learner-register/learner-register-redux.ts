import { connect } from 'react-redux';
import LearnerRegister from './learner-register';
import { RootState } from '../../utils/reduxReducers';
import { newRegisterWithPay, newRegisterWithoutPay } from './actions';
import { RegFormQuestion } from '../../utils/core-types/learner-reg';
import { IntKeyDict } from '../../utils/core-types/misc';
import { RzpIdentifiers } from './rzp-helper';
import { AlertMessageType, dispatchAlertAction } from '../alert-snackbar/alert-snackbar-actions';
//here I have a few action creators and they are used, register this user for t 



const mapStateToProps = (state: RootState, ownProps: any) => {
    return {
        classInfo: state.ClassInfoList[ownProps.classId]
    }
}

const mapDispatchToProps = (dispatch: Function, ownProps: any) => {
    return {
        registerAndPay: (classId: number, rzpData: RzpIdentifiers, regFormData?: IntKeyDict<RegFormQuestion>) => {
            return dispatch(newRegisterWithPay(classId, rzpData, regFormData))
        },
        registerAndNotPay: (classId: number, isPaid: boolean, regFormData?: IntKeyDict<RegFormQuestion>) => {
            return dispatch(newRegisterWithoutPay(classId, isPaid, regFormData))
        },
        publishAlerts: (type: AlertMessageType, message: string) => {
            dispatchAlertAction(dispatch, type, message);
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LearnerRegister);