import React, { useEffect } from "react";

import { Button, IconButton, TextField, Box, Typography, Tabs, Tab, AppBar, Input, InputLabel, Modal } from "@material-ui/core";
import { Close, ChevronLeft } from "@material-ui/icons";
import { setUpReCaptchaVerifier, initiateFirebaseSignIn, submitSignInOtp } from "./firebaseutil";

import { getServerTokenOnLogin, getServerTokenOnSignup, verifyIfAccountExists } from '../../utils/apis/login-signup';

import StyleClasses from "./login-signup-styles";
import { UserFormComp, SignupInput } from "./components/user-form";
import { SubmitOtpComp } from "./components/submit-otp";
import { AlertMessageType } from "../alert-snackbar/alert-snackbar-actions";


interface IProp {
    isOpen: boolean,
    shouldOpenLogin: boolean
    onCloseCallback: () => void,
    onSuccessfulSignIn: () => void,
    publishAlerts: (type: AlertMessageType, message: string) => void
}


type SignInDataObj = {
    name?: string,
    email?: string,
    phoneNumber: string
}


const LoginSignup: React.FC<IProp> = ({ isOpen, onCloseCallback, onSuccessfulSignIn, publishAlerts, shouldOpenLogin = false }) => {
    const classes = StyleClasses();
    //  const [open, setOpen] = React.useState(true);
    const [otpPageView, setOtpPageView] = React.useState(false);
    const [loading, setLoading] = React.useState(false);
    const [signInData, setSignInData] = React.useState<SignInDataObj>({ phoneNumber: '' })

    const handleLoginRequest = async (phoneNo: string) => {
        setSignInData({ phoneNumber: phoneNo })
        console.log("Login Request Accepted");
        setLoading(true)
        try {
            let isRegistered = await verifyIfAccountExists(phoneNo);
            if (isRegistered) {
                await initiateFirebaseSignIn(phoneNo);
                //setLoading(false);
                onOtpSent();
            } else {
                // tell user to signup first.
                publishAlerts(AlertMessageType.ERROR_MESSAGE, "This number is not registered, Please sign up to register")
            }

        } catch (error) {
            publishAlerts(AlertMessageType.ERROR_MESSAGE, "Please enter a valid number");
        } finally {
            setLoading(false)
        }
    }

    const handleSignupRequest = async (signupInput: SignupInput) => {
        setSignInData({ name: signupInput.name, email: signupInput.email, phoneNumber: signupInput.phoneNo });
        setLoading(true);
        try {
            let isRegistered = await verifyIfAccountExists(signupInput.phoneNo);
            if (isRegistered) {
                setLoading(false);
                publishAlerts(AlertMessageType.INFO_MESSAGE, "Phone number already registered, Please Login directly");
                //show the snackbar containing the message.. and clear up the control which tab to 
            } else {
                // begin the signup process and then.. I need to set the loading state to be true or false 
                await initiateFirebaseSignIn(signupInput.phoneNo);
                setLoading(false);
                onOtpSent();
            }
        } catch (error) {
            setLoading(false);
            publishAlerts(AlertMessageType.ERROR_MESSAGE, "Something went wrong, please try again. Make sure to enter valid Phone Number.")
        }
    }

    const onOtpSent = () => {
        setOtpPageView(true)
    }

    const handleOtpResend = async () => {
        setLoading(true);
        try {
            await initiateFirebaseSignIn(signInData.phoneNumber);

        } catch (error) {

        } finally {
            setLoading(false)
        }
    }


    const handleOtpSubmit = async (otpString: string) => {
        console.log("Handle Otp Submit called ", otpString);

        setLoading(true);
        try {
            let result: any = await submitSignInOtp(otpString)
            let firebaseIdToken: string = await result.user.getIdToken(true)
            //let firebaseIdToken = "dummyToken";
            console.log("TOKEN ", firebaseIdToken)

            if (signInData.name && signInData.email) {
                await getServerTokenOnSignup({ idToken: firebaseIdToken, name: signInData.name, email: signInData.email, phoneNumber: signInData.phoneNumber })
            } else {
                await getServerTokenOnLogin({ idToken: firebaseIdToken })
            }
            setLoading(false);
            //setOpen(false);
            onSuccessfulSignIn();
            onCloseCallback();

        } catch (error) {
            //TODO add alert publish and handlers. otp wrong case.
        }

    }

    const handleClose = (e: React.MouseEvent) => {
        console.log("MODAL onclose called")
        // setSignInData({phoneNumber:''})
        // setOtpPageView(false);
        // setLoading(false);
        onCloseCallback()
        //setOpen(false);
    };

    const hideOtpPage = () => {
        setOtpPageView(false);
    };

    useEffect(() => {
        //signup-getotp-btn
        setUpReCaptchaVerifier('signup-getotp-btn');
    }, []);


    return (
        <div onClick={(e) => { e.stopPropagation() }}>
            <div id="signup-getotp-btn" />
            <Modal
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
                disableBackdropClick
                open={isOpen}
                onClose={handleClose}
            >
                <div className={classes.paper}>

                    {/* close button and back button */}
                    <Box display="flex"> {
                        otpPageView && <IconButton aria-label="back" onClick={hideOtpPage} disabled={loading}>
                            <ChevronLeft />
                        </IconButton>}
                        <IconButton
                            className={classes.modalClose}
                            aria-label="close"
                            onClick={handleClose}
                            disabled={loading}>
                            <Close />
                        </IconButton>
                    </Box>

                    {otpPageView == false ? (
                        <UserFormComp loading={loading} onLoginRequest={handleLoginRequest} onSignupRequest={handleSignupRequest} shouldOpenLogin={shouldOpenLogin} />
                    ) : (
                            <SubmitOtpComp loading={loading} onResend={handleOtpResend} onSubmit={handleOtpSubmit} />
                        )}
                </div>
            </Modal>
        </div>
    );
}

export default LoginSignup;
