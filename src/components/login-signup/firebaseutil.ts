import * as firebase from "firebase/app";
import "firebase/auth";
import MockHttp from "../../utils/mock-http";

declare global {
  interface Window { reCaptchaVerifier: any; confirmationResult: any}
}

export const setUpReCaptchaVerifier = (elementId:string) => {
    window.reCaptchaVerifier = new firebase.auth.RecaptchaVerifier(elementId, {
        'size': 'invisible',
        'callback': function(response:any) {
          // reCAPTCHA solved, allow signInWithPhoneNumber.
          // console.log(response);
          // console.log("Hello inside callback");
        }
      });
      // window.reCaptchaVerifier.render().then(function (widgetId:any) {
      //   //window.recaptchaWidgetId = widgetId;
      // });
}

export function submitSignInOtp(otpCode:string){
    return window.confirmationResult.confirm(otpCode);
}


export async function initiateFirebaseSignIn(phoneNumber: string) {

  firebase.auth().useDeviceLanguage();  
  try {
      window.confirmationResult = await firebase.auth().signInWithPhoneNumber(phoneNumber, window.reCaptchaVerifier);
      return 
  } catch(error) {
    throw error
  }
  console.log("Service - FirebaseSignIn called ",phoneNumber)
  //return MockHttp(true,{},2000);
  
}