import { makeStyles, Theme, useTheme, createStyles } from "@material-ui/core/styles";

const useClasses = makeStyles((theme: Theme) =>
    createStyles({
        paper: {
            position: "absolute",
            backgroundColor: theme.palette.background.paper,
            boxShadow: "0 1px 4px 0 rgba(0, 0, 0, 0.14)",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
            borderRadius: 5,
            outline: "none",
            padding: 12,
            width: 400,
            height: 500,
            maxWidth: "90vw",
            maxHeight: "90vh",
            display: "flex",
            flexDirection: "column"
        },
        tabs: {
            background: "transparent",
            boxShadow: "none",

        },
        inputField: {
            width: "100%",
            margin: theme.spacing(2, 0, 2, 0)
        },
        loginBtn: {
            margin: "auto"
        },
        modalClose: {
            marginLeft: "auto",
            padding: theme.spacing(1)
        },
        getOtpBtn: {
            width: "auto",
            display: "flex",
            marginTop: theme.spacing(2),
            marginBottom: theme.spacing(2)
        }
    })
);

export default useClasses;
