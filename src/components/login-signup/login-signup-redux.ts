import React from "react";
//@ts-ignore
import Cookies from 'js-cookie';
import { connect } from 'react-redux';
import {default as LoginSignupComp } from './login-signup';
import { AlertMessageType,dispatchAlertAction } from "../alert-snackbar/alert-snackbar-actions";
import {signInSuccess, LoginSignupWinCloseAction} from './actions';
import { RootState } from "../../utils/reduxReducers";

const mapDispatchToProps = (dispatch:Function,ownProps:any) => {
    return {
        onSuccessfulSignIn : () => {
            dispatch(signInSuccess())
        },
        onCloseCallback : () => {
            const actionObj: LoginSignupWinCloseAction = {type:'LOGIN_SIGNUP_WINDOW_CLOSE'}
            dispatch(actionObj);
        },
        publishAlerts : (type:AlertMessageType,message:string) => {
            dispatchAlertAction(dispatch,type,message);
        },
       
    }
}

const mapStateToProps = (state:RootState) => {
    return {
        isOpen : state.LoginSignupWindow.isOpen,
        shouldOpenLogin : state.LoginSignupWindow.isLoginTab
    }
}


export default connect(mapStateToProps,mapDispatchToProps)(LoginSignupComp);