import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";

const useClasses = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
            display: "flex",
            flexDirection: "column"

        },
        tabs: {
            background: "transparent",
            boxShadow: "none",
            padding: "0 24px"
        },

        tab: {
            fontFamily: "Roboto Slab",
            fontSize: "1.2rem",
            padding: "1rem",
        },

        tabPanel: {
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            paddingTop: theme.spacing(2),
            paddingBottom: theme.spacing(2),
            paddingRight: theme.spacing(3),
            paddingLeft: theme.spacing(3),
            flexGrow: 1,
            justifyContent: "flex-start",
            width: "100%",
            boxSizing: "border-box"
        },

        inputField: {
            width: "100%",
            margin: theme.spacing(2, 0, 2, 0)
        },

        linearProgress: {
            marginBottom: theme.spacing(1),
            marginTop: theme.spacing(1),
            width: "inherit"

        }
    })
);

export default useClasses;