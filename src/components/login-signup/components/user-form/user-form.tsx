import React, { useEffect, useState } from "react";
import { Box, Button, Input, InputLabel, FormControl, TextField, AppBar, Tabs, Tab, CircularProgress, LinearProgress } from "@material-ui/core";
import { useForm, FieldError } from "react-hook-form";

import TabPanel from "./tab-panel";
import useStyleClasses from "./user-form-styles";


export type SignupInput = {
    phoneNo: string,
    name: string,
    email: string
}

type IProp = {
    shouldOpenLogin: boolean
    onLoginRequest: (phoneNo: string) => void
    onSignupRequest: (signupInput: SignupInput) => void
    loading: boolean
}

function a11yProps(index: any) {
    return {
        id: `full-width-tab-${index}`,
        "aria-controls": `full-width-tabpanel-${index}`
    };
}

const UserForm: React.FC<IProp> = ({ onLoginRequest, onSignupRequest, loading, shouldOpenLogin }) => {

    const classes = useStyleClasses();

    const { register, handleSubmit, errors } = useForm();

    const [currentTab, setCurrentTab] = React.useState(shouldOpenLogin ? 1 : 0);  // 0 for signup

    const handleTabChange = (event: React.ChangeEvent<{}>, newTabValue: number) => {
        event.stopPropagation();
        if (!loading) {
            setCurrentTab(newTabValue);
        }
    };

    useEffect(() => {
        setCurrentTab(shouldOpenLogin ? 1 : 0)
    }, [shouldOpenLogin])


    const requestOtp = (data: any) => {
        if (currentTab == 0) {
            onSignupRequest({ name: data.signupName, email: data.signupEmail, phoneNo: '+91-' + data.signupMobile })
        } else {
            onLoginRequest('+91-' + data.loginMobile)
        }
    }

    return (
        <Box className={classes.root}>
            <AppBar className={classes.tabs} position="static" color="default">
                <Tabs
                    value={currentTab}
                    onChange={handleTabChange}
                    indicatorColor="primary"
                    textColor="primary"
                    centered
                    variant="fullWidth"
                    aria-label="login signup tabs"
                >
                    <Tab label="Signup" className={classes.tab} {...a11yProps(1)} selected />
                    <Tab label="Login" className={classes.tab} {...a11yProps(0)} />
                </Tabs>
            </AppBar>

            <TabPanel value={currentTab} index={0} className={classes.tabPanel}>
                <form onSubmit={handleSubmit(requestOtp)}>
                    <Box textAlign="center">
                        <TextField required className={classes.inputField} label="Name" id='signup-name'
                            helperText={!errors.signupName ? "Enter your full name" : "Name is required"}
                            inputRef={register({ required: true })} name="signupName" variant="outlined"
                            error={errors.signupName != undefined} autoFocus />

                        <TextField required className={classes.inputField} label="Mobile No." id='signup-mobile'
                            helperText={!errors.signupMobile ? "Enter your 10 digit mobile no." : 'Enter valid 10 digit mobile no'}
                            inputRef={register({ required: true, maxLength: { value: 10, message: 'errors' }, minLength: { value: 10, message: 'errors' } })} name="signupMobile" variant="outlined"
                            error={errors.signupMobile != undefined}
                        />

                        <TextField required className={classes.inputField} label="Email"
                            helperText={!errors.signupEmail ? null : (errors.signupEmail as FieldError).message ?? 'Email is required'}
                            id='signup-email' inputRef={register({
                                required: true, pattern: {
                                    value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                                    message: "invalid email address"
                                }
                            })}
                            name="signupEmail" variant="outlined"
                            error={errors.signupEmail != undefined} />

                        {loading && <Box className={classes.linearProgress}><LinearProgress color="primary" /></Box>}

                        <Button
                            variant="contained"
                            color="primary"
                            disabled={loading}
                            type="submit"
                        >
                            get otp
                        </Button>
                    </Box>
                </form>
            </TabPanel>

            <TabPanel value={currentTab} index={1} className={classes.tabPanel}>
                <form onSubmit={handleSubmit(requestOtp)}>
                    <Box textAlign="center">
                        <TextField required className={classes.inputField} label="Mobile No." id='login-mobile' inputRef={register({ required: true, maxLength: { value: 10, message: 'errors' }, minLength: { value: 10, message: 'errors' } })} name="loginMobile" variant="outlined"
                            helperText={!errors.loginMobile ? "Enter your 10 digit mobile no." : 'Enter valid 10 digit mobile no'}
                            error={errors.loginMobile != undefined} autoFocus />

                        {loading && <Box className={classes.linearProgress}><LinearProgress color="primary" /></Box>}

                        <Button
                            variant="contained"
                            color="primary"
                            disabled={loading}
                            type="submit"
                        >
                            get otp
                        </Button>
                    </Box>
                </form>
            </TabPanel>
        </Box>

    );
}

export default UserForm;