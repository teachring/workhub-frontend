import {Typography,Box} from "@material-ui/core";
import React, {useEffect,useState} from "react";

interface TabPanelProps {
    children?: React.ReactNode;
    dir?: string;
    index: any;
    value: any;
    className?:any
}
  
const TabPanel:React.FC<TabPanelProps> = (props: TabPanelProps) => {

    const { children, value, index, ...other } = props;

    return (
        (value == index) ? <Typography
        component="div"
        role="tabpanel"
        hidden={value !== index}
        id={`full-width-tabpanel-${index}`}
        aria-labelledby={`full-width-tab-${index}`}
        {...other}
        >
        {children}
        </Typography> : null
    );
}

export default TabPanel;