import {makeStyles,createStyles,Theme} from "@material-ui/core/styles";

const useClasses = makeStyles((theme: Theme) =>
createStyles({

  inputField: {
    width: "100%",
    margin: theme.spacing(2, 0, 2, 0)
  },

  root: {
    flexGrow: 1,
    display: "flex",
    flexDirection: "column"
  },

  linearProgress: {
    marginBottom: theme.spacing(1),
    marginTop:theme.spacing(1),
    width: "inherit"

}
})
);

export default useClasses;