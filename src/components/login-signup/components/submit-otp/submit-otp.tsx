import React, { useEffect, useState } from "react";
import { Box, Button, Input, InputLabel, FormControl, TextField, CircularProgress, LinearProgress } from "@material-ui/core";
import { useForm } from "react-hook-form";

import useStyleClasses from "./submit-otp-styles";


type IProp = {
    onSubmit: (otpString: string) => void,
    onResend: () => void,
    loading: boolean
}

const SubmitOtp: React.FC<IProp> = ({ onSubmit, onResend, loading }) => {

    const classes = useStyleClasses();

    const { register, handleSubmit, errors } = useForm();

    // Now once you submit it then it goes there..

    const onSubmitClicked = (data: any) => {
        console.log("On submit clicked")
        onSubmit(data.otpInput);
    }

    return (
        <Box className={classes.root}>
            <form onSubmit={handleSubmit(onSubmitClicked)}>
                <Box px={4} py={2} textAlign="center">

                    <TextField required className={classes.inputField} label="OTP" id='signup-mobile'
                        helperText={!errors.otpInput ? "Enter the OTP received on your phone" : 'Please enter the OTP'}
                        inputRef={register({ required: true })} name="otpInput" variant="outlined"
                        error={errors.otpInput != undefined} autoFocus />

                    {loading && <Box className={classes.linearProgress}><LinearProgress color="primary" /></Box>}
                </Box>

                <Box display="flex" my={2} justifyContent="space-around" textAlign="center">
                    <Button variant="outlined" color="primary" disabled={loading} onClick={(e) => onResend()}>
                        resend
                    </Button>
                    <Button variant="contained" color="primary" disabled={loading} type="submit">
                        submit
                    </Button>
                </Box>
            </form>
        </Box>
    );
}

export default SubmitOtp;