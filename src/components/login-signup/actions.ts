//We need to realise that someone has \

import {AppInvalidateAction} from '../App/actions';
import AppInitializer from '../App/actions';
import { RootState } from '../../utils/reduxReducers';

export type LoginSignupWinShowAction = {
    type: 'LOGIN_SIGNUP_WINDOW_SHOW',
    isLoginTab:boolean,
    onSuccessAction?: () => void
}

//Ye aaya then I forward it to them that

export type LoginSignupWinCloseAction = {
    type: 'LOGIN_SIGNUP_WINDOW_CLOSE'
}


export const signInSuccess = () => {
    return function(dispatch:(arg0:AppInvalidateAction|any)=> any,getState:() => RootState) {
        dispatch({type:'INVALIDATE_APP'})
        getState().LoginSignupWindow.onSuccessCallback?.();
    }
}

//do I need to 