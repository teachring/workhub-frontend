import React, { useEffect } from 'react'
import { Box, useTheme } from '@material-ui/core'

import { ClassInfo } from '../../utils/core-types/class-types'
import { SavedDraftInfo } from '../../utils/core-types/educator-types'
import AddCard from '../shared/cards/AddCard'
import ClassCardSmall from '../shared/cards/ClassCardSmall'
import Heading from '../shared/heading/heading'
import { useHistory } from 'react-router-dom'
import { buildClassUrl } from '../../utils/helpers'


type IProp = {
    activeClassList?: ClassInfo[],
    pastClassList?: ClassInfo[],
    draftsList?: SavedDraftInfo[],
    fetchClassesIfNeeded: () => void
}

const EducatorHome: React.FC<IProp> = ({ activeClassList, pastClassList, draftsList, fetchClassesIfNeeded }) => {

    const theme = useTheme();

    useEffect(() => {
        fetchClassesIfNeeded()
    }, [])

    const history = useHistory();

    const navigateToCreateClassPage = (draftId?: number) => {
        if (draftId) {
            history.push('/educator/create-class', {
                draftId: draftId
            });
        } else {
            history.push('/educator/create-class')
        }
    }

    const navigateToEducatorClassroom = (classObj: ClassInfo) => {
        const classpath = buildClassUrl(classObj.organizerName, classObj.title, classObj.classId);
        history.push('/educator/classroom/' + classpath)
    }

    return (
        <Box bgcolor={theme.palette.background.default} minHeight="100vh">
            <Heading ml="2rem">Active workshops</Heading>
            <Box display="flex" flexWrap="wrap">
                <AddCard text="Create new class" onActionFired={() => navigateToCreateClassPage()} />
                {
                    draftsList?.map((draft: SavedDraftInfo) => (
                        <ClassCardSmall classObj={draft} type="DRAFT" key={draft.draftId}
                            onActionFired={() => {
                                navigateToCreateClassPage(draft.draftId);
                            }} />
                    ))
                }
                {
                    activeClassList?.map((classObj: ClassInfo) => (
                        <ClassCardSmall classObj={classObj} type="ACTIVE" key={classObj.classId}
                            onActionFired={() => {
                                navigateToEducatorClassroom(classObj);
                            }} />
                    ))
                }
                {
                    pastClassList?.map((classObj: ClassInfo) => (
                        <ClassCardSmall classObj={classObj} type="PAST" key={classObj.classId}
                            onActionFired={() => {
                                navigateToEducatorClassroom(classObj);
                            }} />
                    ))
                }
            </Box>
        </Box>
    )
}

export default EducatorHome;
