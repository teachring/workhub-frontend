import { ClassInfo } from "../../utils/core-types/class-types"
import { fetchEducatorClasses } from '../../utils/apis/educators';
import { RootState } from "../../utils/reduxReducers";
import { SavedDraftInfo } from "../../utils/core-types/educator-types";

export type EducatorRecvClassAction = {
    type:'EDUCATOR_RECEIVE_CLASS',
    classList: ClassInfo[]
}

export type EducatorRecvDraftAction = {
    type:'EDUCATOR_RECEIVE_DRAFT',
    draftList: SavedDraftInfo[]
}

export const fetchEducatorClassesIfNeeded = () => {
    return async function(dispatch:(arg0:EducatorRecvClassAction| EducatorRecvDraftAction) => any,getState:()=> RootState) {
        if(!(getState().EducatorState.ClassIdList) || !(getState().EducatorState.DraftList)){
            const resObj = await fetchEducatorClasses();
            dispatch({type:'EDUCATOR_RECEIVE_CLASS',classList:resObj.classes})
            dispatch({type: 'EDUCATOR_RECEIVE_DRAFT',draftList:resObj.drafts})

        }
        return ;
    }

    //whether it is fetched or not

}