import { connect } from 'react-redux';
import { RootState } from '../../utils/reduxReducers';
import EducatorHome from './educator-home';
import { SavedDraftInfo } from '../../utils/core-types/educator-types';
import { ClassInfo } from '../../utils/core-types/class-types';
import { fetchEducatorClassesIfNeeded } from './actions';

const mapStateToProps = (state: RootState) => {

    const draftsInStore = state.EducatorState.DraftList;
    const classesInStore = state.EducatorState.ClassIdList;

    var draftsList: SavedDraftInfo[] | undefined = undefined;
    if (draftsInStore) {
        draftsList = Object.keys(draftsInStore).map(draftId => draftsInStore[draftId])
    }

    var activeClassList: ClassInfo[] | undefined = undefined;
    var pastClassList: ClassInfo[] | undefined = undefined;

    if (classesInStore) {
        activeClassList = [];
        pastClassList = [];
        classesInStore.forEach(classId => {
            const classObj = state.ClassInfoList[classId];
            if(classObj.endDateTime > new Date()){
                activeClassList!.push(classObj);
            } else {
                pastClassList!.push(classObj);
            }
        })
    }

    return {
        activeClassList: activeClassList,
        pastClassList: pastClassList,
        draftsList: draftsList
    }
}

const mapDispatchToProps = (dispatch: Function) => {
    return {
        fetchClassesIfNeeded: (): void => {
            dispatch(fetchEducatorClassesIfNeeded())
        }
    }

}

export default connect(mapStateToProps, mapDispatchToProps)(EducatorHome);