import { createMuiTheme } from "@material-ui/core/styles";
import { purple, pink, grey } from "@material-ui/core/colors";

const color = "rgba(255, 255, 255, 0.7)";
export const drawerWidth = 270;
export const textColor = grey[800];

const theme = createMuiTheme({
    typography: {
        useNextVariants: true,
        fontFamily: [
            "Nunito",
            "Roboto",
            "Open Sans",
            '"Helvetica Neue"',
            "Arial",
            "sans-serif",
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"'
        ].join(",")
    },
    palette: {
        primary: {
            light: purple[50],
            main: purple[500],
            dark: purple[600],
            contrastText: "#fff"
        },
        secondary: {
            light: pink[300],
            main: pink[500],
            dark: pink[600],
            contrastText: "#fff"
        }
    },
    shape: {
        borderRadius: 8
    },
    shadows: [
        "none",
        "0 1px 4px 0 rgba(0, 0, 0, 0.14)",
        "1px 2px 7px 0px rgba(0,0,0,0.14)",
        "0 6px 13px -2px rgba(0,0,0,0.14)",
        "0 8px 16px -1px rgba(0,0,0,0.14)",
        "0px 9px 19px 2px rgba(0,0,0,0.14)"
    ],
    overrides: {
        MuiButton: {
            text: {
                margin: ".3125rem 1px",
                padding: "10px 30px"
            },
            contained: {
                margin: ".3125rem 1px",
                padding: "10px 30px"
            },
            outlined: {
                margin: ".3125rem 1px",
                padding: "10px 30px"
            },
            root: {
                margin: ".3125rem 1px",
                padding: "10px 30px",
                fontSize: "0.8rem",
                borderRadius: 3
            },
            containedPrimary: {
                boxShadow:
                    "0 2px 2px 0 rgba(156, 39, 176, 0.14), 0 3px 1px -2px rgba(156, 39, 176, 0.2), 0 1px 5px 0 rgba(156, 39, 176, 0.12)",
                "&:hover": {
                    boxShadow:
                        "0 14px 26px -12px rgba(156, 39, 176, 0.4196078431372549), 0 4px 23px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(156, 39, 176, 0.2)"
                }
            }
        },
        MuiCircularProgress: {
            root: {
                alignSelf: "center",
                margin: "2rem"
            }
        },
        MuiTypography: {
            root: {
                color: textColor
            }
        },
        MuiDrawer: {
            // docked: {
            //   [breakpoints.up("sm")]: {
            //     width: drawerWidth,
            //     flexShrink: 0
            //   }
            // },
            paper: {
                color: "#fff",
                width: drawerWidth,
                border: "none",
                paddingLeft: 20,
                paddingRight: 20,
                backgroundColor: "#232629"
            }
        },
        MuiOutlinedInput: {
            root: {
                borderRadius: 0
            }
        },
        MuiGrid: {
            paper: {
                minWidth: 256,
                background: "#18202c",
                "& *": {
                    color
                }
            }
        }
    }
});

theme.drawerWidth = drawerWidth;

export default theme;
