import React from "react";
import { render } from "react-dom";
import thunkMiddleware from 'redux-thunk';
import "./index.css";
import { AppComp } from "./components/App";

import { createStore, applyMiddleware } from 'redux';
import { createLogger } from 'redux-logger';
import { Provider } from 'react-redux';
import { rootReducer } from "./utils/reduxReducers";
import firebase from "firebase/app";
import "firebase/auth";
import { BrowserRouter } from 'react-router-dom';
import { MuiThemeProvider } from "@material-ui/core";
import MuiTheme from "./assets/theme/muiTheme";

const log = require('debug')('INDEX_ENTRY:');

// Need to focus on the routing part as well, react router

// ReactDOM.render(<App />, document.getElementById("root"));


function startApp({ store }: any) {
  render(
    <Provider store={store}>
      <BrowserRouter>
        <MuiThemeProvider theme={MuiTheme}>
          <AppComp />
        </MuiThemeProvider>
      </BrowserRouter>
    </Provider>,
    document.getElementById('root'),
  );
}

// agar reducer should be pure functions, that takes he// now I should not mutate t


const middleware: Array<any> = [thunkMiddleware];
if (process.env.NODE_ENV !== 'production') {
  middleware.push(createLogger());
}

function initializeStore() {

  const store = createStore(rootReducer, applyMiddleware(...middleware));
  log('store is created', store);
  return store;
}


function initializeFirebase() {
  const firebaseConfig = {
    apiKey: "AIzaSyAkxWyuO6wFQf1Ge8lY05lpkgQFYSmGzJU",
    authDomain: "workhubdragonzurfer.firebaseapp.com",
    databaseURL: "https://workhubdragonzurfer.firebaseio.com",
    projectId: "workhubdragonzurfer",
    storageBucket: "workhubdragonzurfer.appspot.com",
    messagingSenderId: "824105967388",
    appId: "1:824105967388:web:4c6792ee491692452ae4d9",
    measurementId: "G-W3YWLBRVSG"
  };

  firebase.initializeApp(firebaseConfig);
}

function main() {

  initializeFirebase();
  const store = initializeStore();
  startApp({ store });

}

// start the app
main();