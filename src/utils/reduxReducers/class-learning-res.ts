import { IntKeyDict } from "../core-types/misc";
import { LearningRes } from "../core-types/shared";
import { RecvLearningResAction, EducatorPublishLearnResAction } from "../../components/educator-classroom/components/learning-res-mgt/actions";
import { AppInvalidateAction } from "../../components/App/actions";



export function ClassLearningRes(state: IntKeyDict<LearningRes[]> = {},action: RecvLearningResAction | EducatorPublishLearnResAction | AppInvalidateAction) {

    switch(action.type) {
        case 'RECEIVE_LEARNING_RESOURCE' : {
            state[action.classId] = action.resList;
            return {...state};
        }
        case 'EDUCATOR_PUBLISH_LEARNING_RESOURCE' : {
            let newState = {...state}
            newState[action.classId] = [action.learningRes].concat(newState[action.classId] || [])
            return newState;
        }
        case 'INVALIDATE_APP' : {
            return {}
        }
        default:
            return state;
    }
    
}