import { LoginSignupWinShowAction, LoginSignupWinCloseAction } from "../../components/login-signup/actions";

type IState = {
    isOpen:boolean,
    isLoginTab:boolean,
    onSuccessCallback?:() => void
}

export default function LoginSignupWindow(state:IState = {isOpen:false,isLoginTab:false},action:LoginSignupWinShowAction | LoginSignupWinCloseAction) :  IState {
    
    switch(action.type) {
        case 'LOGIN_SIGNUP_WINDOW_SHOW' : {
            return {isOpen: true,isLoginTab: action.isLoginTab,onSuccessCallback: action.onSuccessAction}
        }
        case 'LOGIN_SIGNUP_WINDOW_CLOSE' : {
            return {...state,isOpen:false, onSuccessCallback: undefined}
        }
        default:
            return state;
    }

}