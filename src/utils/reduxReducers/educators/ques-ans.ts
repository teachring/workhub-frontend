import { QuesAnsObj } from "../../core-types/shared";
import { IntKeyDict } from "../../core-types/misc";
import { EducatorQuesCloseAction, EducatorReceiveQuesAnsAction } from "../../../components/educator-classroom/components/ques-ans/actions";
import { AppInvalidateAction } from "../../../components/App/actions";
import { NotificationReceiveAction } from "../../reduxActions/notifications";
import { NotificationTypes } from "../../core-types/user-types";


export default function QuesAns(state:IntKeyDict<QuesAnsObj[]> = {}, action:EducatorReceiveQuesAnsAction | EducatorQuesCloseAction | AppInvalidateAction | NotificationReceiveAction) : IntKeyDict<QuesAnsObj[]> {
    
    switch(action.type) {
        case 'RECEIVE_EDUCATOR_QUES_ANS' : {
            const newState = {...state};
            newState[action.classId] = action.quesAnsList;
            return newState;
        }
        case 'EDUCATOR_CLOSE_QUESTION' : {
            let newList = state[action.classId].map(quesObj => {
                if(quesObj.questionId == action.quesId){
                    return action.updatedQuesObj;
                }
                return quesObj;
            })
            state[action.classId] = newList;
            return {...state};
        }
        case 'INVALIDATE_APP' : {
            return {}
        }
        case 'NOTIFICATIONS_RECEIVED' : {
            action.notificationsList.forEach((notificationObj) => {
                if(notificationObj.notificationType == NotificationTypes.CLASSROOOM_QUES_ASKED &&  state[notificationObj.classId]) {
                    if(!state[notificationObj.classId].some((quesAnsObj) => {
                        return quesAnsObj.questionId === (notificationObj.payload as QuesAnsObj).questionId
                    })) {
                        state[notificationObj.classId] = [notificationObj.payload as QuesAnsObj].concat(state[notificationObj.classId])
                    }
                } else if (notificationObj.notificationType == NotificationTypes.CLASSROOM_QUES_CLOSED && state[notificationObj.classId]) {
                    let newList:QuesAnsObj[] = []
                    state[notificationObj.classId].forEach((quesObj) => {
                        if(quesObj.questionId == (notificationObj.payload as QuesAnsObj).questionId) {
                            newList.push(notificationObj.payload);
                        } else {
                            newList.push(quesObj);
                        }
                    })
                    state[notificationObj.classId] = newList;
                }
            })
            return {...state};
        }
        default:
            return state
    }
}