import { EducatorRecvClassAction } from "../../../components/educator-home/actions";
import { AppInvalidateAction } from "../../../components/App/actions";
import { NotificationReceiveAction } from "../../reduxActions/notifications";
import { NotificationTypes } from "../../core-types/user-types";

export default function ClassIdList(state: number[] | null = null, action: EducatorRecvClassAction | AppInvalidateAction | NotificationReceiveAction): number[] | null {

    switch (action.type) {
        case 'EDUCATOR_RECEIVE_CLASS': {
            const newState = action.classList.map((classObj) => classObj.classId)
            return newState;
        }
        case 'INVALIDATE_APP': {
            return null;
        }
        // case 'NOTIFICATIONS_RECEIVED' : {
        //     action.notificationsList.forEach((notificationObj) => {
        //         if(notificationObj.notificationType == NotificationTypes.MODERATOR_ACCESS &&  state && ) {
        //             if(!state.includes(notificationObj.classId))
                        
                   
        //         }
        //     })
            
        // }
        default:
            return state;
    }
}