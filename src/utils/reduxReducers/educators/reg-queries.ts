import { EducatorRegQuery } from "../../core-types/educator-types";
import { IntKeyDict } from "../../core-types/misc";
import {EducatorReceiveRegQueriesAction,EducatorRegQueryCloseAction} from '../../../components/educator-classroom/components/reg-queries/actions';
import { AppInvalidateAction } from "../../../components/App/actions";
import { NotificationReceiveAction } from "../../reduxActions/notifications";
import { NotificationTypes } from "../../core-types/user-types";

export default function RegQueries(state:IntKeyDict<EducatorRegQuery[]> = {},action:EducatorReceiveRegQueriesAction | EducatorRegQueryCloseAction | AppInvalidateAction | NotificationReceiveAction) : IntKeyDict<EducatorRegQuery[]> {

    switch(action.type) {
        case 'RECEIVE_EDUCATOR_REG_QUERY' : {
            let newState = {...state}
            newState[action.classId] = action.regQueries;
            return newState;
        }
        case 'EDUCATOR_CLOSE_REG_QUERY' : {
            let newList:EducatorRegQuery[] = state[action.classId].map(regQuery => {
                if(regQuery.queryId == action.queryId){
                    return {...regQuery, status:'RESOLVED'}
                }
                return regQuery
            })
            state[action.classId] = newList;
            return {...state}
        }
        case 'INVALIDATE_APP' : {
            return {}
        }
        case 'NOTIFICATIONS_RECEIVED' : {
            action.notificationsList.forEach((notificationObj) => {
                if(notificationObj.notificationType == NotificationTypes.REG_ENQUIRY &&  state[notificationObj.classId]) {
                    if(!state[notificationObj.classId].some(
                        (queryObj) => {
                            return queryObj.queryId === (notificationObj.payload as EducatorRegQuery).queryId
                        })) {
                        state[notificationObj.classId] = [notificationObj.payload as EducatorRegQuery].concat(state[notificationObj.classId])
                    }
                }
            })
            return {...state};
        }
        //unread notifications ko use karenge...
        default:
            return state
    }

}
