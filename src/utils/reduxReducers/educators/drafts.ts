import { SavedDraftInfo } from "../../core-types/educator-types";
import { IntKeyDict } from '../../core-types/misc';
import { ClassCreateAction } from "../../../components/create-class/actions";
import { EducatorRecvDraftAction } from "../../../components/educator-home/actions";
import { AppInvalidateAction } from "../../../components/App/actions";
import { DraftCreateAction } from "../../../components/create-class/actions";


export default function ClassDraftInfoList(state: IntKeyDict<SavedDraftInfo> | null = null, action: EducatorRecvDraftAction | DraftCreateAction | ClassCreateAction | AppInvalidateAction) {

    switch (action.type) {

        case 'EDUCATOR_RECEIVE_DRAFT': {
            let newState: IntKeyDict<SavedDraftInfo> = { ...state }
            action.draftList.forEach(classDraftInfoObj => {
                newState[classDraftInfoObj.draftId] = classDraftInfoObj
            })
            return newState;
        }

        case 'DRAFT_CREATE': {
            let newState: IntKeyDict<SavedDraftInfo> = { ...state }
            newState[action.newDraft.draftId] = action.newDraft
            return newState;
        }

        case 'CLASS_PUBLISH': {
            let newState: IntKeyDict<SavedDraftInfo> = { ...state }
            delete newState[action.newClass.classId]
            return newState
        }

        case 'INVALIDATE_APP': {
            return null;
        }

        default:
            return state
    }
}