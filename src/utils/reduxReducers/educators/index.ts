import { combineReducers } from 'redux';
import DraftList from './drafts';
import RegQueries from './reg-queries';
import AnnouncementsDone from './announcements';
import QuesAns from './ques-ans';
import RegisteredLearners from './learner-mgt';
import ClassIdList from './classes-taught';

export const EducatorState = combineReducers({
    DraftList,
    RegQueries,
    AnnouncementsDone,
    QuesAns,
    RegisteredLearners,
    ClassIdList
});


  