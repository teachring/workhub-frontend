import { AnnouncementObj } from "../../core-types/educator-types";
import { IntKeyDict } from "../../core-types/misc";
import { EducatorAnncmntPostAction, EducatorAnncmntReceiveAction } from "../../../components/educator-classroom/components/announcements/actions";
import { AppInvalidateAction } from "../../../components/App/actions";
import { NotificationReceiveAction } from "../../reduxActions/notifications";
import { NotificationTypes } from "../../core-types/user-types";



export default function AnnouncementsDone(state:IntKeyDict<AnnouncementObj[]> = {},action:EducatorAnncmntReceiveAction | EducatorAnncmntPostAction | AppInvalidateAction | NotificationReceiveAction) : IntKeyDict<AnnouncementObj[]> {

    switch(action.type) {
        case 'RECEIVE_EDUCATOR_ANNOUNCEMENT' : {
            let newState = {...state}
            newState[action.classId] = action.announcementList;
            return newState;
        }
        case 'EDUCATOR_POST_ANNOUNCEMENT' : {
            let newState = {...state}
            newState[action.classId] = [action.announcement].concat(newState[action.classId] || [])
            return newState;
        }
        case 'INVALIDATE_APP' : {
            return {}
        }
        case 'NOTIFICATIONS_RECEIVED' : {
            action.notificationsList.forEach((notificationObj) => {
                if(notificationObj.notificationType == NotificationTypes.ANNOUNCEMENT &&  state[notificationObj.classId]) {
                    if(!state[notificationObj.classId].some((anncounmentObj) => {
                        return anncounmentObj.announcementId === (notificationObj.payload as AnnouncementObj).announcementId
                    })) {
                        state[notificationObj.classId] = [notificationObj.payload as AnnouncementObj].concat(state[notificationObj.classId])
                    }  
                }
            })
            return {...state};
        }
        default:
            return state
    }
}