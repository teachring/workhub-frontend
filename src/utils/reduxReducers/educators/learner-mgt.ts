import { EducatorRecvLearnersAction, EducatorSendPayReminderAction, EducatorRecvOfflinePayAction } from "../../../components/educator-classroom/components/learner-mgt/actions";
import { AppInvalidateAction } from "../../../components/App/actions";
import { IntKeyDict } from "../../core-types/misc";
import { RegisteredLearner } from "../../core-types/educator-types";


export default function RegisteredLearners(state:IntKeyDict<RegisteredLearner[]> = {},action: EducatorRecvLearnersAction | EducatorSendPayReminderAction | EducatorRecvOfflinePayAction | AppInvalidateAction) : IntKeyDict<RegisteredLearner[]>{

    switch(action.type) {
        case 'EDUCATOR_RECEIVE_LEARNERS' : {
            state[action.classId] = action.learnerList;
            return {...state};
        }
        case 'EDUCATOR_SENT_PAY_REMINDER' : {
            state[action.classId] = state[action.classId].map((learnerObj) => {
                if(action.userIds.includes(learnerObj.userId)){
                    let newPaymentObj = {...learnerObj.paymentDetails!,lastReminderDateTime:action.sentDateTime};
                    return {...learnerObj,paymentDetails:newPaymentObj};
                } else {
                    return learnerObj;
                }
            })
            return {...state};
        }
        case 'EDUCATOR_RECEIVE_OFFLINE_PAID' : {
            state[action.classId] = state[action.classId].map((learnerObj) => {
                if(action.userIds.includes(learnerObj.userId)){
                    let newPaymentObj = {...learnerObj.paymentDetails!,isPaid:true,paymentDateTime:action.payDateTime};
                    return {...learnerObj,paymentDetails:newPaymentObj};
                } else {
                    return learnerObj;
                }
            })
            return {...state};
        }
        case 'INVALIDATE_APP' : {
            return {}
        }
        default:
            return state;
    }

}