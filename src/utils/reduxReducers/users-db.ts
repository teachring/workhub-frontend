import {User} from '../core-types/user-types';
import { IntKeyDict } from '../core-types/misc';
import { UserDBAction } from '../reduxActions/users-db';


export function UsersDB(state:IntKeyDict<User>={},action:UserDBAction) : IntKeyDict<User> {
    switch(action.type){
        case 'RECEIVE_USER_LIST' :
            let newState:IntKeyDict<User> = {...state}
            action.users.forEach(userObj => {
                newState[userObj.id] = userObj
            })
            return newState
        default:
            return state
    }
}