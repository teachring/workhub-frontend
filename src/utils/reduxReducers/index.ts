import { combineReducers } from 'redux';
import AlertSnackbar from "./alert-snackbar-reducer";
import {ClassInfoList} from './class-reducers';
import { UsersDB } from './users-db';
import {EducatorState} from './educators';
import {AppState} from './app-state';
import NotificationData from './notifications';
import {LearnerState} from './learners';
import {ClassLearningRes} from './class-learning-res';
import LoginSignupWindow from './login-signup-window';

export const rootReducer = combineReducers({
  AppState,
  ClassInfoList,
  EducatorState,
  UsersDB,
  AlertSnackbar,
  NotificationData,
  LearnerState,
  ClassLearningRes,
  LoginSignupWindow
});

export type RootState = ReturnType<typeof rootReducer>
