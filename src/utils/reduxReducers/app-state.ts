import { AppInvalidateAction, AppStartAction } from "../../components/App/actions"


export type AppData = {
    appReady: boolean
    isLoggedIn: boolean,
    userId?: number,
    userName?: string,
}

//Now the thing that crea


export const AppState = (state: AppData = { appReady: false, isLoggedIn: false }, action: AppStartAction | AppInvalidateAction) => {
    switch (action.type) {
        case "INITIALIZE_APP":
            return { appReady: true, isLoggedIn: action.isLoggedIn, userId: action.userId , userName: action.userName}
        case "INVALIDATE_APP":
            return { ...state, appReady: false }
        default:
            return state
    }
}

