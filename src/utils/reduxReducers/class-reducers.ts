import {ClassInfo} from '../core-types/class-types';
import { IntKeyDict } from '../core-types/misc';
import { AddModeratorAction } from '../../components/educator-classroom/components/moderator-mgt/actions';
import { EducatorRecvClassAction } from '../../components/educator-home/actions';
import { ClassCreateAction } from '../../components/create-class/actions';
import { NotificationReceiveAction } from '../reduxActions/notifications';
import { NotificationTypes } from '../core-types/user-types';

export type ClassesReceivedAction = {
    type: 'CLASSES_RECEIVED',
    classList: ClassInfo[]
}



export function ClassInfoList(state:IntKeyDict<ClassInfo>={},action:ClassesReceivedAction | ClassCreateAction | EducatorRecvClassAction | AddModeratorAction | NotificationReceiveAction ) : IntKeyDict<ClassInfo> {

    switch(action.type) {
        case 'CLASSES_RECEIVED' : {
            let newState:IntKeyDict<ClassInfo> = {...state}
            action.classList.forEach(classInfoObj => {
                    newState[classInfoObj.classId] = classInfoObj
                }
            )
            return newState;
        }

        case 'EDUCATOR_RECEIVE_CLASS' : {
            let newState:IntKeyDict<ClassInfo> = {...state}
            action.classList.forEach(classInfoObj => {
                    newState[classInfoObj.classId] = classInfoObj
                }
            )
            return newState;
        }

        case 'CLASS_PUBLISH' : {
            let newState:IntKeyDict<ClassInfo> = {...state}
            newState[action.newClass.classId] = action.newClass
            return newState
        }
        case 'EDUCATOR_ADD_MODERATOR' : {
            if(!state[action.classId].moderatorList.includes(action.userId)){
                state[action.classId].moderatorList.push(action.userId);
                return {...state};
            } else {
                return state;
            }
        }
        case 'NOTIFICATIONS_RECEIVED' : {
            action.notificationsList.forEach((notificationObj) => {
                if(notificationObj.notificationType == NotificationTypes.MODERATOR_ACCESS) {
                    state[notificationObj.classId] = (notificationObj.payload as ClassInfo);
                }
            })
            return {...state};
        }
        default:
            return state
    }
    
}

