export const makeActionCreator = (type: string, ...argNames: string[]) => {
    return function(...args: string[]) {
        let action: any = { type };
        argNames.forEach((_arg, index) => {
            action[argNames[index]] = args[index];
        });
        return action;
    };
};

export const updateObject = (oldObject: Object, newValues: Object) => {
    return Object.assign({}, oldObject, newValues);
};
