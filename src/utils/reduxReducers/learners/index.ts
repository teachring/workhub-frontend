import { combineReducers } from 'redux';
import {RegisteredClassesState} from './reg-classes';
import { RegistrationQueries } from './reg-queries';
import ClassroomTokens from './classroom-token';
import ClassroomQuesAns from './ques-ans';

export const LearnerState = combineReducers({
    RegisteredClassesState,
    RegistrationQueries,
    ClassroomTokens,
    ClassroomQuesAns
})
