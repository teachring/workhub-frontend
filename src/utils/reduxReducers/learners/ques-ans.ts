import { IntKeyDict } from "../../core-types/misc";
import { QuesAnsObj } from "../../core-types/shared";
import { LearnerNewQuesAction, LearnerRecvQusAnsAction } from "../../../components/learner-classroom/components/learner-ques-ans/actions";
import { AppInvalidateAction } from "../../../components/App/actions";
import { NotificationReceiveAction } from "../../reduxActions/notifications";
import { NotificationTypes } from "../../core-types/user-types";

export default function ClassroomQuesAns(state:IntKeyDict<QuesAnsObj[]> = {},action:LearnerNewQuesAction | LearnerRecvQusAnsAction | AppInvalidateAction | NotificationReceiveAction) : IntKeyDict<QuesAnsObj[]> {

    switch(action.type) {
        case 'LEARNER_RECEIVE_QUES_ANS' : {
            state[action.classId] = action.quesAnsList;
            return {...state};
        }
        case 'LEARNER_ASK_NEW_QUES' : {
            let newState = {...state}
            newState[action.classId] = [action.quesObj].concat(newState[action.classId] || [])
            return newState;
        }
        case 'INVALIDATE_APP' : {
            return {};
        }
        case 'NOTIFICATIONS_RECEIVED' : {
            action.notificationsList.forEach((notificationObj) => {
                if(notificationObj.notificationType == NotificationTypes.CLASSROOM_QUES_CLOSED &&  state[notificationObj.classId]) {
                    let newList:QuesAnsObj[] = []
                    state[notificationObj.classId].forEach((quesObj) => {
                        if(quesObj.questionId == (notificationObj.payload as QuesAnsObj).questionId) {
                            newList.push(notificationObj.payload);
                        } else {
                            newList.push(quesObj);
                        }
                    })
                    state[notificationObj.classId] = newList;
                }
            })
            return {...state};
        }
        default:
            return state;
    }
}