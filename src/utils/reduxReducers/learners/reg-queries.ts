import { IntKeyDict } from "../../core-types/misc";
import { LearnerRegQuery } from "../../core-types/learner-reg";
import { ReceiveLearnerRegQueryAction, PublishLearnerRegQueryAction } from "../../../components/class-details/actions";
import { AppInvalidateAction } from "../../../components/App/actions";


export const RegistrationQueries = (state:IntKeyDict<LearnerRegQuery[]> = {},action:ReceiveLearnerRegQueryAction | PublishLearnerRegQueryAction | AppInvalidateAction ) : IntKeyDict<LearnerRegQuery[]> => {
    switch(action.type) {
        case 'RECEIVE_LEARNER_REG_QUERY' : {

            let newState = {...state}
            newState[action.classId] = action.queryList
            return newState;
        }
        case 'PUBLISH_NEW_LEARNER_REG_QUERY' : {
            let newState = {...state}
            newState[action.classId] = [action.query].concat(newState[action.classId] ||[])
            return newState;
        }
        case 'INVALIDATE_APP' : {
            return {}
        }
        default:
            return state
    }

}