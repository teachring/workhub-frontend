import { IntKeyDict } from "../../core-types/misc"
import { AppInvalidateAction } from "../../../components/App/actions"
import { RegInfo } from "../../core-types/learner-reg";
import { LearnerRegisterAction } from "../../../components/learner-register/actions";

export type ClassesRegReceivedAction = {
    type: 'CLASS_REG_DATA_RECEIVED',
    regInfoDict:IntKeyDict<RegInfo>
}


export const RegisteredClassesState = (state: IntKeyDict<RegInfo> = {},action:ClassesRegReceivedAction | AppInvalidateAction | LearnerRegisterAction) => {
    switch(action.type) {
        case 'CLASS_REG_DATA_RECEIVED': {
            let newState:IntKeyDict<RegInfo> = {...state}
            for (let key in action.regInfoDict) {
                newState[key] = action.regInfoDict[key]
            }
            return newState;
        }
        case 'INVALIDATE_APP': {
            return {}
        }
        case 'LEARNER_REGISTER' : {
            let newState:IntKeyDict<RegInfo> = {...state}
            newState[action.classId] = action.regInfo;
            return newState;
        }
        default:
            return state
    }
}

//create class and login signup ka flow end-2-end strong karte hai..
// learner-register ka page bhi properly setup karte