import { IntKeyDict } from "../../core-types/misc";
import { LearnerRecvClassTokenAction } from "../../../components/learner-classroom/components/learner-ques-ans/actions";
import { AppInvalidateAction } from "../../../components/App/actions";


export default function ClassroomTokens(state:IntKeyDict<string> = {},action: LearnerRecvClassTokenAction | AppInvalidateAction ) :IntKeyDict<string> {

    switch(action.type) {
        case 'LEARNER_RECEIVE_CLASS_TOKEN' : {
            state[action.classId] = action.token;
            return {...state};
        }
        case 'INVALIDATE_APP' : {
            return {};
        }
        default:
            return state;
    }

}