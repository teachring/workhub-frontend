import {Notification} from '../core-types/user-types';
import { NotificationReceiveAction, NotificationReadAction } from '../reduxActions/notifications';
import { AppInvalidateAction } from '../../components/App/actions';

type NotificationStateObj = {
    timerId?: NodeJS.Timeout,
    notificationList: Notification[]
}


const NotificationData = (state:NotificationStateObj = {notificationList:[]},action:NotificationReceiveAction | AppInvalidateAction | NotificationReadAction ) => {

    switch(action.type) {
        case 'NOTIFICATIONS_RECEIVED' : {
            console.log("RECEIVED NOTS ",action.notificationsList);
            var newList = action.notificationsList.concat(state.notificationList)
            console.log("NEW LIST ",newList);
            return {timerId:action.timerId,notificationList:newList}
        }
        case 'INVALIDATE_APP' : {
            return {notificationList:[]}
        }
        case 'NOTIFICATIONS_READ' : {
            var newList = state.notificationList.map(notificationItem => {
                if(action.notificationIdList.includes(notificationItem.notificationId)) {
                    return {...notificationItem,isRead:true}
                } else return notificationItem
            })
            return {...state,notificationList:newList}
        }
        default:
            return state
    }
}

export default NotificationData;