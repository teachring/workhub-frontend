
import {Notification, NotificationTypes,NotificationPayloadTypeMap} from '../core-types/user-types';
import axiosObj, { mapRemoteClassToDomainObj } from './API';
import { mapRemoteAnncmntToDomainObj } from './educators';
import { mapRemoteEducatorRegQueryToDomainObj } from './reg-enquires';
import { mapRemoteQuesAnsToDomainObj } from './ques-ans';


type RemoteNotification = {
    notificationId:number,
    classID:number,
    createDateTime:string,
    payload:string,
    isRead:boolean,
    notificationType:string
}

const notificationListMapper = (remoteNots:RemoteNotification[]) : Notification[]=> {
  return remoteNots.map(remoteObj => {

    const notificationObj:Notification = {
      notificationId: remoteObj.notificationId,
      classId: remoteObj.classID,
      payload: remoteObj.payload,
      createdDateTime: new Date(remoteObj.createDateTime),
      isRead: remoteObj.isRead,
      notificationType: NotificationTypes[remoteObj.notificationType]
    }

    switch(notificationObj.notificationType) {
      
      case NotificationTypes.ANNOUNCEMENT: {
        notificationObj.payload = mapRemoteAnncmntToDomainObj(JSON.parse(notificationObj.payload));
        break;
      }
      case  NotificationTypes.REG_ENQUIRY : {
        notificationObj.payload = mapRemoteEducatorRegQueryToDomainObj(JSON.parse(notificationObj.payload));
        break;
      }
      case NotificationTypes.CLASSROOOM_QUES_ASKED : 
      case NotificationTypes.CLASSROOM_QUES_CLOSED : {
        notificationObj.payload = mapRemoteQuesAnsToDomainObj(JSON.parse(notificationObj.payload));
        break;
      }
      case NotificationTypes.MODERATOR_ACCESS : {
        notificationObj.payload = mapRemoteClassToDomainObj(JSON.parse(notificationObj.payload));
        break;
      }
    }
    return notificationObj
  })
}

export const fetchAllNotifications = async ()  => {
    try {
        const response = await axiosObj.get<RemoteNotification[]>("/user/notifications");
        const remoteNotifications = response.data
        const notifications:Notification<any>[] = notificationListMapper(remoteNotifications);
        
        return notifications
    
    } catch(err) {
        throw err;
    }
}

export const fetchLatestNotifications = async (lastFetchedId:number) => {
  try {
    const response = await axiosObj.get<RemoteNotification[]>("/user/notifications/poll?lastID="+lastFetchedId);
    const remoteNotifications = response.data
    const notifications:Notification[] = notificationListMapper(remoteNotifications);

    return notifications

  } catch(err) {
    throw err;
  }
}


export const updateNotificationToRead = async (notificationIdList:number[]) : Promise<void> => {
  try {
    await axiosObj.post("/user/notifications/read",{notificationIDs:notificationIdList})
  } catch (err) {
    throw err
  }
}