import axiosObj from './API';

export const getRzrpOrderId = async (classId:number) : Promise<string> => {
    try {
        const response = await axiosObj.get<any>('/payments/class/'+classId+'/create-order')
        return response.data.order_id;
    } catch(err) {
        throw err;
    }

}