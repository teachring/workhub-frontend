import axiosObj from './API'
import { IntKeyDict } from '../core-types/misc'
import { RegFormQuestion, LearnerRegQuery } from '../core-types/learner-reg'
import { RzpIdentifiers } from '../../components/learner-register/rzp-helper'
//which class you want to register and all the details

type RemoteRegPayRes = {
    paymentStatus: "SUCCESS" | "FAILURE",
    regDateTime?: string,
}

type RemoteRegNotPayRes = {
    regDateTime: string
}


///basically you need to give me register date time and payment date time are 

export const registerByPaying = async (classId:number, rzpData: RzpIdentifiers, regFormData?: IntKeyDict<RegFormQuestion>,) => {
    //Here I need to find out whether the payment status is true or false...
    try {
        const response = await axiosObj.post<RemoteRegPayRes>("/class/"+classId+"/register/online",{
            regFormData: JSON.stringify(regFormData),
            rzpOrderID: rzpData.orderId,
            rzpPaymentID: rzpData.paymentId,
            rzpSignature: rzpData.signature
        })

        const resDataObj = response.data
        return {
            paymentStatus: resDataObj.paymentStatus == 'SUCCESS',
            regDateTime: (resDataObj.regDateTime) ? new Date(resDataObj.regDateTime) : undefined }
      } catch (err) {
        throw err
      }
}

export const registerWithoutPaying = async (classId:number,regFormData?: IntKeyDict<RegFormQuestion>) => {
    try {
        const response = await axiosObj.post<RemoteRegNotPayRes>("/class/"+classId+"/register/offline",{regFormData: JSON.stringify(regFormData)})
        const resDataObj = response.data
        return new Date(resDataObj.regDateTime)
    } catch (err) {
        throw err
    }

}

export const fetchLearnerAccessToken = async (classId:number) : Promise<string> => {
    try {
        const response = await axiosObj.get<{token:string}>('/class/'+classId+'/classroom/token')
        return response.data.token;
    } catch(err) {
        throw err;
    }
}