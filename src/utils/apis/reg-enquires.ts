import axiosObj from './API'
import { LearnerRegQuery } from "../core-types/learner-reg"
import { EducatorRegQuery } from '../core-types/educator-types'

type RemoteRegQueryObj = {
    enquiryId:number,
    body: string,
    creationDateTime: string
}

type RemoteEducatorRegQuery = {
    enquiryId: number, 
    body: string,
    creationDateTime: string,
    status: 'PENDING' | 'RESOLVED'
    createdBy: number
}

export const mapRemoteEducatorRegQueryToDomainObj = (remoteObj: RemoteEducatorRegQuery) : EducatorRegQuery => {
    return {
        queryId: remoteObj.enquiryId,
        query: remoteObj.body,
        askedDateTime: new Date(remoteObj.creationDateTime),
        askedBy: remoteObj.createdBy,
        status: remoteObj.status
    }
}
  
const mapRemoteRegQueryToDomainObj = (remoteObj:RemoteRegQueryObj) : LearnerRegQuery => {
    return {
        queryId: remoteObj.enquiryId,
        query: remoteObj.body,
        askedDateTime: new Date(remoteObj.creationDateTime)
    }
}

export const postNewRegEnquiry = async (classId:number,newQuery:string) : Promise<LearnerRegQuery> => {
    try {
        const response = await axiosObj.post<RemoteRegQueryObj>('/class/'+classId+'/enquiry',{body: newQuery})
        const resDataObj = response.data
        return mapRemoteRegQueryToDomainObj(resDataObj)
    } catch(err) {
        throw err
    }

}

export const fetchAllRegEnquiries = async (classId:number) : Promise<LearnerRegQuery[]> => {
    try {
        const response = await axiosObj.get<RemoteRegQueryObj[]>('/class/'+classId+'/enquiries')
        const resDataObj= response.data
        return resDataObj.map(remoteObj => (
            mapRemoteRegQueryToDomainObj(remoteObj)
        ))
    } catch(err) {
        throw err
    }
}

export const educatorFetchRegEnquiries = async (classId:number) : Promise<EducatorRegQuery[]> => {
    try {
        var response = await axiosObj.get<RemoteEducatorRegQuery[]>('/educator/class/'+classId+'/enquiries')
        const resDataObj = response.data;
        return resDataObj.map(remoteObj => (mapRemoteEducatorRegQueryToDomainObj(remoteObj)))
    } catch(err) {
        throw err
    }
}

export const educatorRegQueryStatusUpdate = async (classId:number,queryId:number) : Promise<void> => {
    try {
        await axiosObj.post('/educator/class/'+classId+'/enquiry',{enquiryId:queryId})
    } catch(err) {
        throw err
    }
}


