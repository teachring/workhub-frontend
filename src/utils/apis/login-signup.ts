import axiosObj from './API';

export type LoginRequestObj = {
    idToken:string
}
  
export type SignUpRequestObj = {
    idToken: string,
    name: string,
    email: string
    phoneNumber: string
}

//Now when I say that login component is very component


export async function verifyIfAccountExists(phoneNo:string): Promise<boolean>{
    try {
        const response = await axiosObj.post<{status:boolean}>('/user/isregistered',{phoneNumber:phoneNo})
        return response.data.status;
    } catch(err) {
        throw err;
    }
}
  
  
export async function getServerTokenOnLogin(reqObj:LoginRequestObj) : Promise<void>{
    try {
        await axiosObj.post('/login',reqObj)
    }catch(err){
        throw err;
    }
}

export async function getServerTokenOnSignup(reqObj:SignUpRequestObj) : Promise<void>{
    try {
        await axiosObj.post('/user/create',reqObj)
    }catch(err){
        throw err;
    }

}
  