import axiosObj from './API';
import { QuesAnsObj } from '../core-types/shared';


type RemoteQuesAnsObj = {
    questionID:number,
    askedBy: number,
    questionBody: string,
    askedDateTime: number,
    answerBody?: string,
    closeBy?: number,
	closeDateTime?: string,
	status:'PENDING' | 'CLOSED'
}

export const mapRemoteQuesAnsToDomainObj = (remoteObj:RemoteQuesAnsObj):QuesAnsObj=> {
    return {
        questionId: remoteObj.questionID,
        askedBy: remoteObj.askedBy,
        questionBody: remoteObj.questionBody,
        askedDateTime: new Date(remoteObj.askedDateTime),
        answerBody: remoteObj.answerBody,
        closedBy: remoteObj.closeBy,
        closeDateTime: (remoteObj.closeDateTime) ? new Date(remoteObj.closeDateTime) : undefined,
        status: remoteObj.status
    }
}

export const educatorCloseQuestion = async (classId:number,quesId:number,ansBody?:string):Promise<QuesAnsObj> => {
    try {
        const response = await axiosObj.post<RemoteQuesAnsObj>('/educator/class/'+classId+'/classroom/q/close',{questionID:quesId,answerBody:ansBody})
        return mapRemoteQuesAnsToDomainObj(response.data)
        
    } catch(err){
        throw err
    }
}


export const educatorFetchQuestions = async (classId:number) : Promise<QuesAnsObj[]> => {
    try {
        let response = await axiosObj.get<RemoteQuesAnsObj[]>('/educator/class/'+classId+'/classroom/questions')
        return response.data.map(remoteObj => mapRemoteQuesAnsToDomainObj(remoteObj))
    }catch(err) {
        throw err
    }
}

export const learnerPostQuestion = async (classId:number,quesBody:string) : Promise<QuesAnsObj> => {
    try {
        const response = await axiosObj.post<RemoteQuesAnsObj>('/class/'+classId+'/classroom/q/create',{questionBody:quesBody})
            return mapRemoteQuesAnsToDomainObj(response.data)

    } catch(err){
        throw err;
    }
}

export const learnerFetchQuestions = async (classId:number) : Promise<QuesAnsObj[]> => {
    try {

        const response = await axiosObj.get<RemoteQuesAnsObj[]>('/class/'+classId+'/classroom/questions');
        return response.data.map(remoteObj => mapRemoteQuesAnsToDomainObj(remoteObj));

    } catch(err) {
        throw err;
    }

}

//Now I need to have the 