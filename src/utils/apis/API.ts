import axios, { AxiosError, AxiosResponse } from "axios";
import { ClassInfo } from "../core-types/class-types";
import { RegInfo, RegClassData } from "../core-types/learner-reg";

const axiosObj = axios.create({
  baseURL: "https://www.teachring.app/api",
  // baseURL: "http://localhost:7000",
  //baseURL: "http://192.168.0.106:7000",
  timeout: 2000,
  headers: { 'content-type': 'application/json' },
  withCredentials: true
});

// {
//   "classID": 7,
//   "organizerName": "ALGOMANIAC",
//   "title": "DELTA",
//   "description": "This is DELTA",
//   "startDateTime": "2020-03-15T23:00:00Z",
//   "endDateTime": "2020-03-16T23:00:00Z",
//   "venue": "orion",
//   "isPaid": true,
//   "feeAmt": "500",
//   "offlinePayAvl": true,
//   "regFormFields": "",
//   "regStartDateTime": "2020-03-10T23:00:00Z",
//   "regEndDateTime": "2020-03-13T23:00:00Z",
//   "agenda": "nothing",
//   "prerequirement": "nothing",
//   "details": "nothing",
//   "faq": "whats up? nothing",
//   "bgColor": "",
//   "isDraft": true,
//   "createdBy": 7,
//   "moderatorList": [
//       7
//   ]
// },

export type RemoteClassInfo = {
  classID: number,
  organizerName: string,
  title: string,
  description?: string,
  startDateTime?: string,
  endDateTime?: string,
  venue?: string,
  isPaid?: boolean,
  feeAmt?: number,
  offlinePayAvl?: boolean,
  regFormFields?: string,
  regStartDateTime?: string,
  regEndDateTime?: string,
  agenda?: string,
  prerequirement?: string,
  details?: string,
  faq?: string,
  bgColor?: string,
  isDraft: boolean,
  createdBy: number,
  // UpdatedAt: string,
  moderatorList?: number[]
}

type RemoteRegInfo = {
  regDateTime: string,
  regFormData?: string,
  paymentDetails?: {
    isPaid: boolean,
    modeSelected: 'ONLINE' | 'OFFLINE',
    paymentDateTime?: string
  }
}

type RemoteRegClassData = {
  classInfo: RemoteClassInfo,
  regData: RemoteRegInfo
}

export const mapRemoteClassToDomainObj = (remoteClassObj: RemoteClassInfo): ClassInfo => {
  let domainObj: ClassInfo = {
    classId: remoteClassObj.classID,
    title: remoteClassObj.title,
    organizerName: remoteClassObj.organizerName,
    startDateTime: new Date(remoteClassObj.startDateTime!),
    endDateTime: new Date(remoteClassObj.endDateTime!),
    regStartDateTime: new Date(remoteClassObj.regStartDateTime!),
    regEndDateTime: new Date(remoteClassObj.regEndDateTime!),
    description: remoteClassObj.description!,
    venue: remoteClassObj.venue!,
    isPaid: remoteClassObj.isPaid!,
    agenda: remoteClassObj.agenda!,
    bgColor: remoteClassObj.bgColor!,
    prerequirement: remoteClassObj.prerequirement!,
    createdBy: remoteClassObj.createdBy,
    // publishDateTime: new Date(remoteClassObj.UpdatedAt),
    publishDateTime: new Date(),
    moderatorList: remoteClassObj.moderatorList!,
    otherDetails: remoteClassObj.details
  }

  if (remoteClassObj.regFormFields) {
    try {
      domainObj.regFormData = JSON.parse(remoteClassObj.regFormFields)
    } catch { }
  }

  // if(remoteClassObj.faq){
  //   domainObj.faq = JSON.parse(remoteClassObj.faq)
  // }

  if (remoteClassObj.isPaid) {
    domainObj.paymentDetails = { feeAmt: remoteClassObj.feeAmt!, offlinePayAvl: remoteClassObj.offlinePayAvl! }
  }

  return domainObj;
}

const mapRemoteRegInfoToDomainObj = (remoteObj: RemoteRegInfo): RegInfo => {
  const regInfoObj: RegInfo = {
    regDateTime: new Date(remoteObj.regDateTime),
  }

  if (remoteObj.regFormData) {
    try {
      regInfoObj.regFormData = JSON.parse(remoteObj.regFormData)
    } catch { }
  }

  if (remoteObj.paymentDetails) {
    regInfoObj.paymentDetails = {
      isPaid: remoteObj.paymentDetails.isPaid,
      modeSelected: remoteObj.paymentDetails.modeSelected,
      paymentDateTime: (remoteObj.paymentDetails.paymentDateTime) ? new Date(remoteObj.paymentDetails.paymentDateTime) : undefined
    }
  }
  return regInfoObj
}

const fetchRegisteredClasses = async () => {
  try {
    const response = await axiosObj.get<RemoteRegClassData[]>("/classes/registered");
    const remoteRegClasses = response.data
    const regClasses: RegClassData[] = remoteRegClasses.map(remoteObj => {
      return {
        classInfo: mapRemoteClassToDomainObj(remoteObj.classInfo),
        regData: mapRemoteRegInfoToDomainObj(remoteObj.regData)
      }
    })
    return regClasses

  } catch (err) {
    throw err;
  }
}

const fetchUpcomingClasses = async (): Promise<ClassInfo[]> => {
  try {
    const response = await axiosObj.get<RemoteClassInfo[]>("/classes/upcoming");
    const remoteClasses = response.data;
    return remoteClasses.map(remoteObj => {
      console.log("UPCOMING ", remoteObj);
      return mapRemoteClassToDomainObj(remoteObj)
    })
  } catch (err) {
    throw err
  }
}

export const fetchClassDataNotSignedIn = () => {
  return fetchUpcomingClasses()
}

export const fetchClassDataSignedIn = () => {
  return Promise.all([fetchRegisteredClasses(), fetchUpcomingClasses()])
}

export default axiosObj;
