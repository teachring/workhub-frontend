import axiosObj from './API';
import { LearningRes } from '../core-types/shared';

type RemoteLearningRes = {
    resourceID: number,
    creationDateTime: string,
    createdBy: number,
    title: string,
    body: string
}


const mapRemoteLearningResToDomainObj = (remoteObj:RemoteLearningRes) : LearningRes => {
    return {
        resourceId: remoteObj.resourceID,
        creationDateTime: new Date(remoteObj.creationDateTime),
        createdBy: remoteObj.createdBy,
        title: remoteObj.title,
        body: remoteObj.body
    }
}


export const educatorFetchAllDocs = async (classId:number) : Promise<LearningRes[]> => {

    try {

        const response = await axiosObj.get<RemoteLearningRes[]>('/educator/class/'+classId+'/classroom/resources');
        return response.data.map((remoteObj) => mapRemoteLearningResToDomainObj(remoteObj));

    } catch(err) {
        throw err;
    }
}

export const learnerFetchAllDocs = async (classId:number): Promise<LearningRes[]> => {
    try {

        const response = await axiosObj.get<RemoteLearningRes[]>('/class/'+classId+'/classroom/resources');
        return response.data.map((remoteObj) => mapRemoteLearningResToDomainObj(remoteObj));

    } catch(err) {
        throw err
    }

}   

export const educatorPostNewDoc = async (classId:number,docTitle:string,docBody:string) : Promise<LearningRes> => {
    try {
        const response = await axiosObj.post<RemoteLearningRes>('/educator/class/'+classId+'/classroom/resource/create',{title:docTitle,body:docBody});
        return mapRemoteLearningResToDomainObj(response.data);
    } catch(err) {
        throw err;
    }
    
}