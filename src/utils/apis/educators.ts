import axiosObj, { RemoteClassInfo, mapRemoteClassToDomainObj } from './API'
import { User } from '../core-types/user-types'
import { AnnouncementObj, RegisteredLearner, SavedDraftInfo, NewDraftObj } from '../core-types/educator-types'
import { ClassInfo, NewClassObj, FAQ } from '../core-types/class-types'
import { getISODateString } from '../helpers'

type RemoteAnncmntObj = {
    announcementId: number,
    createdBy: number,
    createDateTime: string
    message: string
}

type RemoteNewClassObj = {
    organizerName: string,
    title: string
    description?: string
    startDateTime?: string
    endDateTime?: string
    venue?: string
    isPaid: boolean
    feeAmt?: string
    offlinePayAvl?: boolean
    regFormFields?: string
    regStartDateTime?: string
    regEndDateTime?: string
    agenda?: string
    prerequirement?: string
    details?: string
    faq?: string
    isDraft: boolean
}

export const mapRemoteAnncmntToDomainObj = (remoteObj: RemoteAnncmntObj): AnnouncementObj => {
    return {
        announcementId: remoteObj.announcementId,
        createdBy: remoteObj.createdBy,
        message: remoteObj.message,
        createDateTime: new Date(remoteObj.createDateTime)
    }
}

const mapDraftToRemoteObj = (newDraftObj: NewDraftObj): RemoteNewClassObj => {
    const stringifyDateTime = (dateObj: Date | undefined) => {
        return dateObj ? getISODateString(dateObj) : undefined;
    }

    const stringifyComplexObj = (obj: Object | undefined) => {
        return obj ? JSON.stringify(obj) : undefined;
    }

    return {
        organizerName: newDraftObj.organizerName,
        title: newDraftObj.title,
        isPaid: newDraftObj.isPaid,
        isDraft: true,
        startDateTime: stringifyDateTime(newDraftObj.startDateTime),
        endDateTime: stringifyDateTime(newDraftObj.endDateTime),
        regStartDateTime: stringifyDateTime(newDraftObj.regStartDateTime),
        regEndDateTime: stringifyDateTime(newDraftObj.regEndDateTime),
        description: newDraftObj.description,
        venue: newDraftObj.venue,
        agenda: newDraftObj.agenda,
        prerequirement: newDraftObj.prerequirement,
        details: newDraftObj.otherDetails,
        feeAmt: newDraftObj.feeAmt ? newDraftObj.feeAmt.toString() : undefined,
        offlinePayAvl: newDraftObj.offlinePayAvl,
        regFormFields: stringifyComplexObj(newDraftObj.regFormData),
        faq: stringifyComplexObj(newDraftObj.faq),
    }
}

const mapClassToRemoteObj = (newClassObj: NewClassObj): RemoteNewClassObj => {

    const stringifyComplexObj = (obj: Object | undefined) => {
        return obj ? JSON.stringify(obj) : undefined;
    }

    return {
        organizerName: newClassObj.organizerName,
        title: newClassObj.title,
        isPaid: newClassObj.isPaid,
        isDraft: false,
        startDateTime: getISODateString(newClassObj.startDateTime),
        endDateTime: getISODateString(newClassObj.endDateTime),
        regStartDateTime: getISODateString(newClassObj.regStartDateTime),
        regEndDateTime: getISODateString(newClassObj.regEndDateTime),
        description: newClassObj.description,
        venue: newClassObj.venue,
        agenda: newClassObj.agenda,
        prerequirement: newClassObj.prerequirement,
        details: newClassObj.otherDetails,
        feeAmt: newClassObj.paymentDetails ? newClassObj.paymentDetails.feeAmt.toString() : undefined,
        offlinePayAvl: newClassObj.paymentDetails ? newClassObj.paymentDetails.offlinePayAvl : undefined,
        regFormFields: stringifyComplexObj(newClassObj.regFormData),
        faq: stringifyComplexObj(newClassObj.faq)
    }

}
type RemoteRegLearnerObj = {
    userID: number,
    regDateTime: string,
    regFormData?: string,
    paymentDetails?: {
        modeSelected: 'ONLINE' | 'OFFLINE',
        isPaid: boolean,
        paymentDateTime?: string,
        lastReminderDateTime?: string
    }
}

const mapRemoteRegLearnerToDomainObj = (remoteObj: RemoteRegLearnerObj): RegisteredLearner => {
    const regLearnerObj: RegisteredLearner = {
        userId: remoteObj.userID,
        regDateTime: new Date(remoteObj.regDateTime),
    }

    if (remoteObj.regFormData) {
        try {
            regLearnerObj.regFormData = JSON.parse(remoteObj.regFormData)
        } catch { }
    }

    if (remoteObj.paymentDetails) {
        regLearnerObj.paymentDetails = {
            isPaid: remoteObj.paymentDetails.isPaid,
            modeSelected: remoteObj.paymentDetails.modeSelected,
            paymentDateTime: (remoteObj.paymentDetails.paymentDateTime) ? new Date(remoteObj.paymentDetails.paymentDateTime) : undefined,
            lastReminderDateTime: (remoteObj.paymentDetails.lastReminderDateTime) ? new Date(remoteObj.paymentDetails.lastReminderDateTime) : undefined
        }
    }
    return regLearnerObj;
}

const mapRemoteClassToDraftObj = (remoteObj: RemoteClassInfo): SavedDraftInfo => {

    const parseDateIfPresent = (dateStr?: string): Date | undefined => {
        return (dateStr) ? new Date(dateStr) : undefined;
    }

    const draftObj: SavedDraftInfo = {
        draftId: remoteObj.classID,
        createdBy: remoteObj.createdBy,
        lastUpdateDateTime: new Date(),
        isPaid: remoteObj.isPaid!,
        title: remoteObj.title,
        organizerName: remoteObj.organizerName,
        description: remoteObj.description,
        venue: remoteObj.venue,
        feeAmt: remoteObj.feeAmt,
        offlinePayAvl: remoteObj.offlinePayAvl,
        startDateTime: parseDateIfPresent(remoteObj.startDateTime),
        endDateTime: parseDateIfPresent(remoteObj.endDateTime),
        regStartDateTime: parseDateIfPresent(remoteObj.regStartDateTime),
        regEndDateTime: parseDateIfPresent(remoteObj.regEndDateTime),
        agenda: remoteObj.agenda,
        prerequirement: remoteObj.prerequirement,
        otherDetails: remoteObj.details,
    }

    if (remoteObj.faq) {
        try {
            draftObj.faq = JSON.parse(remoteObj.faq)
        } catch { }
    }

    if (remoteObj.regFormFields) {
        try {
            draftObj.regFormData = JSON.parse(remoteObj.regFormFields);
        } catch { }
    }

    return draftObj;
}

export const fetchUserDetails = async (userIdList: number[]): Promise<User[]> => {
    try {
        console.log("Fetching details for ", userIdList);
        const response = await axiosObj.post<User[]>('/educator/users/fetchdetails', userIdList)
        console.log("Fetched details for ", userIdList);
        return response.data
    } catch (err) {
        throw err
    }
}

export const fetchAnnouncements = async (classId: number): Promise<AnnouncementObj[]> => {
    try {
        const response = await axiosObj.get<RemoteAnncmntObj[]>('/educator/class/' + classId + '/classroom/announcements')
        return response.data.map(remoteObj => mapRemoteAnncmntToDomainObj(remoteObj))
    } catch (err) {
        throw err
    }
}

export const postAnnouncement = async (classId: number, msg: string): Promise<AnnouncementObj> => {
    try {
        const response = await axiosObj.post<RemoteAnncmntObj>('/educator/class/' + classId + '/announcement', { message: msg })
        return mapRemoteAnncmntToDomainObj(response.data)
    } catch (err) {
        throw err
    }
}


export const addModerator = async (classId: number, userId: number): Promise<void> => {
    try {
        await axiosObj.post('/educator/class/' + classId + '/moderator/add', { userID: userId })
    } catch (err) {
        throw err
    }
}

export const findModByPhoneNum = async (phoneNo: string): Promise<User | null> => {
    try {
        const response = await axiosObj.get<User | string>('/educator/findmoderators?phone=' + phoneNo)
        if (typeof response.data == 'string')
            return null;
        return response.data;
    } catch (err) {
        throw err;
    }
}

export const fetchRegisteredLearners = async (classId: number): Promise<RegisteredLearner[]> => {
    try {
        const response = await axiosObj.get<RemoteRegLearnerObj[]>('/educator/class/' + classId + '/learners');
        return response.data.map(remoteObj => mapRemoteRegLearnerToDomainObj(remoteObj))

    } catch (err) {
        throw err
    }
}

export const sendPayReminders = async (classId: number, userList: number[], message: string): Promise<Date> => {
    try {
        const response = await axiosObj.post<{ lastReminderDateTime: string }>('/educator/class/' + classId + '/paymentreminder', { userIds: userList, message: message })
        return new Date(response.data.lastReminderDateTime)
    } catch (err) {
        throw err;
    }
}

export const markOfflinePaid = async (classId: number, userList: number[]): Promise<Date> => {

    type ResObj = {
        paymentDateTime: string,
        paymentStatus: string
    }

    try {

        const response = await axiosObj.post<ResObj[]>('/educator/class/' + classId + '/offlinepaid', { userIDs: userList })
        //if(response.data[0].paymentStatus)
        return new Date(response.data[0].paymentDateTime)

    } catch (err) {
        throw err;
    }
}

export type EducatorClassesResponse = {
    drafts: SavedDraftInfo[],
    classes: ClassInfo[]
}

export const fetchEducatorClasses = async (): Promise<EducatorClassesResponse> => {
    try {
        const response = await axiosObj.get<RemoteClassInfo[]>('/educator/classes')
        const retObj: EducatorClassesResponse = { drafts: [], classes: [] }
        response.data.forEach(remoteObj => {
            if (remoteObj.isDraft) {
                retObj.drafts.push(mapRemoteClassToDraftObj(remoteObj))
            } else {
                retObj.classes.push(mapRemoteClassToDomainObj(remoteObj))
            }
        })

        return retObj;

    } catch (err) {
        throw err;
    }

}

export const createNewClass = async (newObj: NewClassObj): Promise<ClassInfo> => {
    try {
        var remoteReqObj: RemoteNewClassObj = mapClassToRemoteObj(newObj);
        const response = await axiosObj.post<RemoteClassInfo>('/class/create', remoteReqObj);
        return mapRemoteClassToDomainObj(response.data);
    } catch (err) {
        throw err;
    }

    //
}

export const createNewDraft = async (newObj: NewDraftObj): Promise<SavedDraftInfo> => {
    try {
        const remoteReqObj = mapDraftToRemoteObj(newObj);
        const response = await axiosObj.post<RemoteClassInfo>('/class/create', remoteReqObj);
        return mapRemoteClassToDraftObj(response.data);

    } catch (err) {
        throw err;
    }
}

export const updateExistingDraft = async (updatedObj: NewDraftObj, draftId: number): Promise<SavedDraftInfo> => {
    try {
        const remoteReqObj = mapDraftToRemoteObj(updatedObj);
        const response = await axiosObj.post<RemoteClassInfo>('/educator/class/' + draftId + '/update', remoteReqObj);
        return mapRemoteClassToDraftObj(response.data);

    } catch (err) {
        throw err;
    }
}

export const publishClassFromDraft = async (newClassObj: NewClassObj, draftId: number): Promise<ClassInfo> => {
    try {
        const remoteReqObj = mapClassToRemoteObj(newClassObj);
        const response = await axiosObj.post<RemoteClassInfo>('/educator/class/' + draftId + '/update', remoteReqObj);
        return mapRemoteClassToDomainObj(response.data);

    } catch (err) {
        throw err;
    }
}