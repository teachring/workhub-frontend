export function getISODateString(date: Date) {
    var tzo = -date.getTimezoneOffset(),
        dif = tzo >= 0 ? '+' : '-',
        pad = function (num: any) {
            var norm = Math.floor(Math.abs(num));
            return (norm < 10 ? '0' : '') + norm;
        };
    return date.getFullYear() +
        '-' + pad(date.getMonth() + 1) +
        '-' + pad(date.getDate()) +
        'T' + pad(date.getHours()) +
        ':' + pad(date.getMinutes()) +
        ':' + pad(date.getSeconds()) +
        dif + pad(tzo / 60) +
        ':' + pad(tzo % 60);
}

export function timeSince(date: Date) {

    var minutes = Math.floor((new Date().getTime() - date.getTime()) / (60 * 1000));

    var interval = minutes / (24 * 60);
    if (interval > 7) {
        return readDate(date);
    }
    interval = Math.floor(minutes / (24 * 60));
    if (interval > 1) {
        return interval + " days ago";
    }
    interval = Math.floor(minutes / 60);
    if (interval > 1) {
        return interval + " hours ago";
    }
    interval = Math.floor(minutes);
    if (interval > 1) {
        return interval + " minutes ago";
    }
    return "few moments ago"
}

export function readDate(date: Date, showYear = true) {
    const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];

    let result = `${months[date.getMonth()]} ${date.getDate()} `;

    if (showYear)
        result += date.getFullYear();

    return result;
}

export function readTime(date: Date) {
    let hours = date.getHours();
    let minutes = date.getMinutes();
    let ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12;

    return (minutes < 10)
        ? `${hours}:0${minutes} ${ampm}`
        : `${hours}:${minutes} ${ampm}`;
}

export function readDateTime(date: Date, showYear = true) {
    return `${readDate(date, showYear)}, ${readTime(date)}`;
}

export const buildClassUrl = (organizerName: string, title: string, classId: number) => {
    const convertStrToUrlPart = (str: string) => {
        return str.replace(/\s+/g, '-').toLowerCase();
    }

    var classUrl = convertStrToUrlPart(organizerName) + '-' + convertStrToUrlPart(title) + '-' + classId;
    classUrl = classUrl.replace(/-{2,}/g, '-')

    return classUrl;
}

export function extractIdFromClassUrl(classUrlPath: string) {
    var n = classUrlPath.lastIndexOf('-');
    return parseInt(classUrlPath.substring(n + 1));
}