import { RootState } from "../reduxReducers"
import { User } from "../core-types/user-types";
import { fetchUserDetails } from '../apis/educators';

export type UserDBAction = {
    type : 'RECEIVE_USER_LIST',
    users:User[]
}

export const fetchUserDetailsIfNeeded = (userIdList:number[]) => {
    return function(dispatch:Function,getState:() => RootState) {
        let newUserIds:number[] = []
        userIdList.forEach(reqId => {
            if(!getState().UsersDB[reqId] && !newUserIds.includes(reqId))
                newUserIds.push(reqId)
        })
        if(newUserIds.length>0){
            return fetchUserDetails(newUserIds)
            .then(dataList => {
                dispatch({type: 'RECEIVE_USER_LIST',users:dataList})
            })
        } else {
            return Promise.resolve();
        }
    }
}