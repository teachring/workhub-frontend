
import {Notification, NotificationTypes} from '../core-types/user-types';
import { fetchAllNotifications,fetchLatestNotifications,updateNotificationToRead} from '../apis/notifications';
import { async } from 'q';
import { AnnouncementObj, EducatorRegQuery } from '../core-types/educator-types';
import { fetchUserDetailsIfNeeded } from './users-db';
import { QuesAnsObj } from '../core-types/shared';
import { ClassInfo } from '../core-types/class-types';
import { RootState } from '../reduxReducers';

export type NotificationReceiveAction = {
    type : 'NOTIFICATIONS_RECEIVED',
    timerId: NodeJS.Timeout,
    notificationsList: Notification[]
}

export type NotificationReadAction = {
    type : 'NOTIFICATIONS_READ',
    notificationIdList: number[]
}

export function subscribeToNotifications(userId:number) {

    return async function(dispatch:(arg0: NotificationReceiveAction | any) => any,getState:() => RootState) {

        let lastFetchedId:number = -100

        fetchAllNotifications()
        .then(async notificationList => {

            //FIXME check if the currentLoggedIn user is the same,

            let timerId = setTimeout(
                function fetchUpdates() {
                    timerId =  setTimeout(fetchUpdates,60*1000);fetchLatestNotifications(lastFetchedId)
                    .then(async notificationUpdates => {

                        await dispatch(fetchUserDetailsAssociated(notificationUpdates));
                        if(getState().NotificationData.timerId){
                            dispatch({type:'NOTIFICATIONS_RECEIVED',timerId:timerId,notificationsList:notificationUpdates})
                        }
                        
                        lastFetchedId = findLargestId(notificationUpdates,lastFetchedId)
                        console.log("Timer ",timerId,"lastFetched ",lastFetchedId)
                    })
                }
            ,10*1000)
            await dispatch(fetchUserDetailsAssociated(notificationList));
            dispatch({type:'NOTIFICATIONS_RECEIVED',timerId:timerId,notificationsList: notificationList})
            lastFetchedId = findLargestId(notificationList,lastFetchedId);
        })

        

    }

}

const findLargestId = (notificationList:Notification[],curVal:number) => {
    let maxVal = curVal
    notificationList.forEach(item => {
        if(item.notificationId>maxVal){
            maxVal = item.notificationId
        }
    })
    return maxVal
}

export function markNotificationsAsRead(notificationIdList:number[]) {

    return function(dispatch:(arg0:NotificationReadAction) => any) {
        
        updateNotificationToRead(notificationIdList)
        .then(() => {
            dispatch({type:'NOTIFICATIONS_READ',notificationIdList:notificationIdList})
        })  
    }

}

const fetchUserDetailsAssociated = (notificationList:Notification[]) => {

    console.log("Fetch userids called!");
    return async function(dispatch:(arg0:any) => any) {

        let userIds:number[] = []
        notificationList.forEach((notificationObj) => {
            switch(notificationObj.notificationType) {
                case NotificationTypes.ANNOUNCEMENT : {
                    userIds.push((notificationObj.payload as AnnouncementObj).createdBy);
                    break;
                }
                case NotificationTypes.REG_ENQUIRY : {
                    userIds.push((notificationObj.payload as EducatorRegQuery).askedBy);
                    break;
                }
                case NotificationTypes.CLASSROOOM_QUES_ASKED : {
                    userIds.push((notificationObj.payload as QuesAnsObj).askedBy);
                    break;
                }
                case NotificationTypes.CLASSROOM_QUES_CLOSED : {
                    userIds.push((notificationObj.payload as QuesAnsObj).closedBy!);
                    break;
                }
                case NotificationTypes.MODERATOR_ACCESS : {
                    userIds.push(...(notificationObj.payload as ClassInfo).moderatorList);
                    break;
                }
            }
        })

        await dispatch(fetchUserDetailsIfNeeded(userIds));
    }

}
