const mock = (success:boolean,returnVal:any, timeout:number) => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
          if(success) {
            resolve(returnVal);
          } else {
            reject({message: 'Error'});
          }
        }, timeout || 1000);
    });
}

export default mock;