import { ClassInfo } from "./class-types"
import { IntKeyDict } from "./misc";

export type RegInfo = {
  regDateTime: Date,
  regFormData?: IntKeyDict<RegFormQuestion>,
  paymentDetails?: {
    isPaid: boolean,
    modeSelected: 'ONLINE'| 'OFFLINE',
    paymentDateTime? : Date
  }
}


export type RegClassData = {
    classInfo: ClassInfo,
    regData: RegInfo
}

export enum RegFormQuesType {
  TEXT = "text",
  CHECKBOX = "checkbox",
  RADIO = "radio"
}

export type RegFormQuesOption = {
  text: string,
  selected?: boolean
};

export type RegFormQuestion = {
  qid: number,
  question: string,
  type: RegFormQuesType,
  options?: Array<RegFormQuesOption>,
  required: boolean,
  selectedOption?: string,
  answer?: string

}

export type LearnerRegQuery = {
  queryId:number,
  query:String,
  askedDateTime: Date
}