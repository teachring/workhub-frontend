import { AnnouncementObj, EducatorRegQuery } from "./educator-types"
import { QuesAnsObj } from "./shared"
import { ClassInfo } from "./class-types"

export type User = {
    id:number,
    userName:string,
    phoneNumber?:string
}

export enum NotificationTypes {
    PAYMENT_REMINDER, 
    ANNOUNCEMENT,     
    REG_ENQUIRY,            // state update if I am a moderator of the class, go to 
    CLASSROOOM_QUES_ASKED,  
    CLASSROOM_QUES_CLOSED,  
    MODERATOR_ACCESS,       
    SUCCESSFUL_REGISTER
}

export type NotificationPayloadTypeMap = {
    [NotificationTypes.PAYMENT_REMINDER] : string,
    [NotificationTypes.ANNOUNCEMENT] : AnnouncementObj,
    [NotificationTypes.REG_ENQUIRY]: EducatorRegQuery,
    [NotificationTypes.CLASSROOOM_QUES_ASKED] : QuesAnsObj,
    [NotificationTypes.CLASSROOM_QUES_CLOSED] : QuesAnsObj,
    [NotificationTypes.MODERATOR_ACCESS] : ClassInfo,
    [NotificationTypes.SUCCESSFUL_REGISTER] : string
}

export type Notification<T=any> = {
    notificationId:number,
    classId:number,
    notificationType:NotificationTypes,
    createdDateTime:Date,
    isRead:boolean,
    payload:T
}