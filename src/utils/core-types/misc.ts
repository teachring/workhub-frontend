import { SettingsInputAntenna, SentimentSatisfiedAlt } from "@material-ui/icons"

export interface IntKeyDict<T>{
    [Key:number] : T;
}

