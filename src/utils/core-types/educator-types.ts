import { PaymentDetails, FAQ } from "./class-types";
import { IntKeyDict } from "./misc";
import { RegFormQuestion } from "./learner-reg";
import { DrawerData } from "./shared";
import {
    DashboardRounded as DashboardRoundedIcon,
    Book as BookIcon,
    Announcement as AnnouncementIcon,
    QuestionAnswer as QuestionAnswerIcon,
    LiveHelp as LiveHelpIcon,
    PeopleAlt as PeopleAltIcon,
    AssessmentRounded as AssessmentRoundedIcon,
    GroupAddRounded as GroupAddRoundedIcon
} from '@material-ui/icons';

export enum ROUTES {
    EMPTY = "",
    BASE = "/educator/classroom/:class_name",
    RESOURCES = "/learning-resources",
    QA = "/qa",
    ANNOUCEMENTS = "/annoucements",
    QUERIES = "/queries",
    PARTICIPANTS = "/participants",
    MODERATORS = "/moderators"
};

export enum DrawerTabs {
    OVERVIEW = "",
    RESOURCES = "/learning-resources",
    QA = "/qa",
    ANNOUCEMENTS = "/annoucements",
    QUERIES = "/queries",
    PARTICIPANTS = "/participants",
    MODERATORS = "/moderators"
}

export type NewDraftObj = {
    organizerName: string,
    title: string,
    description?: string,
    startDateTime?: Date,
    endDateTime?: Date,
    venue?: string,
    isPaid: boolean,
    feeAmt?: number,
    offlinePayAvl?: boolean,
    regStartDateTime?: Date,
    regEndDateTime?: Date,
    regFormData?: IntKeyDict<RegFormQuestion>,
    agenda?: string,
    prerequirement?: string,
    otherDetails?: string,
    faq?: FAQ[],
}

export type SavedDraftInfo = NewDraftObj & {
    draftId: number,
    createdBy: number,
    lastUpdateDateTime: Date
}

export type EducatorRegQuery = {
    queryId: number,
    query: string,
    askedDateTime: Date,
    status: 'PENDING' | 'RESOLVED',
    askedBy: number
}

export type AnnouncementObj = {
    announcementId: number,
    createdBy: number,
    createDateTime: Date
    message: string
}


export type RegisteredLearner = {
    userId: number,
    regDateTime: Date,
    regFormData?: IntKeyDict<RegFormQuestion>,
    paymentDetails?: {
        isPaid: boolean,
        modeSelected: 'ONLINE' | 'OFFLINE',
        paymentDateTime?: Date,
        lastReminderDateTime?: Date
    }
}