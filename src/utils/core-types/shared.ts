export type QuesAnsObj = {
    questionId: number,
    askedBy: number,
    questionBody: string,
    askedDateTime: Date,
    answerBody?: string,
    closedBy?: number,
    closeDateTime?: Date,
    status: 'PENDING' | 'CLOSED'
}

export type LearningRes = {
    resourceId: number,
    title: string,
    createdBy: number,
    creationDateTime: Date,
    body: string
}

export type DrawerItem = {
    url: string,
    text: string,
    icon?: JSX.Element
}

export type DrawerList = {
    heading?: string,
    items?: DrawerItem[]
}

export type DrawerData = {
    base: string,
    lists: DrawerList[]
}