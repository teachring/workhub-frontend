import { IntKeyDict } from "./misc";
import { RegFormQuestion } from "./learner-reg";


export type FAQ = {
    q: string,
    a: string
}


export type NewClassObj = {
    organizerName:string,
    title:string,
    description:string,
    startDateTime:Date,
    endDateTime:Date,
    venue:string,
    isPaid:boolean,
    paymentDetails?:PaymentDetails,
    regStartDateTime:Date,
    regEndDateTime:Date,
    regFormData?: IntKeyDict<RegFormQuestion>,
    agenda:string,
	prerequirement: string,
	otherDetails?:string,
	faq?:FAQ[],
}

export type ClassInfo = NewClassObj & {
    classId:number,
    bgColor: string,
    createdBy:number,
    publishDateTime: Date,
    moderatorList: number[]
}

export interface PaymentDetails {
    feeAmt:number,
    offlinePayAvl:boolean,
}

